package com.cupvm.jvm.method;

import com.cupvm.jvm.Attribute;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.Map;

public class LineNumberTableAttribute extends Attribute {
	private Map<Integer, Integer> table;
	public Map<Integer, Integer> getTable(){
		if(table==null){
			table=new LinkedHashMap<>();
			ByteBuffer buf = ByteBuffer.wrap(info);
			int line_number_table_length = buf.getChar();
			for (int i = 0; i < line_number_table_length; ++i) {
				table.put((int) buf.getChar(), (int) buf.getChar());
			}
			info=null;
		}
		return table;
	}

	public LineNumberTableAttribute(Attribute a) {
		super(a);
	}

	public int getLineNumber(int pc) {
		if(table==null)
			getTable();
		if (pc < 0)
			return 0;
		Integer l = table.get(pc);
		if (l == null)
			return getLineNumber(pc - 1);
		return l;
	}
}
