package com.cupvm.jvm.method;


import java.util.ArrayList;

public class MethodType {
	public char[] args;
	public char rtype;
	//return value size 0: void, 1: int32_t, 2: int64_t
	public int rsize;


	public MethodType(String type){
		parse(type);
	}

	public void parse(String type){
		//int paramscount = 0;
		type = type.substring(1);
		ArrayList<Character> params = new ArrayList<>(16);
		loop:while (true) {
			char c = type.charAt(0);
			switch (c) {
				case ')':
					rtype=type.charAt(1);
					switch(rtype){
						case 'V':
							rsize=0;
							break;
						case 'D':
						case 'J':
							rsize=2;
							break;
						default:
							rsize=1;
					}
					break loop;
				case 'Z':
				case 'B':
				case 'C':
				case 'S':
				case 'I':
				case 'F':
					params.add(c);
					break;
				case 'D':
				case 'J':
					params.add(c);
					params.add(c);
					break;
				case 'L':
					params.add(c);
					type = type.substring(type.indexOf(';') + 1);
					continue;
				case '[':
					params.add(c);
					char at = type.charAt(1);
					while(at=='['){
						type=type.substring(1);
						at = type.charAt(1);

					}
					if(at=='L')
						type = type.substring(type.indexOf(';') + 1);
					else
						type = type.substring(2);
					continue;
			}
			type = type.substring(1);
		}
		//this.paramscount = paramscount;
		char[] args=new char[params.size()];
		for(int i=0;i<args.length;++i){
			args[i]=params.get(i);
		}
		this.args=args;
	}
}
