package com.cupvm.jvm;

public interface Globals {
    Number aconst_null=0L;
    Integer iconst_m1=-1;
    Integer iconst_0=0;
    Integer iconst_1=1;
    Integer iconst_2=2;
    Integer iconst_3=3;
    Integer iconst_4=4;
    Integer iconst_5=5;
    Long lconst_0=0L;
    Long lconst_1=1L;
    Double dconst_0=0.0;
    Double dconst_1=1.0;
    Float fconst_0=0f;
    Float fconst_1=1f;
}
