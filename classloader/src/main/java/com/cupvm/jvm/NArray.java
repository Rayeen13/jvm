package com.cupvm.jvm;

import com.cupvm.jvm._abstract.AField;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;

import java.util.*;

public class NArray extends AClass {


	private final char type;

	public NArray(char type) {
		super(Character.toString(type),null);
		this.type = type;
	}




	/*
	@Override
	public AClass getClass(int id) {
		return null;
	}*/

	@Override
	public AMethod getDeclaredMethod(String nameAndDesc) {
		return null;
	}



	@Override
	public AClass[] getInterfaces() {
		return new AClass[0];
	}

	@Override
	public AMethod getMethodHead(String s) {
		return null;
	}


	@Override
	public HashMap<String, AField> getInstanceFields(){
		return null;
	}

	@Override
	public AMethod getMethod(String signature) {
		return null;
	}


	/*
	@Override
	public AClass getClass(final int index, final boolean initialize, MyThread monitor){
		return null;
	}*/

	public int getID(){
		return 0;
	}
}
