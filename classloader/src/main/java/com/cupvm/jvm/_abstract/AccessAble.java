package com.cupvm.jvm._abstract;
import com.cupvm.jvm.*;
import com.cupvm.jvm.constants.Constant;
import java.nio.*;

public class AccessAble implements ACCESS_MODIFIERS{
	protected String[] annotations;
	protected String[] runtimeAnnotations;
	public final boolean hasAnnotation(String name){
		if(annotations==null) return false;
		for(String a:annotations){
			if(name.equals(a))
				return true;
		}
		return false;
	}
	public final boolean hasRuntimeAnnotation(String name){
		if(runtimeAnnotations==null) return false;
		for(String a:runtimeAnnotations){
			if(name.equals(a))
				return true;
		}
		return false;
	}
	protected int access_flags;
	public final boolean isPublic() {
		return (access_flags & ACC_PUBLIC) != 0;
	}
	public final boolean isPrivate() {
		return (access_flags & ACC_PRIVATE) != 0;
	}
	public final boolean isProtected() {
		return (access_flags & ACC_PROTECTED) != 0;
	}
	public final boolean isFinal() {
		return (access_flags & ACC_FINAL) != 0;
	}
	public final boolean isSynthetic() {
		return (access_flags & ACC_SYNTHETIC) != 0;
	}

	public final void setAccessFlags(int access_flags){
		this.access_flags=access_flags;
	}

	public final int getAccessFlags(){
		return access_flags;
	}
	public void readAttributes(ByteBuffer buf, Constant[] cpool){
	}
	public final void processAttribute(String name, Attribute a, Constant[] cpool){
		switch(name) {
			case "RuntimeInvisibleAnnotations":
				annotations = new AnnotationsAttribute(a, cpool).annotations;
				break;
			case "RuntimeVisibleAnnotations":
				runtimeAnnotations = new AnnotationsAttribute(a, cpool).annotations;
				break;
		}
	}
}
