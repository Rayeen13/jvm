package com.cupvm.jvm._abstract;


import com.cupvm.jvm.FieldIntf;
import com.cupvm.jvm._class.ClassLoader;

import java.io.DataOutput;
import java.io.IOException;

public abstract class AField extends AccessAble implements FieldIntf {
	private String signature;
	private int id;

	public AField(){
	}
	public void setSignature(String signature){
		this.signature=signature;
	}
	public int index;
	public int getModifiers() {
		return access_flags;
	}

	public abstract void write(DataOutput raf) throws IOException;

	public abstract String getDesc();

	public abstract AClass getMyClass();

	public abstract String getName();

	public abstract void setName(String p0);

	public abstract Number getType(ClassLoader classLoader);

	public abstract String parseType();

	public abstract boolean isObject();

	public boolean isVolatile() {
		return (access_flags & ACC_VOLATILE) != 0;
	}

	public boolean isTransient() {
		return (access_flags & ACC_TRANSIENT) != 0;
	}

	public boolean isEnum() {
		return (access_flags & ACC_ENUM) != 0;
	}

	public boolean isStatic() {
		return (access_flags & ACC_STATIC) != 0;
	}

	public abstract Number getDefaultValue(ClassLoader classLoader);

	@Override
	public String getSignature() {
		return signature;
	}

	public void setID(int id){
		this.id=id;
	}
	@Override
	public int getID() {
		return id;
	}
}
