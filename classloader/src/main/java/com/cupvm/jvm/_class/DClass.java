package com.cupvm.jvm._class;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AField;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm.method.Method;

import java.util.ArrayList;
import java.util.HashMap;

public class DClass extends AClass {
    public final ArrayList<AMethod> methods=new ArrayList<>();
    private Method method;

    public DClass(String name){
    	super(name,null);
    	instanceTemplate=new Number[0];
    }


    @Override
    public boolean isInstance(AClass cl) {
        return false;
    }


    @Override
    public AMethod getMethod(String signature) {
        return method;
    }


    @Override
    public AMethod getDeclaredMethod(String signature) {
        return null;
    }

    @Override
    public AClass[] getInterfaces() {
        return new AClass[0];
    }

    @Override
    public AMethod getMethodHead(String s) {
        return null;
    }


    @Override
    public HashMap<String, AField> getInstanceFields(){
        return null;
    }


    int mid=0;
    public void addMethod(String name, Method method) {
    	mid++;
    	methodindex.put(name,mid);
        methodsbyid.put(mid,method);
    }
		public int getID(){
			return 0;
		}

    public void setMethod(Method method){
        this.method=method;
    }
}
