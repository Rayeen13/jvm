package com.cupvm.jvm._class;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.classpath.ClassPath;
import com.cupvm.jvm.classpath.ClassSource;
import com.cupvm.jvm.classpath.DirClassPath;
import com.cupvm.jvm.classpath.JarClassPath;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ClassLoaderImpl extends ClassLoader{
	private final ArrayList<ClassPath> classpath = new ArrayList<>(4);


	private final ClassLoader parentClassLoader;
	private boolean verbose;

	public ClassLoaderImpl() {
		parentClassLoader=null;
	}

	public ClassLoaderImpl(ClassLoader parent) {
		parentClassLoader=parent;
	}



	public void addToClasspath(String s) {
		classpath.add(new DirClassPath(new File(s)));
	}

	public void addToClasspath(ClassPath cp) {
		classpath.add(cp);
	}



	public void reset() {
		/*
		for(ClassPath classPath:classpath){
			try{
				classPath.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		classpath.clear();
		 */
		super.reset();
	}

	public void addJar(File jar) throws IOException{
		if (jar.isDirectory()) {
			addToClasspath(jar.getAbsolutePath());
		} else
			classpath.add(new JarClassPath(jar));
	}

	public InputStream getResource(String path){
		for(ClassPath cp : classpath){
			InputStream e = cp.getResource(path);
			if(e == null)
				continue;
			return e;
		}
		if(verbose)
			logger.warn(path , " resource not found");
		return null;
	}

	public AClass loadClass(String classname){
	    return loadClass(classname, this);
	}
	@Override
	public AClass loadClass(String classname, ClassLoader classLoader){
		if(parentClassLoader!=null){
			AClass clazz = parentClassLoader.loadClass(classname, parentClassLoader);
			if(clazz!=null){
				classindex.put(clazz.getName(), classes.size());
				classes.add(clazz);
				return clazz;
			}
		}
		synchronized(classindex){
			Integer index = classindex.get(classname);
			if(index != null){
				AClass c = classes.get(index);
				return c;
			}

			if(verbose)
				print("[  ] loading class: " + classname + "                                              \r");

			for(ClassPath cp : classpath){
				ClassSource e = cp.getClassSource(classname);
				if(e == null)
					continue;
				try{
					InputStream is = e.getInputStream();
					return loadClass(is, (int) e.getSize(), e.getCodeSource(),classLoader);
				}catch(Exception ex){
					throw new RuntimeException("could not load class "+classname,ex);
				}
			}
		}


		if (classname.charAt(0) == '[') {
			char at = classname.charAt(1);
			switch (at) {
				case 'I':
				case 'Z':
				case 'B':
				case 'C':
				case 'D':
				case 'F':
				case 'J':
				case 'S':
				case 'L':
					return getArrayClass(classname);
			}
		}
		return null;
	}


	public AClass getClassByID(int id){
		try{
			return classes.get(id);
		}
		catch(IndexOutOfBoundsException e){
			return null;
		}
	}
	public List<AClass> getLoadedClasses(){
		return classes;
	}
	public AClass getLoadedClass(String name){
		Integer index = classindex.get(name);
		if(index != null){
			return classes.get(index);
		}
		return null;
	}
}
