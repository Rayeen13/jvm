package com.cupvm.jvm._class;


import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm._abstract.*;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.method.*;
import java.net.URL;
import java.util.*;

public class Class extends AClass {
	public final HashMap<String, ValueHolder> statics = new HashMap<>();
	private static final Logger logger= ConsoleLogger.get();
	public final ArrayList<AMethod> constructors=new ArrayList<>();
	private BootstrapMethodsAttribute bootstrapMethods;
	private final AClass[] interfaces;
	public final Class superClass;
	public final int this_class;
	private final HashMap<Integer, AMethod> resolvedMethods=new HashMap<>();
	private final int id;
	private URL code_source;

	public final Constant[] constantPool;
	protected Class(String name, int id,
					Constant[] constantPool,
					int this_class,
					Class superClass,
					AClass[] interfaces,
					ClassLoader classLoader
					){
		super(name, classLoader);
		this.constantPool=constantPool;
		this.id=id;
		this.interfaces=interfaces;
		this.superClass=superClass;
		this.this_class=this_class;
		if(superClass==null) return;
			references.addAll(superClass.getReferenceNames());
	}

	//	ClassFile {
//		u4             magic;
//		u2             minor_version;
//		u2             major_version;
//		u2             constant_pool_count;
//		cp_info        constant_pool[constant_pool_count-1];
//		u2             access_flags;
//		u2             this_class;
//		u2             super_class;
//		u2             interfaces_count;
//		u2             interfaces[interfaces_count];
//		u2             fields_count;
//		field_info     fields[fields_count];
//		u2             methods_count;
//		method_info    methods[methods_count];
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}


	public final MethodHead getMethodHead(int id){
		Constant mc = constantPool[id];
		return (MethodHead) mc.data;
	}


	@Override
	public final boolean isInstance(AClass cl) {
		if (name==(cl.getName()))
			return true;
		for (AClass i : interfaces) {
			if (cl.getName()==(i.getName()))
				return true;
		}
		if (superClass != null)
			return superClass.isInstance(cl);

		return false;
	}



	/*
	public final Number ldc(int id) {
		Constant c = constantPool[id];
		Number oref;
		switch (c.tag) {
			case Constant.CString:
				oref=c.value;
				if(oref==null) {
				    throw new RuntimeException("look here");
					//oref = NString.createInstance(classLoader.loadClass("java/lang/String"), c.str);
					//c.value=oref;
				}
				return oref;
			case Constant.CFloat:
			case Constant.CInteger:
				return c.value;
			case Constant.CClass:
				Constant cref = constantPool[c.index1];
				String cname = cref.str;
				if (cname.charAt(0) == '[') {
					oref = classLoader.getArrayClass(cname).getMyInstanceRef();
					return oref;
				}
				oref = classLoader.loadClass(cref.str).getMyInstanceRef();
				return oref;
			default:
				throw new RuntimeException("wrong constant type");
		}
	}
	 */

	public final Number ldc2(int id) {
		Constant c = constantPool[id];
		Long l;
		switch (c.tag) {
			case Constant.CLong:
				return c.value;
			case Constant.CDouble:
				return c.value;
			default:
				throw new RuntimeException("wrong constant type");
		}
	}

	public final AMethod getDeclaredMethod(String signature){
		return methodsbyid.get(methodindex.get(signature));
	}


	@Override
	public final AClass[] getInterfaces() {
		return interfaces;
	}


	protected final void resolveMethods(BootstrapMethodsAttribute bootstrapMethods) {
		this.bootstrapMethods=bootstrapMethods;
	    for(BootstrapMethod mh: bootstrapMethods.methods) {
			AClass clazz = classLoader.loadClass(mh.mclazz);
			Method method = (Method) clazz.getMethod(mh.mname + mh.mtype);
			for (Constant arg : mh.arguments) {
				switch (arg.tag) {
					case Constant.CMethodType:
						Constant mt = constantPool[arg.index1];
						mh.desc=mt.str;
						break;
					case Constant.CMethodHandle:
						Constant mh1 = constantPool[arg.index1];
						Constant methodref = constantPool[arg.index2];
						Constant mref1 = constantPool[methodref.index1];
						Constant mclazz = constantPool[mref1.index1];
						Constant mref2 = constantPool[methodref.index2];
						Constant fname1 = constantPool[mref2.index1];
						Constant ftype1 = constantPool[mref2.index2];
						clazz = classLoader.loadClass(mclazz.str);
						mh.method = (Method) clazz.getMethod(fname1.str + ftype1.str);
						break;
				}
				//System.out.println(arg);
			}
		}
	}




	/*
	public final void clinit(MyThread thread) {
		Method m= (Method)getMyMethod("<clinit>()V");
		isInitialized = true;
		if(m==null)
			return;
		final MyThread t = new MyThread(this, classLoader,"clinit "+name,  thread)
				.setMethodToRun(m);
		try {
			t.run();
			//methodsbyname.remove("<clinit>()V");
		} catch (Exception e) {
			throw new RuntimeException(name + ".<clinit>()V" + t.getStackTrace(), e);
		}
	}
*/

	public final AField getField(final String name){
		return fields.get(name);
	}

	public final AMethod getMethod(int id) {
		AMethod m=resolvedMethods.get(id);
		if(m==null) {
			Constant mc = constantPool[id];
			int cref = mc.index1;

			AClass clazz;
			if (cref == this_class) {
			    //clazz=this;
				m= getMethod(mc.str);
			} else {
				clazz = getClass(cref);
				if(clazz==null) {
					logger.error("class not found: ", getClassName(cref));
					return null;
				}
				m= clazz.getMethod(mc.str);
			}
			/*
			if(m==null)
				throw new RuntimeException("method "+clazz.getName()+"."+mc.str+" not found");
			 */
			resolvedMethods.put(id,m);
		}
		return m;
	}

	//initializes methods class
	public final AMethod getStaticMethod(int id){
		AMethod m=resolvedMethods.get(id);
		if(m==null) {
			Constant mc = constantPool[id];
			int cref = mc.index1;

			AClass clazz;
			if (cref == this_class) {
				clazz=this;
				m= getMethod(mc.str);
			} else {
				clazz = getClass(cref);
				m= clazz.getMethod(mc.str);
			}
			if(m==null)
				throw new RuntimeException("method "+clazz.getName()+"."+mc.str+" not found");
			resolvedMethods.put(id,m);
		}
		return m;
	}

	/*
	public final AMethod getMethodHead(int id) {
		AMethod m=resolvedMethods.get(id);
		if(m==null) {
			Constant mc = constantPool[id];
			int cref = mc.index1;
			int mref = mc.index2;
			//int nid = cp.getConstant(mref - 1).index1;
			//int tid = cp.getConstant(mref - 1).index2;
			//id=constants.getConstant(ntref-1).index1;
			//String mname=cp.getConstant(nid - 1).str;
			//mname+=cp.getConstant(tid - 1).str;

			int nid = constantPool[(mref)].index1;
			int tid = constantPool[(mref)].index2;
			//id=cp.getConstant(ntref-1).index1;
			String mname = constantPool[(nid)].str;
			String parameters = constantPool[(tid)].str;
			//parameters=parameters.substring(0,parameters.indexOf(')')+1);
			if (cref == this_class) {
				m= getMethod(mname + parameters);
			} else {

				m= getClass(cref).getMethodHead(mname+parameters);
			}
			resolvedMethods.put(id,m);
		}
		return m;
	}
	 */

	protected static final ArrayList<Method> methods=new ArrayList<>();
	@Override
	public final AMethod getMethod(String signature){
		Integer mid=methodindex.get(signature);
		if (mid != null)
			return methods.get(mid);
		AClass sc = superClass;
		AMethod m;
		if(sc==null){
			for(AClass i:interfaces){
				m = i.getMethod(signature);
				if(m!=null) return m;
			}
			return null;
		}

		m = sc.getMethod(signature);
		if (m == null){
			for(AClass i:interfaces){
				m = i.getMethod(signature);
				if(m!=null) return m;
			}
		}
		return m;
	}

	public final AMethod getMethodHead(String signature) {
		Integer mid=methodindex.get(signature);
		if (mid != null)
			return methods.get(mid);
		for(AClass i:getInterfaces()) {
			AMethod m=i.getMethod(signature);
			if(m!=null)return m;
		}
		AClass sc = superClass;
		if (sc == null) return null;
		return sc.getMethodHead(signature);
	}

	public final BootstrapMethod getMethodHandle(int index) {
		return bootstrapMethods.methods[index];
	}

	@Override
	public final HashMap<String, AField> getInstanceFields(){
		return instanceFields;
	}


	public final AClass getClass(int cref) {
		return classLoader.loadClass(getClassName(cref));
	}


	public final String getClassName(int cref){
		Constant c = constantPool[cref];
		c = constantPool[c.index1];
		return c.str;
	}


//	method_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}

//	attribute_info {
//		u2 attribute_name_index;
//		u4 attribute_length;
//		u1 info[attribute_length];
//	}


	@Override
	public final String toString() {
		return name;
	}

	@Override
	public final void addMethod(final Method m){
		int mid= Class.methods.size();
		String signature=m.signature;
		methodindex.put(signature,mid);
		methods.add(m);
		m.setID(mid);

		methodsbyid.put(mid, m);
		if (signature.startsWith("<init>")) {
			constructors.add(m);
		}
	}

	public final int getThisClass() {
		return this_class;
	}



	public final URL getCodeSource(){
		return code_source;
	}


	public void createInstanceTemplate(){
		int index;
		Number[] superFields;
		Collection<AField> myfields=instanceFields.values();
		int offset;
		if(superClass==null){
			superFields=null;
			offset=0;
			instanceTemplate=new Number[myfields.size()];
		}
		else{
			superFields=superClass.instanceTemplate;
			offset=superFields.length;
			instanceTemplate=new Number[myfields.size()+offset];
			System.arraycopy(superFields,0,instanceTemplate,0,offset);
		}
		int i=offset;
		for(AField f:myfields){
			f.index=i;
			if(f.isObject())
				references.add(i);
			instanceTemplate[i++]=f.getDefaultValue(classLoader);
		}

		if(superClass!=null)
			instanceFields.putAll(superClass.instanceFields);
	}
	public final int getID(){
		return id;
	}

	@Override
	public Class getSuperClass() {
		return superClass;
	}
}
