package com.cupvm.jvm.classpath;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class DirClassPath extends ClassPath {
	private final File parent;

	public DirClassPath(File parent) {
		this.parent = parent;

	}

	@Override
	public final ClassSource getClassSource(String s) {
		try {
			File f = new File(parent, s + ".class");
			if (!f.exists())
				return null;
			InputStream is = new FileInputStream(f);
			return new ClassSource(is, parent.getAbsolutePath(),f.length());
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	public InputStream getResource(String path){
		try {
			File f = new File(parent, path);
			if (!f.exists())
				return null;
			return new FileInputStream(f);
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	@Override
	public void close(){
	}

	@Override
	public String toString(){
		return parent.toString();
	}
}
