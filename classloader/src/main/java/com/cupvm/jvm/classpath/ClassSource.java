package com.cupvm.jvm.classpath;

import java.io.InputStream;

public class ClassSource {
	private final InputStream is;
	private final long size;
	private final String codeSource;

	public ClassSource(InputStream is, String codeSource,long size) {
		this.is = is;
		this.codeSource = codeSource;
		this.size=size;
	}

	public final InputStream getInputStream() {
		return is;
	}

	public final long getSize(){
		return size;
	}

    public final String getCodeSource() {
		return codeSource;
    }
}
