package com.cupvm.jvm.classpath;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class JarClassPath extends ClassPath {
	private final ZipFile jar;
	private final File file;
	public final String prefix;


	public JarClassPath(String path) throws IOException{
		this(new File(path),"");
	}


	public JarClassPath(File f, String prefix)throws IOException {
		this.file=f;
		this.jar = new ZipFile(f);
		this.prefix = prefix;
	}

	public JarClassPath(final File jar) throws IOException{
		this(jar,"");
	}

	@Override
	public final ClassSource getClassSource(String classname) {
		ZipEntry e = jar.getEntry(prefix + classname + ".class");
		if (e != null) {
			try {
				InputStream is = jar.getInputStream(e);
				return new ClassSource(is, file.getAbsolutePath(),e.getSize());
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
		return null;
	}
	public InputStream getResource(String path){
		ZipEntry e = jar.getEntry(prefix + path);
		if (e != null) {
			try {
				return jar.getInputStream(e);
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
		return null;
	}

	@Override
	public void close() throws IOException{
		jar.close();
	}

	@Override
	public String toString(){
		return file.toString();
	}
}
