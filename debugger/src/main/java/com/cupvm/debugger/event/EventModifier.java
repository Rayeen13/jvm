package com.cupvm.debugger.event;

import com.cupvm.debugger.Location;

public class EventModifier {
    public final int modKind;
    public int count;
    public int exprID;
    public int threadID;
    public int refTypeID;
    public String classPattern;
    public String clazzexclude;
    public Location loc;
    public int exception;
    public boolean caught;
    public boolean uncaught;
    public int declating;
    public int fieldID;
    public int thread;
    public int size;
    public int depth;
    public int instance;
    public String s;

    public EventModifier(int modKind){
       this.modKind=modKind;
    }
}
