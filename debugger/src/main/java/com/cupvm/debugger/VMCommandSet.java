package com.cupvm.debugger;

public interface VMCommandSet {
    int VERSION = 1;
    int CLASSES_BY_SIGNATURE = 2;
    int ALL_CLASSES = 3;
    int ALL_THREADS = 4;
    int TOPLEVELTHREADGRPOUPS = 5;
    int DISPOSE = 6;
    int IDSIZES = 7;
    int SUSPEND = 8;
    int RESUME = 9;
    int CREATE_STRING=11;
    int CAPABILITIES = 12;
    int CLASSPATHS=13;
    int HOLD_EVENTS=15;
    int RELEASE_EVENTS=16;
}
