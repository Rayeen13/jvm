package com.cupvm.debugger;
public interface Constants{
	//TypeTag
	byte CLASS=1;
	byte INTERFACE=2;
	byte ARRAY=3;
	//ClassStatus
	byte VERIFIED=1;
	byte PREPARED=2;
	byte INITIALIZED=4;
	byte ERROR=8;
}
