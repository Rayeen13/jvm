package com.cupvm.debugger;

public interface ReferenceTypeCommandSet {
    int SIGNATURE=1;
    int CLASSLOADER=2;
    int FIELDS=4;
    int METHODS=5;
    int SOURCE_FILE=7;
    int STATUS=9;
    int INTERFACES=10;
}
