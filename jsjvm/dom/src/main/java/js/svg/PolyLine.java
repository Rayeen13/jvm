package js.svg;

public class PolyLine extends SVGElement{
	private PolyLine(){}
	public final native SVGPointList getPoints();
}
