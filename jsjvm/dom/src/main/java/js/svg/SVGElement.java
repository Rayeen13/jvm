package js.svg;

import js.dom.DOMElement;

public class SVGElement extends DOMElement{
	protected SVGElement(){
		super(SVGELEMENT);
	}
	public final native void rotate(float angle, float x, float y);
	public final native void translate(float x, float y);
	public final native void scale(float x, float y);
	public final native float getWidth();
	public final native float getHeight();
}
