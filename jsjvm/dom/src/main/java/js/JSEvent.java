package js;

public class JSEvent extends JSObject {

    protected JSEvent(){
        super(JSEVENT);
    }

    public native void preventDefault();
}
