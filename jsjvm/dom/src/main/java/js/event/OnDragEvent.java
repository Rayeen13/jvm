package js.event;


public abstract class OnDragEvent extends EventListener{
    private boolean canceled;
    public void dispatch(){
        handle(element,event);
    }
    public void cancel(){
        this.canceled=true;
    }
}
