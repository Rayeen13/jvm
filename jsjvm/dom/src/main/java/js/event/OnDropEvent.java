package js.event;

import js.dom.DOMElement;

public abstract class OnDropEvent implements Runnable{
    private DOMElement me;
    private DOMElement draggable;
    private int x, y;
    @Override
    public void run() {
        onDrop(me,draggable, x, y);
    }
    public abstract void onDrop(DOMElement me, DOMElement draggable, int x, int y);
}
