package js.net.websocket;

public abstract class OnMessage implements Runnable{
	protected String message;
	public abstract void onMessage(String message);
	@Override
	public void run(){
		onMessage(message);
	}
}
