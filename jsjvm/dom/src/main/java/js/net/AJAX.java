package js.net;

import java.io.IOException;
import java.util.function.Consumer;

import org.json.*;

public final class AJAX{
	public static class AJAXResult implements Runnable{
		private final Consumer<String> consumer;
		private final Consumer<Throwable> onerror;
		public AJAXResult(Consumer<String> consumer, Consumer<Throwable> onerror){
			this.consumer = consumer;
			this.onerror = onerror;
		}
		private String text;
		private Throwable exception;
		@Override
		public void run(){
			if(exception!=null)
				onerror.accept(exception);
			else
				consumer.accept(text);
		}
	}

	public static void getAsync(String url, Consumer<String> consumer, Consumer<Throwable> onerror){
		getAsync(url,new AJAXResult(consumer, onerror));
	}

	public native static void getAsync(String url, AJAXResult event);

	public static native String get(String url) throws IOException;
	public static native byte[] getByteArray(String url) throws IOException;
	public static native String post(String url, JSONObject params) throws IOException;
}
