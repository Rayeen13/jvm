package js;

public class JSStyle extends JSObject {
    protected JSStyle(){
	    super(JSSTYLE);
    }
    public native final void setpx(String key, float px);
    public native final void setem(String key, float em);
    public native final void setpercent(String key, float px);
}
