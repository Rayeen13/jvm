package js.dom;

public class ShadowRoot extends JSElement {
	protected ShadowRoot(){
		super(SHADOWROOT);
	}
	public native <T extends DOMElement> T getElementById(String id);
	public native DOMCollection getElementsByClassName(String popup);
}
