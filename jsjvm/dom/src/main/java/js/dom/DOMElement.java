package js.dom;
import js.*;
import js.event.*;

public class DOMElement extends JSElement {
	protected DOMElement(){
		super(DOMELEMENT);
	}

	public DOMElement(int type){
		super(type);
	}

	public final native JSStyle getStyle();
	public final native void css(String name, String value);
	public final native void cssRGBA(String name, int r, int g, int b, int a);
	public final void draggable(OnDragEvent onDragStart, OnDragEvent onDrag, OnDragEvent onDragEnd){
		draggable(onDragStart,onDrag,onDragEnd,null);
	}
	public final native void draggable(OnDragEvent onDragStart, OnDragEvent onDrag,OnDragEvent onDragEnd, String helper);
	public final void draggable(OnDragEvent onDrag){
		draggable(null,onDrag,null,null);
	}
	public final native void setAttributeNS(String namespace, String key, String value);
	public final native void setAttribute(String key, String value);
	public final native void setIntAttribute(String key, int value);
	public final native void setFloatAttribute(String key, float value);
	public final native String getAttribute(String key);
	public final native void addClass(String classname);
	public final native void removeClass(String hidden);
	public final native void setID(String id);
	public final native void click();
	public final native DOMElement getDOMElement(String key);
	public final native ShadowRoot attachShadow();

	/**
	 *
	 * @return left offset relative to document
	 */
	public final native int offsetLeft();

	/**
	 *
	 * @return top offset relative to document
	 */
	public final native int offsetTop();
	public final native int outerWidth();
	public final native int outerHeight();
	public final native DOMElement cloneNode(boolean deep);
	public final native boolean hasClass(String classname);
	public final native int offsetLeftRelativeToWindow();
	public final native int offsetTopRelativeToWindow();
	public final native void droppable(OnDropEvent onDropEvent);
    public final native void remove();
	public final native void setAnchor(Object o);
	public final native Object getAnchor();
}
