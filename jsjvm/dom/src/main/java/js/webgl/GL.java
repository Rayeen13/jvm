package js.webgl;

import js.InlineNative;
import js.dom.*;
import java.nio.ByteBuffer;

public class GL implements GLC {
	static{
		System.loadLibrary("webgl");
	}

	public GL(DOMElement canvas){
		init(canvas);
	}

	private native void init(DOMElement canvas);
	//@InlineNative
	public final native Object createBuffer();
	//@InlineNative
	public final native Object createFrameBuffer();
	@InlineNative
	public final native void deleteBuffers(Object id);
	@InlineNative
	public final native void bindFrameBuffer(int target, Object buffer);
	@InlineNative
	public final native void bindBuffer(int target, Object buffer);
	//@InlineNative
	public final native void bufferData(int type, float[] data, int draw_type);
	//@InlineNative
	public final native void bufferData(int type, short[] data, int draw_type);
	@InlineNative
	public final native void drawElements(int mode, int count, int type, int offset);
	@InlineNative
	public final native void drawArrays(int mode, int count, int size);
	@InlineNative
	public final native void clearColor(float r, float g, float b, float a);
	@InlineNative
	public final native void clearDepth(float d);
	@InlineNative
	public final native void enable(int constant);
	@InlineNative
	public final native void depthFunc(int constant);
	@InlineNative
	public final native void clear(int mask);
	@InlineNative
	public final native void vertexAttribPointer(int index, int size, int type, boolean normalized, int stride, int offset);
	@InlineNative
	public final native void enableVertexAttribArray(int index);
	//@InlineNative
	public final native Object createShader(int type);
	//@InlineNative
	public final native void shaderSource(Object shader, String source);
	@InlineNative
	public final native void compileShader(Object shader);
	//@InlineNative
	public final native Object createProgram();
	@InlineNative
	public final native void attachShader(Object program, Object shader);
	@InlineNative
	public final native void linkProgram(Object program);
	@InlineNative
	public final native void useProgram(Object program);
	@InlineNative
	public final native void viewport(int x, int y, int width, int height);
	@InlineNative
	public final native void uniformMatrix4fv(Object location, boolean transpose, float[] value);
	@InlineNative
	public final native void uniformMatrix3fv(Object location, boolean transpose, float[] value);
	@InlineNative
	public final native void uniformMatrix2fv(Object location, boolean transpose, float[] value);
	@InlineNative
	public final native void uniform1f(Object location, float value);
	@InlineNative
	public final native void uniform2f(Object location, float value1, float value2);
	@InlineNative
	public final native void uniform3f(Object location, float value1, float value2, float value3);
	@InlineNative
	public final native void uniform1i(Object id, int v);
	//@InlineNative
	public final native Object getUniformLocation(Object shaderProgram, String name);
	//@InlineNative
	public final native int getAttribLocation(Object shaderProgram, String name);
	@InlineNative
	public final native void bindVertexArray(Object id);
	@InlineNative
	public final native void deleteVertexArrays(Object vao);
	//@InlineNative
	public final native Object createVertexArray();
	@InlineNative
	public final native void activeTexture(int texture);
	@InlineNative
	public final native void bindTexture(int texture, Object texid);
	//@InlineNative
	public final native Object createTexture();
	@InlineNative
	public final native void texParameteri(int target, int pname, int param);
	@InlineNative
	public final native void texParameterf(int target, int pname, float param);
	@InlineNative
	public final native void texImage2D(int target, int level, int internalformat, int format, int type, Canvas2D canvas);
	@InlineNative
	public final native void texImage2D(int target, int level, int internalformat, int format, int type, DOMElement image);
	@InlineNative
	public final native void texImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, DOMElement image);
	@InlineNative
	public final native void texImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, ByteBuffer image);
	@InlineNative
	public final native void generateMipmap(int target);
	@InlineNative
	public final native void lineWidth(int i);
	@InlineNative
	public final native void stencilOp(int fail, int zfail, int pass);
	//@InlineNative
	public final native Object getParameter(int parameter);
	@InlineNative
	public final native void stencilFunc(int func, int ref, int mask);
	@InlineNative
	public final native void stencilFuncSeparate(int func, int ref, int mask);
	@InlineNative
	public final native void stencilMask(int mask);
	@InlineNative
	public final native void stencilMaskSeparate(int face, int mask);
	@InlineNative
	public final native void colorMask(boolean red, boolean green, boolean blue, boolean alpha);
	@InlineNative
	public final native void depthMask(boolean mask);
	@InlineNative
	public final native void framebufferTexture2D(int target, int attachment, int textarget, Object texture, int level);

}
