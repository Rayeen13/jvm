package js;

public class JSException extends RuntimeException{
	public JSException(String message){
		super(message);
	}
}
