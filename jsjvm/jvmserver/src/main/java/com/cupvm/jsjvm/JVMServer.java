package com.cupvm.jsjvm;


import com.cupvm.*;
import com.cupvm.jsc.ByteCodeToJS;
import com.cupvm.jsc.JVMInterface;
import com.cupvm.jvmservlet.jdwp.JDWPWebsocketCreator;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.*;
import org.eclipse.jetty.server.handler.*;
import java.net.*;
import java.io.*;
import com.cupvm.jvmservlet.*;


/**
 * Example of setting up a Jetty WebSocket server
 * <p>
 * Note: this uses the Jetty WebSocket API, not the javax.websocket API.
 */
public class JVMServer{
	private static final LoggerImpl logger = LoggerImpl.get();
	static{
		logger
			.getConsoleLogger()
			.setLevelFilter(Logger.WARNING);
		org.eclipse.jetty.util.log.Log.setLog(new org.eclipse.jetty.util.log.Logger(){
    @Override public String getName() { return "cupjvm"; }
    @Override public void warn(String msg, Object... args) { 
			logger.warn(msg, args);
		}
    @Override public void warn(Throwable thrown) { 
			logger.warn(thrown);
		}
    @Override public void warn(String msg, Throwable thrown) { 
			logger.warn(msg, thrown);
		}
    @Override public void info(String msg, Object... args) { 
			//logger.info(msg, args);
		}
    @Override public void info(Throwable thrown) { 
			//logger.info(thrown.toString());
		}
    @Override public void info(String msg, Throwable thrown) { 
			//logger.info(msg, thrown);
		}
    @Override public boolean isDebugEnabled() { return true; }
    @Override public void setDebugEnabled(boolean enabled) { }
    @Override public void debug(String msg, Object... args) { 
			//logger.debug(msg, args);
		}
    @Override public void debug(Throwable thrown) { 
			//logger.debug(thrown.toString());
		}
    @Override public void debug(String msg, Throwable thrown) { 
			//logger.debug(msg, thrown);
		}
    @Override public void debug(String msg, long l) { 
			logger.debug(msg, " ", l);	
		}
    @Override public org.eclipse.jetty.util.log.Logger getLogger(String name) { return this; }
    @Override public void ignore(Throwable ignored) { }
});
	}
	private static File jar;

		/**
		 * Servlet layer
		 */
		@SuppressWarnings("serial")
		/*
		public static class ChatServlet extends WebSocketServlet
		{
				@Override
				public void configure(WebSocketServletFactory factory )
				{
						// Register the echo websocket with the basic WebSocketCreator
						factory.register(ChatSocket.class);
				}
		}*/


		static int port=8080;
		static boolean debug=false;
	private static String mainClass;
	private static String cp;

	private static void parseArgs(String[] args){
			for(int i=0;i<args.length;++i){
				String arg=args[i];
				if(arg.startsWith("-")){
					switch(arg){
						case "-v":
							ByteCodeToJS.jsComments =true;
							logger
								.getConsoleLogger()
								.setLevelFilter(Logger.INFO);
							break;
						case "-debug":
							debug=true;
							break;
						case "-p":
							port=Integer.valueOf(args[i+1]);
						break;
						case "-jar":
							jar=new File(args[i+1]);
							logger.info("jar file: ", jar);
							++i;
						break;
						case "-cp":
							cp=args[i+1];
							logger.info("-cp: ", cp);
							++i;
							break;
					}
				}
				else {
					mainClass=arg;
					logger.info("mainClass ", mainClass);
				}
			}
		}

		public static void main(String[] args) throws Exception{
			parseArgs(args);
			main(port, debug);
		}

	public static void startThisJar(String[] args) throws Exception{
		URL url=JVMServer.class.getProtectionDomain().getCodeSource().getLocation();
		jar = new File(url.toURI());
		//ChatSocket.setJarFile(jar);
		parseArgs(args);
		main(port, debug);
	}

	public static void main(int port, boolean debug) throws Exception{
		JVMServlet jvmServlet =new JVMServlet(debug,null);
		jvmServlet.reset();
		if(jar!=null)
			jvmServlet.setJarFile(jar);
		if(mainClass!=null)
			jvmServlet.setMainClass(mainClass);
		if(cp!=null)
			jvmServlet.addFolderToClassPath(cp);
		JVMInterface ji=jvmServlet.getJVMInterface();
		try{
			URL url=JVMServer.class.getProtectionDomain().getCodeSource().getLocation();
			File rt = new File(url.toURI());
			if (rt.isDirectory()){
				ji.addFolderToClassPath(rt.getAbsolutePath()+"/rt");
			} else {
				try{
					ji.addJarToClassPath(rt,"rt/");
				}catch(IOException e){
					throw new RuntimeException(e);
				}
			}
		}
		catch(URISyntaxException e){
			throw new RuntimeException(e);
		}
		/*
		SyntheticClassLoader cl=ji.createClassLoader();
		cl.addJar(new File("../examples/helloworld/target/jsjvm_example.jar"));
		*/

		jvmServlet.init();

		//ji.compileRecursively();

		Server server = new Server(port);

		ServletContextHandler handler;
		HandlerList handlers = new HandlerList();
		server.setHandler(handlers);

		File webroot=new File("./webroot");
		if(webroot.isDirectory())
		{
			logger.info("webroot folder found");
			handler=initWebRoot();
			ji.setOutputDir(webroot);
		}
		//throw new RuntimeException("root folder not found");
		else
			handler=initJarWebRoot();
		handler.addServlet(new ServletHolder(jvmServlet), "/jsjvm");
		//handler.addServlet(new ServletHolder(JSServlet.class), "/js");
		//handler.addServlet(new ServletHolder(ChatServlet.class), "/jvm");

		handler.addServlet(new ServletHolder(new MyWebsocketServlet(new FileSystemWebsocketCreator())),"/fs");

		if(debug) {
			JDWPWebsocketCreator jdwpWebsocketCreator = new JDWPWebsocketCreator();
			jdwpWebsocketCreator.setJVMInterface(jvmServlet.getJVMInterface());
			handler.addServlet(new ServletHolder(new MyWebsocketServlet(jdwpWebsocketCreator)), "/jdwp");
		}

		MovedContextHandler rewriteHandler = new MovedContextHandler();
		rewriteHandler.setContextPath("/");
		rewriteHandler.setPermanent(true);
		rewriteHandler.setDiscardPathInfo(true);
		rewriteHandler.setDiscardQuery(false);
		rewriteHandler.setVirtualHosts(new String[] {"www.domain.de"});
		rewriteHandler.setNewContextURL("http://domain.de");
		handlers.addHandler(rewriteHandler);

		handlers.addHandler(handler);
		try {
			server.start();
		}
		catch (Exception e){
			logger.error("port ",port," already in use");
			server.stop();
			return;
		}
		server.join();
	}


	public static ServletContextHandler initWebRoot()throws Exception{
			ServletContextHandler context = new ServletContextHandler(
					ServletContextHandler.SESSIONS);

			// Resolve file to directory
			context.setContextPath("/");
			context.setResourceBase("./webroot");
			context.setWelcomeFiles(new String[]{"index.html"});
			ServletHolder holderPwd = new ServletHolder("default", DefaultServlet.class);
			holderPwd.setInitParameter("dirAllowed","true");
			context.addServlet(holderPwd,"/");
			// Add the echo socket servlet to the /echo path map
			//context.dumpStdErr();
			return context;
		}

		public static ServletContextHandler initJarWebRoot()throws Exception{
			ServletContextHandler context = new ServletContextHandler(
					ServletContextHandler.SESSIONS);
			// Figure out what path to serve content from
			ClassLoader cl = JVMServer.class.getClassLoader();
			// We look for a file, as ClassLoader.getResource() is not
			// designed to look for directories (we resolve the directory later)
	    URL f = cl.getResource("webroot/");
	    if (f == null)
		    throw new RuntimeException("Unable to find webroot in the jar file");

	    // Resolve file to directory
	    URI webRootUri = f.toURI();
	    logger.info("WebRoot is " + f);
	    context.setContextPath("/");
	    context.setBaseResource(Resource.newResource(webRootUri));
	    context.setWelcomeFiles(new String[]{"index.html"});
	    ServletHolder holderPwd = new ServletHolder("default", DefaultServlet.class);
	    holderPwd.setInitParameter("dirAllowed","true");
	    context.addServlet(holderPwd,"/");
	    // Add the echo socket servlet to the /echo path map
	    //context.dumpStdErr();
		return context;
	}
}
