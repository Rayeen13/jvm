package lisp.symbols;
import lisp.*;

public class T extends Symbol{
	public T(){
		super("t");
	}

	@Override
	public String toLispString(Context ctx){
		return "t";
	}
	

	public Cons evaluate(Context ctx){
		return this;
	}
}
