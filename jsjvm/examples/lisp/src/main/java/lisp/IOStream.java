package lisp;

import lisp.primitives.PByte;
import lisp.primitives.PChar;
import lisp.primitives.PInteger;

public abstract class IOStream extends Cons{

	public abstract void close();
	public abstract void write(int b);

	public abstract void write(byte[] arr);

	public abstract byte readByte();

	public abstract int readInt32();

	public PByte readPByte(){
		return new PByte(readByte());
	}

	public PInteger readPInteger32(){
		return new PInteger(readInt32());
	}

	public abstract void writeInt(int value);

	public abstract void writeDouble(double value);

	public abstract char readChar();
	public PChar readPChar(){
		return new PChar(readChar());
	}
	public abstract void writeChar(char value);

	public abstract void writeLong(long value);
}
