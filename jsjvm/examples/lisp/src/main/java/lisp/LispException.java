package lisp;

public class LispException extends RuntimeException{
	private Error error;
	public LispException(Error err){
		super("", null, false, false);
		this.error=error;	
	}
	public LispException(String message, Error err){
		super(message, null, false, false);
		this.error=error;	
	}
}
