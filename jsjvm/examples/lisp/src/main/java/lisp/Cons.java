package lisp;
import lisp.symbols.*;
import lisp.functions.*;

public class Cons{
	public static final LispString TYPE=new LispString("CONS");
	protected Cons first;
	protected Cons rest;

	protected Cons type;
	public Cons(){
		type=TYPE;
	}

	public Cons convert(Cons cons){
		first=cons.first;	
		rest=cons.rest;
		return this;
	}

	public Cons getFirst(){
		return first;
	}

	public Cons getRest(){
		return rest;
	}

	public void setFirst(Cons first){
		this.first=first;
	}

	public void setRest(Cons rest){
		this.rest=rest;
	}

	public Cons add(Cons lisp){
		rest=new Cons();
		rest.first=lisp;
		return rest;
	}

	public Cons evaluate(Cons rest, Context context){
	    if(first instanceof Symbol) 
	    	return first.evaluate(this.rest, context);
	    throw new RuntimeException("unbound symbol: " + first.quote());
	}

	/*
	public String quote(){
		StringBuilder sb=new StringBuilder();
		sb.append('(');
		String prefix="";
		Cons lisp=rest;
		while(lisp!=null){
			Cons first=lisp.getFirst();
			sb.append(prefix);
			prefix=" ";
			sb.append(first.quote());
			lisp=lisp.rest;
		}
		sb.append(')');
		return sb.toString();
	}*/

	public String quote(){
		return "";
	}

	public final String quoteString(){
		StringBuilder sb=new StringBuilder();
		Cons cons=first;
		sb.append('(');
		while(cons!=null){
			sb.append(cons.quote());
			sb.append(" ");
			if(cons.first!=null){
				sb.append(cons.first.quote());
			}
			cons=cons.rest;
		}
		cons=rest;
		while(cons!=null){
			sb.append(cons.quote());
			if(cons.first!=null)
				sb.append(cons.first.quote());
			cons=cons.rest;
		}
		sb.append(')');
		/*
		String prefix="";
		Cons lisp=rest;
		while(lisp!=null){
			sb.append(prefix);
			prefix=" ";
			sb.append(lisp);
			sb.append(lisp.getFirst().toString());
			lisp=lisp.rest;
		}*/
		return sb.toString();
	}

	public LispString getType(){
		return TYPE;
	}

	public String toLispString(Context ctx){
		return quoteString();
	}

	public Cons resolve(Context ctx){
		return null;
	}

	public void write(IOStream stream, Context ctx){
		stream.write(toLispString(ctx).getBytes());
	}

	public Cons coerce(String type) {
		throw new RuntimeException("could not convert "+getType()+" to "+type);
	}
}
