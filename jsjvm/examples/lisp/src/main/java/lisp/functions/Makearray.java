package lisp.functions;
import lisp.*;
import lisp.instance.*;
import lisp.primitives.*;

public class Makearray extends Function{
	public Makearray (){
		super("make-array");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		int dim=((PInteger)rest.evaluate(rest.getRest(),ctx)).intValue();
		System.out.println(dim);
		return new ObjectArrayInstance(dim);
	}
}
