package lisp.functions.list;
import lisp.*;

public class Rest extends Function{
	public Rest(){
		super("rest");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Cons first=rest.getFirst();
		if(first.getClass()==Symbol.class){
			first=first.resolve(ctx);
			return first.getRest().getFirst();
		}
		return rest.getRest();
	}
}
