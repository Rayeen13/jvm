package lisp.functions.math;
import lisp.*;
import lisp.primitives.*;

public class Min extends Function{
	public Min(){
		super("min");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		double min=Double.POSITIVE_INFINITY;

		Cons lisp=rest;
		while(lisp!=null){
			PDouble n=(PDouble)lisp.evaluate(this.rest,  ctx);
			if(n.doubleValue()<min){
				min=n.doubleValue();
			}
			lisp=lisp.getRest();
		}
		return new PDouble(min);
	}
}
