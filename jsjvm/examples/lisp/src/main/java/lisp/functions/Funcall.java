package lisp.functions;
import lisp.*;

public class Funcall extends Function{
	public Funcall(){
		super("funcall");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		if(rest.evaluate(this.rest, ctx)==Symbol.T)
			return rest.getRest().evaluate(this.rest, ctx);
		else
			return rest.getRest().getRest().evaluate(this.rest,    ctx);
	}
}
