package lisp.functions;
import lisp.*;

public class Setq extends Function{
	public Setq(){
		super("setq");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Symbol var=(Symbol)rest.getFirst();
		ctx.setGlobal(var.getName(), rest.getRest().getFirst().evaluate(this.rest, ctx));
		return NIL;
	}
}
