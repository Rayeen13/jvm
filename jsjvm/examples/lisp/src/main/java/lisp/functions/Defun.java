package lisp.functions;
import lisp.*;

public class Defun extends Function{
	public Defun(){
		super("defun");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		String name = ((Symbol)rest.getFirst()).getName();
		rest=rest.getRest();
		Cons args=rest.getFirst();
		rest=rest.getRest();
		Cons body=rest;
		CFunction f = new CFunction(name, args, body);
		f.setFirst(first);
		f.setRest(rest);
		ctx.put(f);
		return this;
	}
}
