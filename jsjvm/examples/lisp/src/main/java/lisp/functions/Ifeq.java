package lisp.functions;
import lisp.*;

public class Ifeq extends Function{
	public Ifeq(){
		super("=");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		double value=((Primitive)rest.evaluate( this.rest,    ctx)).doubleValue();
		Cons lisp=rest;
		while(lisp!=null){
			if(((Primitive)rest.getRest().evaluate(this.rest,      ctx)).doubleValue()!=value)
					return Symbol.NIL;
				lisp=lisp.getRest();
		}
		return Symbol.T;
	}
}
