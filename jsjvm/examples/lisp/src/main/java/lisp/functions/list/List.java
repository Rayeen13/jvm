package lisp.functions.list;
import lisp.Cons;
import lisp.Context;
import lisp.Function;
import lisp.Symbol;

public class List extends Function{
    public List(){
        super("list");
    }

    @Override
    public Cons evaluate(Cons rest, Context ctx) {
	System.out.println("first: "+rest.getFirst());
	System.out.println("rest: "+rest.getRest().getFirst());
        return rest;
    }
}
