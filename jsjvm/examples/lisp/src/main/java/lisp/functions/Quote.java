package lisp.functions;
import lisp.*;

public class Quote extends Function{
	public Quote(){
		super("quote");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		return this.rest.getFirst();
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(quote");
		Cons exp=rest;
		while(exp!=null){
			sb.append(' ')
				.append(exp.getFirst());
			exp=exp.getRest();
		}
		sb.append(')');
		return sb.toString();
	}
}
