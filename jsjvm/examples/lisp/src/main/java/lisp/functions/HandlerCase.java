package lisp.functions;
import lisp.*;
import lisp.Error;

public class HandlerCase extends Function{
	public HandlerCase(){
		super("handler-case");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Cons ret;
		try{
			ret=rest.getFirst().evaluate(this.rest,  ctx);
			return ret;
		}
		catch(LispException e){
			Cons cons=rest.getRest();
			//Error catchExp=(Error)cons.evaluate( null,   ctx);
			cons=cons.getFirst().getRest();
			Symbol msg=(Symbol)cons.getFirst().getFirst();
			ctx.setLocal(msg.getName(), new LispString(e.getMessage()));
			ret = cons.getRest().evaluate(cons.getRest().getRest(),    ctx);
			ctx.unsetLocal(msg.getName());
			return ret;
		}
		catch(Exception e){
			Cons cons=rest.getRest();
			Error catchExp=(Error)cons.evaluate( null,   ctx);
			cons=cons.getFirst().getRest();
			Symbol msg=(Symbol)cons.getFirst().getFirst();
			ctx.setLocal(msg.getName(), new LispString(e.getMessage()));
			ret = cons.getRest().evaluate(cons.getRest().getRest(),    ctx);
			ctx.unsetLocal(msg.getName());
			return ret;
		}
	}
}
