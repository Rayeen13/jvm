package lisp.functions;
import lisp.*;

public class ReadFromString extends Function{
	public ReadFromString(){
		super("read-from-string");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		String exp=((LispString)rest.getFirst()).toLispString(ctx);
		return new LispParser(exp).evaluate();
	}
}
