package lisp.functions;
import lisp.*;

public class Defvar extends Function{
	public Defvar(){
		super("defvar");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Symbol var=(Symbol)rest.getFirst();
		ctx.setGlobal(var.getName(), rest.getRest().getFirst().evaluate(rest.getRest(),  ctx));
		return null;
	}
}
