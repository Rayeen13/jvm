package lisp.functions;
import lisp.*;
import java.io.*;

public class Load extends Function{
	public Load(){
		super("load");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		String file=((LispString)rest.getFirst()).toLispString(ctx);
		try{
			return new LispParser(new File(file), ctx).evaluate();
		}
		catch(IOException e){
			throw new RuntimeException();
		}
	}
}
