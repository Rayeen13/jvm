package lisp.functions.io;
import lisp.*;

public class Write extends Function{
	public Write(){
		super("write");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Cons value=rest.getFirst().evaluate(this.rest,   ctx);
		rest=rest.getRest();
		IOStream io;
		if(rest==null)
			io=(IOStream)ctx.getLocal("*standard-output*");
		else {
			Cons stream=rest.getFirst();
			if(stream.getClass()==Symbol.class){
				io=(IOStream) stream.resolve(ctx);
			}
			else
				io= (IOStream) (stream.evaluate(null, ctx)).resolve(ctx);
		}
		value.write(io, ctx);
		return null;
	}
}
