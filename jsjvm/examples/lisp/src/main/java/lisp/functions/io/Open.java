package lisp.functions.io;
import lisp.*;
import java.io.*;

public class Open extends Function{
	public Open(){
		super("open");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
	    Cons first=rest.getFirst();
	    if(first.getClass()==Symbol.class)
	    	first=first.resolve(ctx);
		String name = ((LispString)first).toLispString(ctx);
		try{
			return new FileStream(name);
		}
		catch(FileNotFoundException e){
			throw new LispException("FileNotFound: "+name, Symbol.ERROR);
		}
		catch(IOException e){
			throw new LispException("IOException", Symbol.ERROR);
		}
	}
}
