package lisp;
import java.io.*;

public class IOStreamImpl extends IOStream{
	public final DataInputStream in;
	public final DataOutputStream out;
	public IOStreamImpl(InputStream is, OutputStream os){
		this.in=new DataInputStream(is);
		this.out=new DataOutputStream(os);
	}

	@Override
	public void close(){
		try {
			in.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public void write(int b){
		try{
			out.writeByte(b);
		}
		catch(IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public void write(byte[] arr){
		try{
			out.write(arr);
		}
		catch(IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte readByte() {
		try {
			return in.readByte();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int readInt32() {
		try {
			return in.readInt();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeInt(int value) {
		try {
			out.writeInt(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeDouble(double value) {
		try {
			out.writeDouble(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public char readChar() {
		try {
			return in.readChar();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeChar(char value) {
		try {
			if(value<127)
				out.writeByte(value);
			else
				out.writeChar(value);
			out.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeLong(long value) {
		try {
			out.writeLong(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
