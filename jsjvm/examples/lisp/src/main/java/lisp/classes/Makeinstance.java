package lisp.classes;
import lisp.*;
import lisp.instance.*;

public class Makeinstance extends Function{
	public Makeinstance(){
		super("make-instance");
	}

	public Cons evaluate(Cons rest, Context ctx){
		LClass c=(LClass)rest.getFirst().evaluate(this.rest,   ctx);
		return new ClassInstance(c);
	}
}
