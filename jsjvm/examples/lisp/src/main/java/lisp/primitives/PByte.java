package lisp.primitives;

import lisp.Context;
import lisp.IOStream;
import lisp.LispString;
import lisp.Primitive;

public class PByte extends PNumber{
    private final byte value;
    public PByte(byte value){
        this.value=value;
        type=TYPE;
    }

    public double doubleValue(){
        return value;
    }

    public int intValue(){
        return value;
    }

    @Override
    public String toString(int intValue) {
        return null;
    }

    public String toString(){
        return Integer.toString(value);
    }

    @Override
    public Primitive incf(){
        return new PInteger(value+1);
    }

    @Override
    public void write(IOStream stream, Context ctx){
        stream.write(value);
    }

    public static final LispString TYPE=new LispString("BYTE");
    @Override
    public LispString getType(){
        return TYPE;
    }
}
