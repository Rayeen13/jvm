import js.*;
import js.dom.*;
import js.event.EventListener;
import js.webgl.*;
import java.util.*;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import com.cupvm.jsjvm.*;


public class MainGL implements Runnable{
	private final DOMElement body;
	float[] pMatrix;
	Object positionsBuffer;
	Object cubeVerticesColorBuffer;
	Object cubeVerticesIndexBuffer;
	Object shaderProgram;
	int vertexPosition;
	int vertexColor;
	Object projectionMatrix;
	Object modelViewMatrix;
	private GL gl;
	private Object cubeTexture;
	private int textureCoordAttribute;
	private Object uSampler;
	private Canvas2D canvas;
	private ArrayList<Cube> cubes=new ArrayList<>();

	private Physics physics;
	public MainGL(){
		DOMElement canvas=DOM.createElement("canvas");
		canvas.setInt("width", DOM.getWidth());
		canvas.setInt("height", DOM.getWidth());
		body=DOM.getElementById("body");
		body.appendChild(canvas);
		DOMElement button=DOM.createElement("button");
		button.setContent("add cube");

		EventListener evl=new EventListener(){
			public void handle(JSElement e, JSEvent event){
				cubes.add(new Cube(modelViewMatrix,0,0,-8, physics.addBox(0,0,-8)));
			}

		};

		button.addEventListener("click", evl);
		button.css("background","gray");
		button.css("width","100%");
		button.css("height","100px");
		body.appendChild(button);

		physics=new Physics();
		gl=new GL(canvas);
	}

	public static class Cube{
		private MotionState motionState;
		private Object modelViewMatrix;
		private Matrix4f mMatrix;
		private static float[] matrix=new float[16];
		public Cube(Object modelViewMatrix, int x, int y, int z, MotionState ms){
			this.motionState=ms;
			this.mMatrix=new Matrix4f();
			mMatrix.translate(x,y,z);
			this.modelViewMatrix=modelViewMatrix;
		
		}
		private Transform trans = new Transform();
		public void draw(GL gl){
			motionState.getWorldTransform(trans);
			trans.getOpenGLMatrix(matrix);
			//mMatrix.rotate(1.1f,1,0,0);
			//mMatrix.rotate(1.2f,0,1,0);
			gl.uniformMatrix4fv(
					modelViewMatrix,
					false,
					matrix);
			//int vertexCount = 36;
			//int offset = 0;
			gl.drawElements(GL.TRIANGLES, 36, GL.UNSIGNED_SHORT, 0);
		}
	}
	public static void main(String[] args) throws Exception{
		if(!DOM.isInitialized()){
			//DOM is not initialized
			JVMServer.startThisJar(args);
			return;
		}
		DOM.registerThread();
		
		new MainGL().startWEBGL();
		Console.log("before eventloop");
	}

	public void drawScene(GL hl){

		gl.clearDepth(1.0f);                 // Clear everything

		// Clear the canvas before we start drawing on it.

		gl.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

		 gl.bindBuffer(GL.ELEMENT_ARRAY_BUFFER,cubeVerticesIndexBuffer);

		 gl.useProgram(shaderProgram);
		 gl.uniformMatrix4fv(
				 projectionMatrix,
				 false,
				 pMatrix);
		 for(Cube c:cubes){
			 c.draw(gl);
		 }
	}

	public void startWEBGL(){
		//Image img=DOM.createImage();
		GL gl=this.gl;
		float[] positions = new float[]{
			// vordere Fläche
			-1.0f, -1.0f,  1.0f, 0.0f, 0.0f,
			1.0f, -1.0f,  1.0f, 0.0f, 1.0f,
			1.0f,  1.0f,  1.0f, 1.0f, 0.0f,
			-1.0f,  1.0f,  1.0f,1.0f, 1.0f,

			// hintere Fläche
			-1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
			-1.0f,  1.0f, -1.0f,0.0f, 1.0f,
			1.0f,  1.0f, -1.0f,1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,1.0f, 1.0f,

			// obere Fläche
			-1.0f,  1.0f, -1.0f, 0.0f, 0.0f,
			-1.0f,  1.0f,  1.0f,0.0f, 1.0f,
			1.0f,  1.0f,  1.0f,1.0f, 0.0f,
			1.0f,  1.0f, -1.0f,1.0f, 1.0f,

			// untere Fläche
			-1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
			1.0f, -1.0f, -1.0f,0.0f, 1.0f,
			1.0f, -1.0f,  1.0f,1.0f, 0.0f,
			-1.0f, -1.0f,  1.0f,1.0f, 1.0f,

			// rechte Fläche
			1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
			1.0f,  1.0f, -1.0f,0.0f, 1.0f,
			1.0f,  1.0f,  1.0f,1.0f, 0.0f,
			1.0f, -1.0f,  1.0f,1.0f, 1.0f,

			// linke Fläche
			-1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
			-1.0f, -1.0f,  1.0f,0.0f, 1.0f,
			-1.0f,  1.0f,  1.0f,1.0f, 0.0f,
			-1.0f,  1.0f, -1.0f,1.0f, 1.0f
		};
		positionsBuffer = gl.createBuffer();

		gl.bindBuffer(GL.ARRAY_BUFFER, positionsBuffer);
		gl.bufferData(GL.ARRAY_BUFFER, positions, GL.STATIC_DRAW);

		short[] cubeVertexIndices = new short[]{
			0,  1,  2,      0,  2,  3,    // vorne
			4,  5,  6,      4,  6,  7,    // hinten
			8,  9,  10,     8,  10, 11,   // oben
			12, 13, 14,     12, 14, 15,   // unten
			16, 17, 18,     16, 18, 19,   // rechts
			20, 21, 22,     20, 22, 23    // links
		};

		float[] textureCoordinates = new float[]{
				// vorne
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f,
				// hinten
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f,
				// oben
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f,
				// unten
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f,
				// rechts
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f,
				// links
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f
		};
		Object cubeVerticesTextureCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesTextureCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, textureCoordinates,
				gl.STATIC_DRAW);

		float[] colors = new float[]{
			1,0,0,1,
			0,1,0,1,
			0,0,0,1,
			1,1,0,1,

			0,0,0,1,
			1,0,0,1,
			1,0,0,1,
			1,0,0,1,

			1,0,0,1,
			1,1,0,1,
			0,0,0,1,
			0,0,1,1,

			0,0,0,1,
			0,1,0,1,
			0,1,1,1,
			0,0,0,1,

			1,0,1,1,
			0,0,0,1,
			0,0,0,1,
			0,0,0,1,

			0,1,0,1,
			1,0,1,1,
			1,1,0,1,
			0,0,0,1
		};

		cubeVerticesColorBuffer = gl.createBuffer();

		gl.bindBuffer(GL.ARRAY_BUFFER, cubeVerticesColorBuffer);

		gl.bufferData(GL.ARRAY_BUFFER, colors, GL.STATIC_DRAW);

		cubeVerticesIndexBuffer = gl.createBuffer();
		gl.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, cubeVerticesIndexBuffer);

		gl.bufferData(GL.ELEMENT_ARRAY_BUFFER,cubeVertexIndices, GL.STATIC_DRAW);
		Console.log("arrays initialized");
		Console.log(positions.length);
		Console.log(cubeVertexIndices.length);


		String vsSource = "attribute vec4 aVertexPosition;" +
				"attribute vec2 aTextureCoord;" +
				"attribute vec4 aVertexColor;" +
				"uniform mat4 uModelViewMatrix;" +
				"uniform mat4 uProjectionMatrix;" +
				"varying lowp vec4 vColor;" +
				"varying highp vec2 vTextureCoord;" +
				"void main(void){" +
				"gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;" +
				"vColor = aVertexColor;" +
				"vTextureCoord = aTextureCoord;" +
				"}";

		String fsSource = "varying lowp vec4 vColor;void main(void){gl_FragColor = vColor;}";
		String fsSourceTex = "varying highp vec2 vTextureCoord;" +
				"varying lowp vec4 vColor;" +
				"uniform sampler2D uSampler;" +
				"void main(void) {" +
				"gl_FragColor = 0.75*texture2D(uSampler, vTextureCoord)+(0.25*vColor);" +
				"}";



		Object vertexShader = loadShader(gl, GL.VERTEX_SHADER, vsSource);
		Object fragmentShader = loadShader(gl, GL.FRAGMENT_SHADER, fsSourceTex);

		// Create the shader program

		shaderProgram = gl.createProgram();
		gl.attachShader(shaderProgram, vertexShader);
		gl.attachShader(shaderProgram, fragmentShader);
		gl.linkProgram(shaderProgram);


		vertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
      		vertexColor = gl.getAttribLocation(shaderProgram, "aVertexColor");
		textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
		gl.enableVertexAttribArray(textureCoordAttribute);
      		projectionMatrix = gl.getUniformLocation(shaderProgram, "uProjectionMatrix");
      		modelViewMatrix = gl.getUniformLocation(shaderProgram, "uModelViewMatrix");
		cubes.add(new Cube(modelViewMatrix,-2,0,-8, physics.addBox(-2,0,-8)));
		cubes.add(new Cube(modelViewMatrix,2,0,-8,physics.addBox(2,0,-8)));
		

		 this.pMatrix=new float[]{
			 1.8106601238250732f,
			 0f,
			 0f,
			 0f,
			 0f,
			 2.4142136573791504f,
			 0f,
			 0f,
			 0f,
			 0f,
			 -1.0020020008087158f,
			 -1,
			 0f,
			 0f,
			 -0.20020020008087158f,
			 0f
		 };
		gl.enable(GL.DEPTH_TEST);           // Enable depth testing
		gl.depthFunc(GL.LEQUAL);            // Near things obscure far things

		 {
			 int numComponents = 3;
			 int type = GL.FLOAT;
			 boolean normalize = false;
			 int stride = 5*4;
			 int offset = 0;
			 gl.bindBuffer(GL.ARRAY_BUFFER, positionsBuffer);
			 gl.vertexAttribPointer(
					 vertexPosition,
					 numComponents,
					 type,
					 normalize,
					 stride,
					 offset);
			 gl.enableVertexAttribArray(
					 vertexPosition);
		 }
		 // Tell WebGL how to pull out the colors from the color buffer
		 // into the vertexColor attribute.
		 {
			 int numComponents = 4;
			 int type = GL.FLOAT;
			 boolean normalize = false;
			 int stride = 0;
			 int offset = 0;
			 gl.bindBuffer(GL.ARRAY_BUFFER, cubeVerticesColorBuffer);
			 gl.vertexAttribPointer(
					 vertexColor,
					 numComponents,
					 type,
					 normalize,
					 stride,
					 offset);
			 gl.enableVertexAttribArray(
					 vertexColor);
		 }
		// Tell WebGL how to pull out the texture coordinates from
		// the texture coordinate buffer into the textureCoord attribute.
		{
			int numComponents = 2;
			int type = gl.FLOAT;
			boolean normalize = false;
			int stride = 0;
			int offset = 0;
			gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesTextureCoordBuffer);
			gl.vertexAttribPointer(
					textureCoordAttribute,
					numComponents,
					type,
					normalize,
					stride,
					offset);
			gl.enableVertexAttribArray(
					textureCoordAttribute);
		}
		uSampler = gl.getUniformLocation(shaderProgram, "uSampler");

		gl.useProgram(shaderProgram);
		initTextures(gl);
		gl.clearColor(0.0f, 0.0f, 0.0f, 1.0f);  // Clear to black, fully opaque
		DOM.requestAnimationFrame(this);
		while(true){
			Runnable ev=DOM.getNextEvent();
			ev.run();
		}
	}

	private void initTextures(GL gl) {
		gl.enable(gl.TEXTURE_2D);
		cubeTexture = gl.createTexture();
		DOMElement cubeImage = DOM.createElement("IMG");
		cubeImage.addEventListener("load",new EventListener() {
			@Override
			public void handle(JSElement jsElement, JSEvent jsEvent) {
				handleTextureLoaded(cubeImage, cubeTexture);
			}
		});
		cubeImage.setString("crossOrigin","");
		cubeImage.setString("src","https://picsum.photos/256");
		body.appendChild(cubeImage);
		canvas=new Canvas2D();
		body.appendChild(canvas);
		Canvas2D.Context2D ctx = canvas.getContext2D();
		ctx.setFillStyle("#FFFF00");
		ctx.fillRect(0, 0, 150, 75);
		ctx.setFont("30px Arial");
		ctx.setTextAlign("center");
		ctx.fillText("hello world", canvas.getWidth()/2, canvas.getHeight()/2);

	}

	private void handleTextureLoaded(DOMElement image, Object texture) {
		Console.debugger();
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
		gl.generateMipmap(gl.TEXTURE_2D);
		//gl.bindTexture(gl.TEXTURE_2D, null);
		// Tell WebGL we want to affect texture unit 0


		// Tell the shader we bound the texture to texture unit 0
		gl.uniform1i(uSampler, 0);
	}

	public static Object loadShader(GL gl, int type, String source){
		Object shader = gl.createShader(type);
		gl.shaderSource(shader, source);
		gl.compileShader(shader);
		return shader;
	}

	//EventHandler for animations Frame reqquest:
	public void run() {
		drawScene(gl);
		physics.step();
		DOM.requestAnimationFrame(this);
	}
}
