package com.cupvm.jsc.jvalue;

public class JValue{
	public static final JValue NULL=new JValue(null, 0);
	public String source;
	public JValue(Number value){
		this.source=null;
		this.value=value;
	}

	public JValue(String source, Number value){
		this.source=source;
		this.value=value;
	}
	public final Number value;
	public boolean equals(JValue other){
		if(value==null)return false;
		if(other.value==null)return false;
		return value.equals(other.value);
	}
}
