package com.cupvm.jsc;

import com.cupvm.jvm.method.MethodHead;
import com.cupvm.jvm.method.MethodType;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.classpath.JarClassPath;
import com.cupvm.jvm.method.Method;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class JSC{
	public boolean node=false;
	private static boolean debug=false;
	private final SyntheticClassLoader classLoader;

	public JSC() {
		this.classLoader=new SyntheticClassLoader(null, debug);
	}

	public void addFolderToClassPath(String path){
	    classLoader
		.addToClasspath(path);
	}
	public void addJarToClassPath(String path) throws IOException{
		JarClassPath jcp = new JarClassPath(path);
classLoader
		.addToClasspath(jcp);
	}
	public void addJarToClassPath(File f, String prefix) throws IOException{
		JarClassPath jcp = new JarClassPath(f,prefix);
classLoader
		.addToClasspath(jcp);
	}

	private JSFunction buildJSFunction(Class cC, Method m) {
		if(debug)
			return new ByteCodeToJSDebug(classLoader, cC, m).compile();
		else
			return new ByteCodeToJS(classLoader, cC, m, false).compile();
	}


	Stack<String> stack;
	public void compile(String mainClass){
		stack=new Stack<>();
		compile(mainClass,stack);
	}

	private void compile(String mainClass, Stack<String> stack){
		try{
			compile(new MethodHead( mainClass,"main","main([Ljava/lang/String;)V",new MethodType("([Ljava/lang/String;)V")),stack);
		}
		catch(Exception e){
			System.out.println("Compiler Error:");
			System.out.println(e.getMessage());
			for(String s:stack){
				System.out.println(s);
			}
		}
	}

	private void compile(MethodHead md, Stack<String> stack){
		final JSFunction method = classLoader.buildJSFunction(md.classname,md.signature);
		//null method is native method
		if(method==null)return;
		for(String clazz:method.classDependecies){
		    classLoader.getClass(clazz);
		}
		for(MethodHead _md:method.methodDependecies){
			compile(_md, stack);
		}
	}


	public void write(FileOutputStream fisClasses) throws IOException {
		classLoader.getJS(fisClasses);
	}
}
