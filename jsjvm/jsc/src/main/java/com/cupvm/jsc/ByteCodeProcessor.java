package com.cupvm.jsc;

import com.cupvm.jvm.OPs;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm._class.ReferenceKind;
import com.cupvm.jvm.method.Method;

public abstract class ByteCodeProcessor implements OPs, ReferenceKind {
    protected final Method method;
    protected Class cC;
    public ByteCodeProcessor(Class cC, Method m){
        this.cC=cC;
        this.method=m;
    }

    protected static boolean IS_SETTER(short[] code){
        return code[0] == OPs.ALOAD_0 && OPs.isLOAD1(code[1]) && code[2] == OPs.PUTFIELD && code[5] == OPs.RETURN;
    }

    protected static boolean IS_GETTER(short[] code){
        return code[0] == OPs.ALOAD_0 && code[1] == OPs.GETFIELD && OPs.isReturn(code[4]);
    }

    protected static short i(short b1, short b2){
        return (short) (((b1 & 0xff) << 8) | (b2 & 0xff));
    }

    protected static int ii(short b1, short b2){
        return (((b1 & 0xff) << 8) | (b2 & 0xff));
    }

    protected static int i(short b1, short b2, short b3, short b4) {
        return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
                ((0xFF & b3) << 8) | (0xFF & b4);
    }
}
