package com.cupvm.jsc;

import com.cupvm.jvm.LineNumberEntry;
import com.cupvm.jvm.OPs;
import com.cupvm.jvm.method.*;
import com.cupvm.jvm._class.Class;
import java.util.*;
import com.cupvm.jsc.jvalue.*;

public class JSFunction implements OPs {
	public boolean optimizeStack=true;
	public static final int GETTER = 1;
	public static final int SETTER = 2;
	private static HashMap<String, String> signatures=new HashMap<>();
	private static int signatureCounter=0;
	private String etableAsString;
	//public String argsTypesAsString;

	private static String ss(String signature){
		String shortSig=signatures.get(signature);
		if(shortSig==null){
			shortSig="s"+ (++signatureCounter);
			signatures.put(signature, shortSig);
		}
		return shortSig;
	}

	public boolean hasBackwardsJumps;
	public boolean hasSwitches;
	public final boolean hasETable;
	public final MException[] etable;
	public final MethodHead mt;
	public boolean async=false;
	final Set<Integer> classes;
	public int type;
	public static final String EMPTY_METHOD = "t.pop();";
	private final boolean isFinal;
	//for getter and setter
	public String fieldname;
	public String fieldtype;
	public final int max_stack;
	public final int max_lv;
	public boolean usesFrame;
	protected OP[] ocode;
	protected final Map<Integer, Integer> jumps;
	protected final LinkedHashSet<String> variables;
	public final Method method;
	public int[] refs_indexes;
	protected String js;
	protected final String signature;
	protected final String shortSignature;
	public final HashSet<String> classDependecies;
	public final HashSet<MethodHead> methodDependecies;
	protected String header;
	public boolean hasGOTO;
	protected final boolean debug;
	protected final String classLoader;

	public JSFunction(Method method, String classLoaderID, boolean debug){
		if(classLoaderID==null)
			classLoader="rcl";
		else
			classLoader=classLoaderID;
		this.debug=debug;
		if(debug) async=true;
		classes=new HashSet<>();
		jumps = new LinkedHashMap<>();
		variables = new LinkedHashSet<>();
		classDependecies=new HashSet<>();
		methodDependecies=new HashSet<>();
		ocode=new OP[method.code.length];
		this.mt=method.mh;
		this.isFinal = method.isFinal();// || method.isSynthetic();
		this.max_stack = method.getMaxStack();
		for(int i=0;i<max_stack;++i)
			addVariable("s"+i);
		this.max_lv = method.getMaxLocals();
		this.method = method;
		this.signature = method.getSignature();
		this.shortSignature=ss(signature);
		CodeAttribute codeattr = method.getCodeAttr();
		boolean hasETable=false;
		if(codeattr != null){
			MException[] etable = codeattr.getExceptionTable();
			this.etable=etable;
			hasETable=etable.length>0;
			for(MException e : etable){
				putJumpLabel(e.handler_pc);
			}
		}
		else
			etable=null;
		this.hasETable=hasETable;
	}
	public JSFunction(final JSFunction base){
		classDependecies=base.classDependecies;
		methodDependecies=base.methodDependecies;
		jumps=base.jumps;
		variables=base.variables;
		ocode=base.ocode;
		mt=base.mt;
		isFinal=base.isFinal;
		max_lv=base.max_lv;
		max_stack=base.max_stack;
		method=base.method;
		signature=base.signature;
		shortSignature=base.shortSignature;
		header=base.header;
		hasETable=base.hasETable;
		etable=base.etable;
		debug=base.debug;
		usesFrame=base.usesFrame;
		async=base.async;
		classes=base.classes;
		classLoader=base.classLoader;
		optimizeStack=base.optimizeStack;
	}


	public void addVariable(String name){
		switch(name){
			case "f":
				throw new RuntimeException();
		}
		variables.add(name);
	}

	public void setCode(OP[] code){
		ocode=code;
	}


	public void putJumpLabel(int pc){
		Integer l = jumps.get(pc);
		if(l == null){
			jumps.put(pc, pc);
		}
	}

	public String getJS(){
		return js;
	}

	public boolean isEmpty(){
		return this.js == EMPTY_METHOD;
	}
	public void loadDependecies(JVMInterface ji){
	
	}

	public void buildJS(SyntheticClassLoader ji){
		int length = ocode.length;
		if(length == 1){
		    this.js=EMPTY_METHOD;
			return;
		}
		StringBuilder functionBuilder=new StringBuilder();
		StringBuilder bodyBuilder = new StringBuilder();
		buildJSBody(bodyBuilder, ji, debug);

		//StringBuilder headerBuilder = new StringBuilder();
		if(ByteCodeToJS.jsComments)
			functionBuilder.append("/*")
					.append(method.getMyClass().getName())
					.append(".")
					.append(signature)
					.append("*/");
		functionBuilder.append("const f=t.peek();");
		if(header!=null)
			functionBuilder.append(header);
		if(debug)
			functionBuilder.append("const depth=t.entries.length;");

		if(!variables.isEmpty()){
			functionBuilder.append("let ");
			String prefix = "";
			for(String variable : variables){
				functionBuilder
						.append(prefix)
						.append(variable);
				prefix=",";
			}
			functionBuilder.append(";");
		}

		if(!jumps.isEmpty()){
			functionBuilder.append("let pc=f.pc;");
			functionBuilder.append("while(true){");
			functionBuilder.append("switch(pc){");
			functionBuilder.append("default:throw 'wrong pc';");
			if(jumps.get(0) == null)
				functionBuilder.append("case 0:");
		}
		//functionBuilder.append(functionBuilder.toString());
		functionBuilder.append(bodyBuilder.toString());
		if(!jumps.isEmpty()){
			functionBuilder.append("}");
			functionBuilder.append("}");
		}

		this.js = functionBuilder.toString();
	}

	protected void buildJSBody(StringBuilder js, SyntheticClassLoader ji, boolean debug){
		int length=ocode.length;
		loop:
		for(int i = 0; i < length; ++i){
			OP op = ocode[i];
			if(op==null)continue;
			if(ByteCodeToJS.jsComments){
				js
					.append("/*")
					.append(op.name)
					.append("*/\n");	
			}
			//System.out.println("check i:"+op.pc);
			Integer j = jumps.get(i);
			if(j != null)
				buildJumpLabel(js, j);
			if(debug)
				js.append("await dstep(t, "+i+");");
			switch(op.op){
				case IFEQ:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("===0){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IFNE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("!==0){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IFLT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("<0){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IFGE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append(">=0){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IFGT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append(">0){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IFLE:
						js
								.append("if(")
								.append("s")
								.append(op.stackpos)
								.append("<=0){")
								.append("pc=")
								.append(op.jumpTarget)
								.append(";")
								.append("continue;")
								.append("}");
					ocode[i].gc();
						continue loop;
				case IF_ICMPEQ:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("===")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ICMPNE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("!==")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ICMPLT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append(">")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ICMPGE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("<=")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ICMPGT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("<")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ICMPLE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append(">=")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ACMPEQ:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("==")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IF_ACMPNE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("!=")
							.append("s")
							.append(op.stackpos)
							.append("){")
							.append("pc=")
							.append(op.jumpTarget)
							.append(";")
							.append("continue;")
							.append("}");
					ocode[i].gc();
					continue loop;
				case IFNULL:
						js
								.append("if(!")
								.append("s")
								.append(op.stackpos)
								.append("){")
								.append("pc=")
								.append(op.jumpTarget)
								.append(";")
								.append("continue;")
								.append("}");
					ocode[i].gc();
						continue loop;
				case IFNONNULL:
						js
								.append("if(")
								.append("s")
								.append(op.stackpos)
								.append("){")
								.append("pc=")
								.append(op.jumpTarget)
								.append(";")
								.append("continue;")
								.append("}");
					ocode[i].gc();
						continue loop;
				case INVOKESTATIC:
					if(op.sm!=null)
						op.sm.invokeStatic(js,op.stackpos);
					else
						genStaticInvocation(op,js,ji);
					continue loop;
				case INVOKESPECIAL:
					if(op.sm!=null)
						op.sm.invokeSpecial(js, op.stackpos);
					else
						genSpecialInvocation(op,js,ji);
					continue loop;
				case INVOKEVIRTUAL:
					if(op.sm!=null)
						op.sm.invokeVirtual(js, op.stackpos);
					else
						genInvocation(op,js,ji);
					continue loop;
				default:
					ocode[i].gc();
			}

			if(op.clazz != null){
				//this op depends on clazz
				if(!variables.contains("clazz")){
					variables.add("clazz");
				}
				int id=ji.getLoadedClassID(op.clazz.getName());
				if(id>0){
					//class should be avaliable under "_c"+id
						js.append("clazz=")
								.append("_c")
								.append(id)
								.append(";");
				}
				else
					//load class from server

					js.append("clazz=await ")
							.append(((SyntheticClassLoader)op.clazz.getClassLoader()).getJSName())
							.append(".loadClass(\"")
							.append(op.clazz.getName())
							.append("\");if(!clazz){f.pc=")
							.append(i)
							.append(";throw 'class not found';}");
			}
			if(op.js==null)
				throw new RuntimeException("null code");
			js.append(op.js);
			if(op.donpcheck!=null){
				genNPCheck(js, i, op.donpcheck, op);
			}
			if(op.suffix!=null)
				js.append(op.suffix);
			if(ByteCodeToJS.jsComments)
				js.append("\n");
		}
	}

	protected void genNPCheck(final StringBuilder js, int pc, String variable, OP op){
		addVariable("e");
		if(hasETable)
			js
					.append("if(!"+variable+"){")
					.append("e=np(t, "+pc+","+op.op+");")
					.append("if(e)")
					.append("throw e;")
					.append("else{")
					.append("pc=f.pc;")
					.append("s0=e;")
					.append("continue;")
					.append("}")
					.append("}");
		else
			js
					.append("if(!"+variable+")")
					.append("throw np(t, "+pc+","+op.op+");");
	}

	protected void genUnsatisfiedLinkError(final StringBuilder js, int pc, OP op){
		addVariable("e");
		if(hasETable)
			js
					.append("if(m==null){")
					.append("e=ule(t, "+pc+","+op.op+");")
					.append("if(e)")
					.append("throw e;")
					.append("else{")
					.append("pc=f.pc;")
					.append("s0=e;")
					.append("continue;")
					.append("}")
					.append("}");
		else
			js
					.append("if(m==null)")
					.append("throw ule(t, "+pc+","+op.op+");");
	}

	public void wrap(SyntheticClassLoader ji, StringBuilder js, boolean debug){
		if(async)
			js.append("async ");
		js.append("function f")
			.append(method.getID())
			.append("(t");
		boolean isStatic=method.isStatic();
		if(!isStatic){
			js.append(", v0");
		}
		char[] args=mt.mt.args;
		String prefix=",";
		for(int i=0;i<args.length;++i){
			js.append(prefix)
					.append('v');
			if(isStatic)
				js.append(i);
			else
				js.append(i+1);
			prefix=",";
		}
		js.append("){");
		buildJS(ji);
		js.append(getJS());
		js.append("}");
	}

	protected void genInvocation(final OP op, final StringBuilder js, SyntheticClassLoader ji){
		if(op.method.classname=="java/lang/invoke/MethodHandle"){
			genExactIncocation(op, js);
			return;
		}
		JSFunction m = ji.getLoadedMethod(op.method);
		if(m==null){
			if(op.method.isNative&&op.method.isFinal){
				genSpecialInvocation(op, js, ji);
			}
			else {
				genVirtualInvocation(op,js,ji);
			}
			return;
		}
		//m is loaded
		//final JSFunction jsm = m.jsFunction;
		//async&=jsm.async;
		if(m.isFinal){
			genSpecialInvocation(op, js, ji);
			return;
		}
		genVirtualInvocation(op,js,ji);
	}

	private void genExactIncocation(final OP op, final StringBuilder js){
		int args_length=op.method.mt.args.length + 1;
		js
				//.append("debugger;oref=f.stack[f.stackpos-")
				//.append(args_length)
				//.append("];");
				.append("oref=")
				.append("s")
				.append(op.stackpos)
				.append(";");
		js.append("m=oref.clazz.method;")
			.append("if(!m.native)")
				.append("t.push(new Frame(m));")
				.append("f.pc=")
				.append(op.pc)
				.append(";");
		//genInvocation(js, op.method.mt.args.length,  op.method.mt.rtype);


		//js.append("f.stackpos-=")
		//		.append(op.method.mt.args.length+1)
		//		.append(";");
		StringBuilder argsb=new StringBuilder();
		String prefix=",";
		for(int i=1;i<args_length;++i){
			argsb
					.append(prefix)
					.append("s")
					.append(op.stackpos+i);
					//.append("f.stack[f.stackpos +")
					//.append(i)
					//.append("]");
			prefix=",";
		}

		js.append("try{");
		switch(op.method.mt.rtype){
			case 'V':
				js.append("await m(t")
						.append(argsb.toString())
						.append(");");
				break;
			case 'L':
			case '[':
			case 'I':
			case 'F':
			case 'Z':
			case 'B':
			case 'C':
				js
					.append("s")
					.append(op.stackpos)
					.append("=await m(t")
					.append(argsb.toString())
					.append(");");
				break;
			case 'J':
			case 'D':
				js
					.append("s")
					.append(op.stackpos)
					.append("=await m(t")
					.append(argsb.toString())
					.append(");");
				break;
		}
		if(debug)
			js
					.append("if(t.entries.length!=depth) debugger;");
		js
				.append("}")

				.append("catch(error){");
		if(ByteCodeToJS.debugOnException)
			js
					.append("debugger;");
		if(hasETable)
			js
					.append("if(f.checkETable(error, t))")
					.append("{")
					.append("pc=f.pc;")
					.append("s0=error;")
					.append("continue;")
					.append("}")
					.append("else{")
					.append("t.pop();throw error;")
					.append("}");
		else
			js
					.append("t.pop();throw error;");
		js
				.append("}");

	}

	private void genVirtualInvocation(final OP op, final StringBuilder js, SyntheticClassLoader ji){
		async=true;
		js
				.append("oref=")
				.append("s")
				.append(op.stackpos)
				.append(";");
		if(op.donpcheck!=null) {
			genNPCheck(js, op.pc, "oref", op);
		}
		else if(ByteCodeToJS.jsComments)
			js.append("/*NPCHECK SKIPPED*/");

		addVariable("m");
		beforeInvocation(js);
		js
				.append("f.pc=")
				.append(op.pc)
				.append(";");
		JSFunction mh = ji.getLoadedMethod(op.method);
		//if(mh==null)
			js
					.append("m = await oref.clazz.getMethodByName(\"")
					.append(op.method.signature)
					.append("\");");
			/*
		else
			js
					.append("m = await oref.clazz.methods[\"")
					.append(op.method.signature)
					.append("\"];");
					*/
		genUnsatisfiedLinkError(js, op.pc, op);
		js
			.append("if(!m.native)")
			.append("t.push(new Frame(m));");
		genInvocation("m", js, 1+op.method.mt.args.length, op.method.mt.rtype, op.stackpos, op.invokeTemplate, true, optimizeStack);
		afterInvocation(js);
	}

	protected void inlineNative(OP op, StringBuilder js){
		if(op.method.isNative&&op.method.isFinal){
			async=true;
			if(op.method.signature.endsWith("V")){
				addVariable("opstack");
				addVariable("sp");
				if(ByteCodeToJS.jsComments)
					js.append("/*inline native method*/\n");
				js
					.append("(");
				String prefix="";
				int stackpos=1;
				for(char c: op.method.mt.args){
					js.append(prefix);
					prefix=",";
					js
						.append("opstack[sp+")
						.append(stackpos++)
						.append("]");
					if(c=='[')
						js.append(".arr");
				}
				js.append(");");
				return;
			}
			else System.out.println("WARN: can't inline non-void return type method: "+op.method.classname+"."+op.method.signature);
		}
		else System.out.println("WARN: only native final methods can be inlined");

	}

	protected void genSpecialInvocation(OP op, StringBuilder js, SyntheticClassLoader ji){
		js
				.append("oref=")
				.append("s")
				.append(op.stackpos)
				.append(";");
		if(op.donpcheck!=null){
			genNPCheck(js, op.pc, "oref", op);

		}
		else if(ByteCodeToJS.jsComments) js.append("/*NPCHECK SKIPPED*/");
		if(op.method.nativeInline){
			inlineNative(op, js);
			return;

		}
		JSFunction m = ji.getLoadedMethod(op.method);
		if(m == null){
			beforeInvocation(js);
			js
					.append("f.pc=")
					.append(op.pc)
					.append(";")
					.append("m=await ")
					.append(((SyntheticClassLoader)op.clazz.getClassLoader()).getJSName())
					.append(".getMethod(\"")
					.append(op.method.classname)
					.append("\",\"")
					.append(op.method.signature)
					.append("\");");
			genUnsatisfiedLinkError(js, op.pc, op);
				if(!op.method.isNative)
			js
					.append("t.push(new Frame(m));");
			genInvocation("m", js, 1 + op.method.mt.args.length, op.method.mt.rtype, op.stackpos, op.invokeTemplate, true, optimizeStack);
			afterInvocation(js);
			return;
		}
		//m is loaded
		//final JSFunction jsm = m.jsFunction;
		if(m.isEmpty()){
			if(ByteCodeToJS.jsComments)
				js
						.append("/*EMPTY METHOD SKIPPED: ").append(op.method.classname).append(op.method.signature)
						.append("*/");
			//js.append("f.stackpos-=").append(op.method.mt.args.length + 1)
				//	.append(";");
			return;
		}
		switch(m.type){
			case GETTER:
				StringBuilder inline = new StringBuilder();
				if(ByteCodeToJS.jsComments)
					inline.append("/*GETTER INLINED:*/");
				if(!variables.contains("oref")){
					addVariable("oref");
					//inline.append("let oref;");
				}
				inline
						//.append("oref=f.pop();")
						.append("oref=")
						.append("s")
						.append(op.stackpos)
						.append(";")
						.append("f.push(oref.fields[\"")
						.append(m.fieldname)
						.append("\"]);");
				if(m.fieldtype.equals("D") | m.fieldtype.equals("J"))
					inline.append("f.push(0);");
				//op.js = inline.toString();
				js.append(inline.toString());
				return;
			case SETTER:
				inline = new StringBuilder();
				if(ByteCodeToJS.jsComments)
					inline.append("/*SETTER INLINED:*/");
				if(!variables.contains("o1")){
					addVariable("o1");
					//inline.append("let o1;");
				}
				if(m.fieldtype.equals("D") || m.fieldtype.equals("J"))
					inline.append("f.pop();");
				inline
						.append("o1=f.pop();")
						.append("s")
						.append(op.stackpos);
				inline.append(".fields[\"")
				.append(m.fieldname)
				.append("\"]=o1;");
				//op.js = inline.toString();
				js.append(inline.toString());
				return;
			default:
				addVariable("m");
				int cid=(ji.loadClass(op.method.classname)).getID();

				//culd be buggy:
				//if(op.method.isNative||op.method.id==null)
				JSFunction mh = ji.getLoadedMethod(op.method);
				if(op.method.isNative)
					js.append("m=")
							.append("_c")
							.append(cid)
							.append(".methods[\"")
							.append(op.method.signature)
							.append("\"];");
				else {
					//js.append("m=jvm.ms[").append(m.id).append("];");
					String mname="f"+mh.method.getID();
					js
							.append("t.push(new Frame(")
							.append(mname)
							.append("));");
					beforeInvocation(js);
					genInvocation(mname, js, 1 + op.method.mt.args.length,  op.method.mt.rtype, op.stackpos, op.invokeTemplate, true,optimizeStack);
					afterInvocation(js);
					return;
				}
				js
						//.append("stack.push(new Frame(m));")
						.append("f.pc=")
						.append(op.pc)
						.append(";");
				beforeInvocation(js);
				genInvocation("m", js, 1 + op.method.mt.args.length,  op.method.mt.rtype, op.stackpos, op.invokeTemplate, true, optimizeStack);
				afterInvocation(js);
		}
	}

	protected void genStaticInvocation(final OP op, StringBuilder js, SyntheticClassLoader ji){
		if(op.method.isNative){
			beforeInvocation(js);
			genInvocation(op.method.nativeSignature, js, op.method.mt.args.length,  op.method.mt.rtype, op.stackpos, op.invokeTemplate, true, optimizeStack);
			afterInvocation(js);
			return;
		}
		addVariable("m");
		JSFunction mh = ji.getLoadedMethod(op.method);

		js.append("f.pc=")
				.append(op.pc)
				.append(";");
		beforeInvocation(js);
		if(mh!=null){
			String mname="f"+mh.method.getID();
			js
					.append("t.push(new Frame(")
					.append(mname)
					.append("));");
			genInvocation(mname, js, op.method.mt.args.length,  op.method.mt.rtype, op.stackpos, op.invokeTemplate, true, optimizeStack);
			afterInvocation(js);
			return;
		}
		else{
			async=true;
			addVariable("e");
			js
					.append("m = await ")
					.append(((SyntheticClassLoader)op.clazz.getClassLoader()).getJSName())
					.append(".getMethod(\"")
					.append(op.method.classname)
					.append("\",\"")
					.append(op.method.signature)
					.append("\");");
			genUnsatisfiedLinkError(js, op.pc, op);
		}
		js
				.append("t.push(new Frame(m));");
		genInvocation("m", js, op.method.mt.args.length,  op.method.mt.rtype, op.stackpos, op.invokeTemplate, true, optimizeStack);
		afterInvocation(js);

	}

	public void beforeInvocation(StringBuilder sb){
			sb.append("try{");
	}
	public void afterInvocation(StringBuilder sb){
		sb
			.append("}")

			.append("catch(error){error=_e(error);");
		if(ByteCodeToJS.debugOnException)
			sb
				.append("debugger;");
		if(hasETable)
			sb
					.append("if(f.checkETable(error, t))")
					.append("{")
					.append("pc=f.pc;")
					.append("s0=error;")
					.append("continue;")
					.append("}")
					.append("else{")
					.append("t.pop();throw error;")
					.append("}");
		else
			sb
				.append("t.pop();throw error;");
		sb
			.append("}");
	}
	public void genInvocation(String invprefix, StringBuilder sb, int args, char returnType, int stackpos, JValue[] invokeTemplate, boolean await, boolean optimizeStack){
		StringBuilder argsb=new StringBuilder();
		String prefix=",";
		for(int i=0;i<args;++i){
		    String source = null;
				if(optimizeStack){
					JValue v = invokeTemplate[i];
					if(v!=null)
						source=v.source;
				}
		    //source=null;
		    argsb.append(prefix);
		    if(source!=null)
		    	argsb.append(source);
		    else
				argsb
						.append("s")
						.append(stackpos+i);
			prefix=",";
		}

		switch(returnType){
			case 'V':
				if(await)
					sb.append("await ");
				sb.append(invprefix)
					.append("(t")
					.append(argsb.toString())
					.append(");");
				break;
			case 'L':
			case '[':
			case 'I':
			case 'F':
			case 'Z':
			case 'B':
			case 'C':
				sb
						.append("s")
						.append(stackpos)
						.append("=");
				if(await)
					sb.append("await ");
				sb
						.append(invprefix)
						.append("(t")
						.append(argsb.toString())
						.append(");");
				break;
			case 'J':
			case 'D':
				sb
						.append("s")
						.append(stackpos)
						.append("=");
				if(await)
					sb.append("await ");
				sb
						.append(invprefix)
						.append("(t")
						.append(argsb.toString())
						.append(");");
				break;
		}
		if(debug)
			sb
				.append("if(t.entries.length!=depth) debugger;");

	}

	public String getExceptionTable(SyntheticClassLoader cl){
		if(etableAsString!=null)
			return etableAsString;
		CodeAttribute codeattr = method.getCodeAttr();
		if(codeattr==null) return null;
		MException[] etable = codeattr.getExceptionTable();
		if(etable.length==0)
			return null;
		Class cC = (Class) method.getMyClass();
		//////build exception table:
		StringBuilder etsb = new StringBuilder();
		etsb.append("[");
		String prefix = "";
		for(MException e : etable){
			etsb.append(prefix);
			prefix = ",";
			etsb.append("[");
			etsb.append(e.start_pc);
			etsb.append(",");
			etsb.append(e.end_pc);
			etsb.append(",");
			etsb.append(e.handler_pc);
			etsb.append(",");
			if(e.catch_type == 0)
				etsb.append(0);
			else{
				String className=cC.getClassName(e.catch_type);
				cl.getClass(className);
				etsb.append(cl.getClassID(className));
			}
			etsb.append("]");
		}
		etsb.append("]");
		etableAsString=etsb.toString();
		return etableAsString;
	}

	public boolean returnsNull = false;

	public void returns(Number pop){
		if(returnsNull) return;
		returnsNull = pop == null;
	}

	public void setHeader(String header) {
		this.header=header;
	}

	private String lineNumbersArray;
    public String buildLineNumberArray(){
    	if(lineNumbersArray!=null)
    		return lineNumbersArray;
		StringBuilder sb=new StringBuilder();
		sb.append("[");
		List<? extends LineNumberEntry> table = method.getLineNumbers();
		if(table==null)
			return null;
		Iterator<? extends LineNumberEntry> it = table.iterator();
		int codeLength=method.code.length;
		if(it.hasNext()) {
			LineNumberEntry entry = it.next();
			int nextpc=entry.getPC();
			int currentLine=entry.getLineNumber();
			String prefix="";
			int nextLine=currentLine;
			for (int i = 0; i < codeLength; i++) {
				if(i>=nextpc){
					currentLine=nextLine;
					if(it.hasNext()){
						entry=it.next();
						nextpc=entry.getPC();
						nextLine = entry.getLineNumber();
					}
				}
				sb.append(prefix);
				prefix=",";
				sb.append(currentLine);
			}
		}
		sb.append("]");
		lineNumbersArray=sb.toString();
		return lineNumbersArray;
    }

		protected void buildJumpLabel(StringBuilder js, int target){
				js
						.append("case ")
						.append(target)
						.append(":");
		}

	public boolean mayHaveBraces(){
    	return !hasOverlappingRanges&&!hasSwitches&&!hasETable&&!hasBackwardsJumps&&!hasGOTO;
	}

    public void addClassDependecy(String classname) {
    	classDependecies.add(classname);
    }

	public void addMethodDependecy(MethodHead mh) {
    	methodDependecies.add(mh);
	}

	private static class JumpRange{
		private final int width;
		public final int begin;
		public final int end;

		public JumpRange(int begin, int end){
    		this.begin=begin;this.end=end;
    		this.width=end-begin;
		}
		public boolean overlaps(JumpRange range){
			return this.begin < range.begin + range.width &&
					this.begin + this.width > range.begin;
		}
	}

	ArrayList<JumpRange> ranges=new ArrayList<>();
	public void addJumpRange(int pc, int jumpOffset){
    	if(jumpOffset<0)
    		hasBackwardsJumps=true;
    	int target=pc+jumpOffset;
    	//putJumpLabel(target);
		ranges.add(new JumpRange(pc, target));
		checkforOverlappingRanges();
	}
	public void createJumpLabels(){
		for(JumpRange range:ranges){
			putJumpLabel(range.end);
		}
	}

	private boolean hasOverlappingRanges;
	private void checkforOverlappingRanges(){
		if(hasOverlappingRanges)return;
		int rangesCount=ranges.size();
		for(int i=0;i<rangesCount;++i){
			for(int j=0;j<rangesCount;++j){
				JumpRange ri = ranges.get(i);
				JumpRange rj = ranges.get(j);
				if(ri==rj)continue;
				if(ri.overlaps(rj)){
					hasOverlappingRanges=true;
					return;
				}
			}
		}
	}

	private String toString;
	@Override
	public String toString(){
		if(toString!=null)return toString;
		StringBuilder sb=new StringBuilder();
		toString=sb.toString();
		return toString;
	}
}
