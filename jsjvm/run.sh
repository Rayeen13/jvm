#!/bin/bash
#enable job control:
set -m
#build example project:
mvn -f examples/jvmtest/pom.xml clean install -q &&
#copy the js jvm
cp -r webroot/js/* jvmserver/webroot/js/jvm/ &&
#
cd jvmserver &&
#build jsjvm, for this you need to compile the whole project from root first
mvn clean install -q &&
#run the server
if command -v nc &> /dev/null && command -v xdg-open &> /dev/null
then
	java -jar target/jvm-jar-with-dependencies.jar -v -jar ../examples/jvmtest/target/jsjvm_example-jar-with-dependencies.jar &
	while ! nc -z localhost 8080; do
		sleep 0.1 # wait for 1/10 of the second before check again
	done
	xdg-open http://localhost:8080/
	fg %1
	exit
else
	java -jar target/jvm-jar-with-dependencies.jar -v -jar ../examples/jvmtest/target/jsjvm_example-jar-with-dependencies.jar
fi
	
