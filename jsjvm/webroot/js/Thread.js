function initThread(jvm){
jvm.initial_classes.push("java/lang/Thread");
jvm.initial_classes.push("java/lang/Runnable");
jvm.inits.push(function(jvm){
jvm.
anm("java/lang/Thread", "start()V" , [],function(stack,lv,pF){
	alert("starting thread");
	const thread=lv[0];
	const worker = new Worker('js/CJSExecutor.js');
	thread.worker=worker;

	worker.addEventListener('message', function(e) {
		alert('Worker said: '+e.data);
	}, false);

	worker.postMessage('Hello World'); // Send data to our
});
});
}
