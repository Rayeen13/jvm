function java_lang_Math_cos(stack, v){
	return Math.cos(v);
}
function java_lang_Math_sin(stack, v){
	return Math.sin(v);
}
function java_lang_Math_tan(stack, v){
	return Math.tan(v);
}
function java_lang_Math_exp(stack, v){
	return Math.exp(v);
}
function java_lang_Math_log(stack, v){
	return Math.log(v);
}
function java_lang_Math_abs(stack, v){
	return Math.abs(v);
}
function java_lang_Math_ceil(stack, v){
	return Math.ceil(v);
}
function java_lang_Math_sqrt(stack, v){
	return Math.sqrt(v);
}
function java_lang_Math_random(stack){
	return Math.random();
}
function java_lang_Math_pow(stack, v1, d1, v2, d2){
	return Math.pow(v1, v2);
}
self.init_math=
async function(cl){
let java_lang_Math = await cl.loadClass("java/lang/Math",true);
const java_lang_AE = await cl.loadClass(_AE,true);
jvm.inits.push(async function(cl){
cl.
dn(java_lang_Math, "random()D" , [], java_lang_Math_random, true);
cl.
dn(java_lang_Math, "pow(DD)D" , [_D, _D], java_lang_Math_pow, true);
cl.
dn(java_lang_Math, "exp(D)D" , [_D, _D], java_lang_Math_exp, true);
cl.
dn(java_lang_Math, "log(D)D" , [_D, _D], java_lang_Math_log, true);
cl.
dn(java_lang_Math, "cos(D)D" , [_D, _D], java_lang_Math_cos, true);
cl.
dn(java_lang_Math, "sin(D)D" , [_D, _D], java_lang_Math_sin, true);
cl.
dn(java_lang_Math, "tan(D)D" , [_D, _D], java_lang_Math_tan, true);
cl.
dn(java_lang_Math, "abs(D)D" , [_D, _D], java_lang_Math_abs, true);
cl.
dn(java_lang_Math, "sqrt(D)D" , [_D, _D], java_lang_Math_sqrt,true);
cl.
dn(java_lang_Math, "ceil(D)D" , [_D, _D], java_lang_Math_ceil,true);
});
}
function java_lang_Math_roundDouble(stack, v1, d1){
	return BigInt(Math.round(v1));
}
function java_lang_Math_round(stack, v1){
	return Math.round(v1);
}
