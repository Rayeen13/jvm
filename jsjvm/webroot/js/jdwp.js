const VM_COMMAND=1;
	const CLASSES_BY_SIGNATURE=2;
	const ALL_CLASSES=3;
  const ALL_THREADS=4;
  const SUSPEND = 8;
  const RESUME = 9;
	const DISPOSE=6;
	const CREATE_STRING=11;

//
const REFERENCETYPE=2;
    const CLASSLOADER=2;

//
const CLASS_TYPE=3;
    const SUPERCLASS=1;
//
const EVENT_REQUEST = 15;
	const SET_EVENT_REQUEST = 1;
	const CLEAR_EVENT_REQUEST = 2;
//
const OBJECT_REFERENCE=9;
    const REFERENCE_TYPE=1;
//
const THREAD_REFERENCE=11;
	const THREAD_NAME=1;
    const THREAD_STATUS=4;
			//THREAD_STATUS:
			//SUSPEND_STATUS:
			const SUSPEND_STATUS_SUSPENDED=1;
    const THREAD_GROUP=5;
    const THREAD_FRAMES=6;
    const THREAD_FRAME_COUNT=7;
//
const THREADGROUPREFERENCE=12;
	const THREAD_GROUP_NAME=1;
	const CHILDREN=3;
//
const STACK_FRAME=16;
	const THIS_OBJECT=3;


//TypeTag
const T_CLASS=1;
const T_INTERFACE=2;
const T_ARRAY=3;

//ClassStatus
const T_VERIFIED=1;
const T_PREPARED=2;
const T_INITIALIZED=4;
const T_ERROR=8;

//EventKind
const SINGLE_STEP=1;
const BREAKPOINT=2;
const CLASS_PREPARE=8;

//modkinds
const MK_COUNT=1;
const MK_CONDITIONAL=2;
const MK_THREADONLY=3;
const MK_CLASSONLY=4;
const MK_CLASSMATCH=5;
const MK_CLASSEXCLUDE=6;
const MK_LOCATIONONLY=7;
const MK_EXCEPTIONONLY=8;
const MK_FIELDONLY=9;
const MK_STEP=10;
const MK_INSTANCEONLY=11;
const MK_SOURCENAMEMATCH=12;


function isPrimitive(test) {
    return (test !== Object(test));
}

class WriteAble{
	constructor(){
		this.toWrite=[];
	}
	writeByte(value){
		this.length+=1;
		this.toWrite.push(function(pos, dv){
			dv.setInt8(pos, value);
			return 1;
		});
	}

	writeInt(value){
		this.length+=4;
		this.toWrite.push(function(pos, dv){
			dv.setInt32(pos, value);
			return 4;
		});
	}

	writeLong(value){
		this.length+=8;
		this.toWrite.push(function(pos, dv){
			dv.setInt32(pos, 0);
			dv.setInt32(pos+4, value);
			return 8;
		});
	}

	writeSignature(signature){
		this.writeString("L"+signature+";");
	}

	writeString(str){
		const strbytes=new TextEncoder("utf-8").encode(str);
		const length=strbytes.length;
		this.writeInt(length);
		this.length+=length;
		this.toWrite.push(function(pos, dv){
			for(let i=0;i<length;++i){
				dv.setInt8(pos++, strbytes[i]);
			}
			return length;
		});
	}

	writeFrameLocation(frame){
			this.writeByte(1);
			this.writeInt(frame.method.clazz.id);
			this.writeInt(frame.method.id);
			this.writeLong(frame.pc);
	}

	writeLocation(clazzID, methodID, pc){
			this.writeByte(1);
			this.writeInt(clazzID);
			this.writeInt(methodID);
			this.writeLong(pc);
	}
}

var cmdIDCounter=0;
class EventCommandPacket extends WriteAble{
	constructor(){
		super();
		this.id=++cmdIDCounter;
		this.length=11;
		this.flags=0;
		this.cmd_set=64;
		this.cmd=100;
	}
	toArray(){
		const arr=new ArrayBuffer(this.length);
		const dv=new DataView(arr);
		let pos=0;
		dv.setInt32(pos, this.length);
		pos+=4;
		dv.setInt32(pos, this.id);
		pos+=4;
		dv.setUint8(pos, this.flags);
		pos+=1;
		dv.setUint8(pos, this.cmd_set);
		pos+=1;
		dv.setUint8(pos, this.cmd);
		pos+=1;
		const len=this.toWrite.length;
		for(let i=0;i<len;++i){
			pos+=this.toWrite[i](pos, dv);
		}
		return arr;

	}
}

class CommandPacket{
	constructor(dis){
		let pos=0;
		this.length=dis.getInt32(pos);
		pos+=4;
		this.id=dis.getInt32(pos);
		pos+=4;
		this.flags=dis.getUint8(pos);
		pos+=1;
		this.command_set=dis.getUint8(pos);
		pos+=1;
		this.command=dis.getUint8(pos);
		pos+=1;
		this.dv=new DataView(dis.buffer.slice(pos, pos+this.length-11));
		pos+=this.length-11;
		this.pos=0;
	}

	readByte(){
		return this.dv.getUint8(this.pos++);
	}

	readBoolean(){
		return this.dv.getUint8(this.pos++)==1;
	}

	readInt(){
		const pos=this.pos;
		this.pos+=4;
		return this.dv.getInt32(pos);
	}
	readString(){
		const length=this.readInt();
		const strbytes=this.dv.buffer.slice(this.pos, this.pos+length);
		this.pos+=length;
		return new TextDecoder("utf-8").decode(strbytes);
	}
	readLocation(){
		const type = this.readByte();
		switch (type){
			case 1:
				const classID = this.readInt();
				const methodID = this.readInt();
				const pc1 = this.readInt();
				const pc2 = this.readInt();
				return {classID:classID, methodID:methodID, pc:pc2};
		}
		throw "unknown location type";
	}
}

class ReplyPacket extends WriteAble{
	constructor(id, error_code, data){
		super();
		if(data)
			this.length=11+data.length;
		else
			this.length=11;
		this.id=id;
		this.flags=0x80;
		this.error_code=error_code;
		this.data=data;
	}

	writeInt(value){
		this.length+=4;
		this.toWrite.push(function(pos, dv){
			dv.setInt32(pos, value);
			return 4;
		});
	}

	toArray(){
		const arr=new ArrayBuffer(this.length);
		const dv=new DataView(arr);
		let pos=0;
		dv.setInt32(pos, this.length);
		pos+=4;
		dv.setInt32(pos, this.id);
		pos+=4;
		dv.setUint8(pos, this.flags);
		pos+=1;
		dv.setUint16(pos, this.error_code);
		pos+=2;
		const len=this.toWrite.length;
		for(let i=0;i<len;++i){
			pos+=this.toWrite[i](pos, dv);
		}
		return arr;

	}
}

let refCounter=1;
function parseJValues(lv) {
	if(!lv)
		return undefined;
	const l=lv.length;
	let rlv=new Array(l);
	for(let i=0;i<l;i++){
		const value=lv[i];
		if(!value){
			rlv[i]=0;
			continue;
		}
		if(isPrimitive(value))
			rlv[i]=value;
		else{
			if(!value.ref){
				value.ref=refCounter++;
			}
			rlv[i]=value.ref;
		}
	}
		return rlv;
}

class EventHandler{
	constructor(jdwp, suspendPolicy, requestID){
		this.jdwp=jdwp;
		this.requestID=requestID;
		this.suspendPolicy=suspendPolicy;
	}
	remove(){}
	handle(){}
	sendEvent(ev){
			ev.cmd=EVENT;
			ev.requestID=this.requestID;
			ev.eventKind=this.eventKind;
			this.jdwp.socket.send(JSON.stringify(ev));
			this.jdwp.suspended=this.suspendPolicy>0;
	}
}

class SingleStepHandler extends EventHandler{
	constructor(jdwp, suspendPolicy, requestID, mods){
		super(jdwp, suspendPolicy, requestID);	
		this.eventKind=SINGLE_STEP;
		const l=mods.length;
		let loc;
		let depth;
		let size;
		this.count=0;
		this.occured=0;
		for(let i=0;i<l;++i){
			const mod=mods[i];
			switch(mod.kind){
				case MK_COUNT://threadID
					this.count=mod.count;
					break;
				case MK_CLASSEXCLUDE://classFilter
							//debugger;
					break;
				case MK_STEP:
					depth=mod.depth;
					size=mod.size;
					this.thread=jvm.threads[mod.thread];
					break;
				default: debugger;
			}
		}
		if(!this.thread)
			debugger;
		const $this = this;
		let currentStackDepth=this.thread.entries.length;
		const cF=this.thread.peek();
		let currentPC=cF.pc;
		const lineNumbers=cF.method.lineNumbers;
		const len=lineNumbers.length;
		const currentLine=lineNumbers[currentPC];
		let nextPC=currentPC;
		for(let i=currentPC+1;i<len;++i){
			if(lineNumbers[i]!=currentLine){
				nextPC=i;
				break;
			}
		}
		switch(depth){
			case 0:
				//step INTO
				this.thread.onSingleStep=async function(stack, pc){
					if(--$this.count>0)return;
						const f=stack.peek();
						const evcmd=new EventCommandPacket();
						evcmd.writeByte($this.suspendPolicy);
						evcmd.writeInt(1);
						evcmd.writeByte(SINGLE_STEP);
						evcmd.writeInt($this.requestID);
						evcmd.writeInt($this.thread.ID);
						evcmd.writeFrameLocation(f);
						$this.jdwp.socket.send(evcmd.toArray());
						switch($this.suspendPolicy){
								case 1: 
										debugger;
										await suspendThread(stack);
										break;
								case 2:
										vmSuspend();
										await suspendThread(stack);
										break;
						}
				}
				break;
			case 1:
				//step OVER
				this.thread.onSingleStep=async function(stack, pc){
					if(stack.entries.length>currentStackDepth) return;
					if(--$this.count>0)return;
					if(pc>nextPC){
						const f=stack.peek();
						const evcmd=new EventCommandPacket();
						evcmd.writeByte($this.suspendPolicy);
						evcmd.writeInt(1);
						evcmd.writeByte(SINGLE_STEP);
						evcmd.writeInt($this.requestID);
						evcmd.writeInt($this.thread.ID);
						evcmd.writeFrameLocation(f);
						$this.jdwp.socket.send(evcmd.toArray());
						switch($this.suspendPolicy){
								case 1: 
										debugger;
										await suspendThread(stack);
										break;
								case 2:
										vmSuspend();
										await suspendThread(stack);
										break;
						}
					}
				}
				break;
			case 2:
				debugger;
				//step OUT
				this.thread.onSingleStep=async function(stack, pc){
					if(stack.entries.length>=currentStackDepth) return;
					if(--$this.count>0)return;
						const f=stack.peek();
						const evcmd=new EventCommandPacket();
						evcmd.writeByte($this.suspendPolicy);
						evcmd.writeInt(1);
						evcmd.writeByte(SINGLE_STEP);
						evcmd.writeInt($this.requestID);
						evcmd.writeInt($this.thread.ID);
						evcmd.writeFrameLocation(f);
						$this.jdwp.socket.send(evcmd.toArray());
						switch($this.suspendPolicy){
								case 1: 
										debugger;
										await suspendThread(stack);
										break;
								case 2:
										vmSuspend();
										await suspendThread(stack);
										break;
						}
				}
				break;
		}
	}
	remove(){
		this.thread.onSingleStep=undefined;
	}
}

class BreakPointHandler extends EventHandler{
	constructor(jdwp, suspendPolicy, requestID, mods){
		super(jdwp, suspendPolicy, requestID);	
			this.eventKind=BREAKPOINT;
			const l=mods.length;
			let loc;
			for(let i=0;i<l;++i){
					const mod=mods[i];
					switch(mod.kind){
							case MK_THREADONLY://threadID
									this.thread=jvm.threads[mod.threadID];
									break;
							case MK_LOCATIONONLY:
									loc=mod.loc;
									break;
							default: debugger;
					}
			}
			const method=self["f"+loc.methodID];
			let breakpoints=method.breakpoints;
			if(!breakpoints){
					breakpoints={};
					method.breakpoints=breakpoints;
			}
			breakpoints[loc.pc]={requestID: requestID, loc:loc};
			this.method=method;
			this.loc=loc;
	}
	remove(){
			delete this.method.breakpoints[this.loc.pc];
	}
	async handle(thread){
		if(this.thread && this.thread!=thread)
			return;
		const evcmd=new EventCommandPacket();
		evcmd.writeByte(this.suspendPolicy);
		evcmd.writeInt(1);
		evcmd.writeByte(BREAKPOINT);
		evcmd.writeInt(this.requestID);
		evcmd.writeInt(thread.ID);
		evcmd.writeLocation(this.loc.classID, this.loc.methodID, this.loc.pc);
		this.jdwp.socket.send(evcmd.toArray());
		switch(this.suspendPolicy){
			case 1: 
				debugger;
				await suspendThread(thread);
				break;
			case 2:
				vmSuspend();
				await suspendThread(thread);
				break;
		}
		//this.sendEvent({loc:this.loc});
	}
}

class ClassPrepareHandler extends EventHandler{
	constructor(jdwp, suspendPolicy, requestID, mods){
		super(jdwp, suspendPolicy, requestID);	
			this.eventKind=CLASS_PREPARE;
			let classPattern=undefined;
			let classExclude=undefined;
			let l=mods.length;
			this.count=0;
			for(let i=0;i<l;++i){
					const mod=mods[i];
					switch(mod.kind){
							case MK_COUNT:
								debugger;
								this.count=mod.count;
								break;
							case MK_CLASSMATCH:
								classPattern=mod.classPattern;
								break;
							case MK_CLASSEXCLUDE:
								classExclude=mod.classExclude;
								break;
							default:debugger;

					}
			}
			this.classPattern=classPattern;
			this.classExclude=classExclude;
			//this.class_prepare_events.push({pattern:classPattern, exclude:classExclude, requestID:requestID});
	}
	remove(){
			let evs=jdwp.classPrepareEvents;
			let l=evs.length;
			for(let i=0;i<l;++i){
				if(evs[i].requestID==this.requestID){
					evs.splice(i, 1);
					return;
				}
			}
			console.error("could not remove ClassPrepare event Handler");
			debugger;
	}
	async handle(className){
		if(--$this.count>0)return;
		const evcmd=new EventCommandPacket();
		evcmd.writeByte(this.suspendPolicy);
		evcmd.writeInt(1);
		evcmd.writeByte(CLASS_PREPARE);
		evcmd.writeInt(this.requestID);
		evcmd.writeString(className);
		this.jdwp.socket.send(evcmd.toArray());
		switch(this.suspendPolicy){
			case 1: 
				debugger;
				break;
			case 2:
				vmSuspend();
				await suspendThread(thread);
				break;
		}
			/*
		if(this.classPattern==undefined){
			this.sendEvent({name:className});
			return;
		}
		if(this.classExclude==className)
			return;
		if(this.classPatern==className){
			this.sendEvent({name:className});
			return;
		}*/
	}
}

class JDWP{
	constructor(){
		this.eventHandlers={};
		this.classPrepareEvents=[];
		this.requestIDCounter=1;
	}
	async waitForConnection(){
		const $this=this;
		return new Promise(function(resolve, reject){
			if($this.connected){
				resolve();
				return;
			}
			$this.onOpen=function(){
				$this.onOpen=undefined;
				resolve();
			}
		});
	}
	connect(){
		var loc = window.location, new_uri;
		if (loc.protocol === "https:") {
			new_uri = "wss:";
		} else {
			new_uri = "ws:";
		}
		new_uri += "//"+loc.host+"/jdwp";
		let socket;
		try{
			socket=new WebSocket(new_uri);
			this.socket=socket;
		}
		catch(error){
			console.error(error);
			return;
		}
		const $this=this;
		$this.socket.onmessage=async function(ev){
			const dv=new DataView(await ev.data.arrayBuffer());
			const cmd=new CommandPacket(dv);
			let replyPacket;
			switch(cmd.command_set){
				case VM_COMMAND:
					replyPacket=await $this.processVMCommand(cmd);
					break;
				case REFERENCETYPE:
					replyPacket=$this.processReferenceType(cmd);
					break;
				case CLASS_TYPE:
					replyPacket=$this.processClassType(cmd);
					break;
				case EVENT_REQUEST :
					replyPacket=$this.processEventRequest(cmd);
					break;
				case OBJECT_REFERENCE:
					replyPacket=$this.processObjectReference(cmd);
					break;
				case THREAD_REFERENCE:
					replyPacket=$this.processThreadReference(cmd);
					break;
				case THREADGROUPREFERENCE:
					replyPacket=$this.processThreadGroupReference(cmd);
					break;
				case STACK_FRAME:
					replyPacket=$this.processStackFrame(cmd);
					break;
				default:
					console.error("JDWP unknown command_set: "+cmd.command_set);
					debugger;
					break;
			}
			if(!replyPacket)
				replyPacket=new ReplyPacket(cmd.id, 0);
			$this.socket.send(replyPacket.toArray());
		}
		$this.socket.onopen=function(ev){
			if($this.onOpen)
				$this.onOpen();
		}
		$this.socket.onclose=async function(ev){
			await vmDispose();
			createJDWPConnection();
			$this.connected=false;
		}
		$this.socket.onerror=function(error){
			$this.connected=false;
		}
	}

	async processVMCommand(cmd){
		let reply;
		switch(cmd.command){
			case ALL_CLASSES:
				reply=new ReplyPacket(cmd.id, 0);
				const classloaders=jvm.classloaders;
				let sumLen=0;
				for(let i=0;i<classloaders.length;++i){
					sumLen+=Object.keys(classloaders[i].classes).length;
				}
				reply.writeInt(sumLen);
				for(let i=0;i<classloaders.length;++i){
					const classloader=classloaders[i];
					const names=Object.keys(classloader.classes);
					const len=names.length;
					for(let i=0;i<len;++i){
						const name=names[i];
						const clazz=classloader.classes[name];
						if(clazz.isInterface)
							reply.writeByte(T_INTERFACE);
						else
							reply.writeByte(T_CLASS);
						reply.writeInt(clazz.id);
						reply.writeSignature(name);
						reply.writeInt(T_VERIFIED | T_PREPARED | T_INITIALIZED);
					}
				}
				return reply;
			case ALL_THREADS:
				reply=new ReplyPacket(cmd.id, 0);
				const threads=jvm.threads;
				const ids=Object.keys(threads);
				const length=ids.length;
				reply.writeInt(length);
				for(let i=0;i<length;++i){
					reply.writeInt(threads[ids[i]].ID);
				}
				return reply;
			case SUSPEND:
				await vmSuspend();
				reply=new ReplyPacket(cmd.id, 0);
				return reply;
			case RESUME:
				await vmResume();
				reply=new ReplyPacket(cmd.id, 0);
				return reply;
			case DISPOSE:
				this.socket.send(new ReplyPacket(cmd.id, 0).toArray());
				this.socket.close();
				return null;
			case CREATE_STRING:
				debugger;
				const str=cmd.readString();
				reply=new ReplyPacket(cmd.id, 0);
				reply.writeInt(0);
				return reply;
			default:	
				console.error("JDWP unknown VM command: "+cmd.command);
				debugger;
				break;
		}
		return null;
	}

	processReferenceType(cmd){
		let reply;
		switch(cmd.command){
			case CLASSLOADER:
				debugger;
				break;
			default:	
				console.error("JDWP unknown referenceType command: "+cmd.command);
				debugger;
				break;
		}
		return null;
	}

	processClassType(cmd){
		let reply;
		let replyPacket;
		switch(cmd.command){
			case SUPERCLASS:
				replyPacket=new ReplyPacket(cmd.id, 0);
				let clazzID=cmd.readInt();
				let clazz=jvm.cs[clazzID];
				let superClass=0;
				if(clazz.super_class)
					superClass=clazz.super_class.id;
				replyPacket.writeInt(superClass);
				return replyPacket;
			default:	
				console.error("JDWP unknown classType command: "+cmd.command);
				debugger;
				break;
		}
		return null;
	}

	processEventRequest(cmd){
		let eventKind;
		let requestID;
		let replyPacket;
		switch(cmd.command){
			case SET_EVENT_REQUEST:
				eventKind=cmd.readByte();
				const suspendPolicy=cmd.readByte();
				const modCount=cmd.readInt();
				const mods=[];
				for(let i=0;i<modCount;++i){
					const modKind = cmd.readByte();
					const mod={kind:modKind}
					mods[i]=mod;
					switch (modKind) {
						case MK_COUNT:
							mod.count=cmd.readInt();
							break;
						case MK_CONDITIONAL:
							mod.exprID = cmd.readInt();
							break;
						case MK_THREADONLY:
							//threadID
							mod.threadID = cmd.readInt();
							break;
						case MK_CLASSONLY:
							//referenceTypeID
							mod.refTypeID = cmd.readInt();
							break;
						case MK_CLASSMATCH:
							//String
							mod.classPattern=cmd.readString();
							break;
						case MK_CLASSEXCLUDE:
							//String
							mod.clazzexclude = cmd.readString();
							break;
						case MK_LOCATIONONLY:
							//location
							mod.loc=cmd.readLocation();
							break;
						case MK_EXCEPTIONONLY:
							//referenceTypeID
							mod.exception = cmd.readInt();
							mod.caught=cmd.readBoolean();
							mod.uncaught=cmd.readBoolean();
							break;
						case MK_FIELDONLY:
							//referenceTypeID
							mod.declating = cmd.readInt();
							mod.fieldID = cmd.readInt();
							break;
						case MK_STEP:
							//threadID
							mod.thread = cmd.readInt();
							mod.size = cmd.readInt();
							mod.depth = cmd.readInt();
							break;
						case MK_INSTANCEONLY:
							//objectID
							mod.instance = cmd.readInt();
							break;
						case MK_SOURCENAMEMATCH:
							//objectID
							//string
							mod.s=cmd.readString();
							break;
					}
				}
				requestID=this.addEventListener(suspendPolicy, eventKind, mods);
				replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeInt(requestID);
				return replyPacket;
			case CLEAR_EVENT_REQUEST:
				eventKind=cmd.readByte();
				requestID=cmd.readInt();
				this.removeEventListener(eventKind, requestID);
				return null;
			default:	
				console.error("JDWP unknown EventRequest command: "+cmd.command);
				debugger;
				break;
		}
		return null;
	}

	processObjectReference(cmd){
		switch(cmd.command){
			case REFERENCE_TYPE:
				const oid=cmd.readInt();
				const replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeByte(T_CLASS);
				replyPacket.writeInt(java_lang_Thread.id);
				return replyPacket;
			default:	
				console.error("JDWP unknown ObjectReference command: "+cmd.command);
				debugger;
				break;
		}
	}

	processThreadReference(cmd){
		let threadID;
		let thread;
		let replyPacket;
		switch(cmd.command){
			case THREAD_NAME:
				threadID=cmd.readInt();
				thread=jvm.threads[threadID];
				replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeString(thread.instance.fields.name.str);
				return replyPacket;
			case THREAD_GROUP:
				threadID=cmd.readInt();
				replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeInt(-1);
				return replyPacket;
			case THREAD_STATUS:
				threadID=cmd.readInt();
				thread=jvm.threads[threadID];
				replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeInt(thread.state);
				replyPacket.writeInt(thread.suspended?1:0);
				return replyPacket;
			case THREAD_FRAMES:
				threadID=cmd.readInt();
				const startFrame=cmd.readInt();
				let length=cmd.readInt();
				thread=jvm.threads[threadID];
				replyPacket=new ReplyPacket(cmd.id, 0);
				refCounter=1;
				const jframes=thread.entries;
				const l=jframes.length;

				length=Math.min(length, jframes.length);
				if(length===-1)
					length=jframes.length-startFrame;
				replyPacket.writeInt(length);

				for(let i=startFrame;i<startFrame+length;++i){
					replyPacket.writeInt(l-i-1);
					let jframe=jframes[l-i-1];
					replyPacket.writeFrameLocation(jframe);
				}
				return replyPacket;
			case THREAD_FRAME_COUNT:
				threadID=cmd.readInt();
				thread=jvm.threads[threadID];
				replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeInt(thread.entries.length);
				return replyPacket;
			default:	
				console.error("JDWP unknown ThreadReference command: "+cmd.command);
				debugger;
				break;
		}
	}

	processThreadGroupReference(cmd){
		switch(cmd.command){
			case CHILDREN:
						debugger;
				const threadGroupID=cmd.readInt();
				const replyPacket=new ReplyPacket(cmd.id, 0);
				const threads=jvm.threads;
				const ids=Object.keys(threads);
				const length=ids.length;
				replyPacket.writeInt(length);
				for(let i=0;i<length;++i){
					replyPacket.writeInt(threads[ids[i]].ID);
				}
				//no child thread groups
				replyPacket.writeInt(0);
				return replyPacket;
			default:	
				console.error("JDWP unknown ThreadGroupReference command: "+cmd.command);
				debugger;
				break;
		}
	}

	processStackFrame(cmd){
		let threadID;
		let thread;
		let frameID;
		let frame;
		switch(cmd.command){
			case THIS_OBJECT:
				threadID=cmd.readInt();
				frameID=cmd.readInt();
				thread=jvm.threads[threadID];
				const replyPacket=new ReplyPacket(cmd.id, 0);
				replyPacket.writeInt(42);
				return replyPacket;
			default:	
				console.error("JDWP unknown ThreadGroupReference command: "+cmd.command);
				debugger;
				break;
		}
	}


	removeEventListener(eventKind, requestID){
		const handler=this.eventHandlers[requestID];
		if(!handler){
			console.error("eventHandler not found. requestID: "+requestID);
			debugger;
			return;
		}
		handler.remove();
		delete this.eventHandlers[requestID];
	}

	addEventListener(suspendPolicy, eventKind, mods){
		const requestID=this.requestIDCounter++;
		let handler;
		let msize;
		switch(eventKind){
			case SINGLE_STEP:
				handler=new SingleStepHandler(this, suspendPolicy, requestID, mods);
				break;
			case BREAKPOINT:
				handler=new BreakPointHandler(this, suspendPolicy, requestID, mods);
				break;
			case CLASS_PREPARE:
				handler=new ClassPrepareHandler(this, suspendPolicy, requestID, mods);
				/*
								if(handler.classPattern){
										if(jvm.classes[handler.classPattern]){
												handler.handle(handler.classPattern);
												return;
										}
								}*/
				this.classPrepareEvents.push(handler);
				if(jvm.events.onClassPrepared)break;
				const $this=this;
				const classPrepareEvents=this.classPrepareEvents;
				jvm.events.onClassPrepared=function(name){
					const l=classPrepareEvents.length;
					for(let i=0;i<l;++i){
						const ev=classPrepareEvents[i];
						ev.handle(name);
					}
				};
				break;
		}
		this.eventHandlers[requestID]=handler;
		return requestID;
	}
	async breakpoint(thread, brk){
		const $this=this;
		const handler=$this.eventHandlers[brk.requestID];
		if(!handler){
			console.error("eventHandler not found. requestID: "+requestID);
			return;
		}
		await handler.handle(thread);
	}

}
var jdwp;
function createJDWPConnection(){
	jdwp=new JDWP();
	jdwp.connect();
}
createJDWPConnection();

async function vmSuspend(){
	const threads=jvm.threads;
	const ids=Object.keys(threads);
	const length=ids.length;
	for(let i=0;i<length;++i){
		const t=threads[ids[i]];
		//t.suspend();
		++t.suspended;
	}
}

async function vmDispose(f){
	const ehs=jdwp.eventHandlers;
	const ids=Object.keys(ehs);
	const length=ids.length;
	for(let i=0;i<length;++i){
		const id=ids[i];
		const eh=ehs[id];
		delete ehs[id];
		if(eh)
			eh.remove();
	}
	await vmResume(true);
}
async function vmResume(f){
	const threads=jvm.threads;
	const ids=Object.keys(threads);
	const length=ids.length;
	for(let i=0;i<length;++i){
		const t=threads[ids[i]];
		//t.resume();
		if(f)
			t.suspended=0;
		else
			--t.suspended;
	}

	const st=jvm.suspendedThreads;
	const l=st.length;
	while(st.length>0){
		const thread=st.pop();
		if(thread.suspended<=0){
			sthreads.push(thread.resume);
		}
	}
	_yield();
}

async function suspendThread(thread){
	return new Promise(function(resolve, reject){
		const st=jvm.suspendedThreads;
		st.push(thread);	
		thread.resume = resolve;
	});
}

async function dstep(stack, pc, lv){
	if(stack.onSingleStep){
		stack.peek().pc=pc;
		await stack.onSingleStep(stack, pc);
	}
	if(stack.suspended){
		await suspendThread(stack);
	}
	const method=stack.peek().method;
	if(method.breakpoints){
		if(method.breakpoints[pc]){
			stack.peek().pc=pc;
			await jdwp.breakpoint(stack, method. breakpoints[pc]);
		}
	}
}
