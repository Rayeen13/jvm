"use strict";
self.
init_classloader=async function(cl){
jvm.inits.push(async function(cl){
	self.java_net_URLClassLoader=await cl.loadClass("java/net/URLClassLoader",true);
	self.java_net_URLClassLoader.bindNatives();
});
}

async function java_net_URLClassLoader_init(stack, $this, urls){
	const url=urls[0];
	const clID=url.fields.spec0.str;
	if(self["cl"+clID]){
		$this.cl=self["cl"+clID];
		return;
	}
	return new Promise(function(resolve, reject){
		var script = document.createElement('script');
		script.onload = async function(){
			const cl=self["cl"+clID];
			$this.cl=cl;
			$this.cl.clID=parseInt(clID);
			const initFunction=self["init_cl"+clID];
			if(initFunction){
				await initFunction();
				self["init_cl"+clID]=undefined;
			}
			jvm.classloaders.push(cl);
			resolve();
		};
		script.src = "/jsjvm?cmd=GETJS&cl="+clID;
		document.head.appendChild(script);
	});
}

async function java_net_URLClassLoader_loadClass(stack, $this, classname){
	const clazz=await $this.cl.loadClass(classname.str);
	return clazz.getInstance();
}
