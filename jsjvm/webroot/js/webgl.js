"use strict";
self.
init_webgl=async function(cl){
let js_webgl_GL = await cl.loadClass("js/webgl/GL",true);
js_webgl_GL.bindNatives();
}
function js_webgl_GL_init(stack, $this, element){
	const canvas = element.obj;
	const gl = canvas.getContext('webgl2', {stencil:true});
	$this.obj=gl;
}
function js_webgl_GL_viewport(stack, $this, x, y, w, h){
	$this.obj.viewport(x, y, w, h);
}
function js_webgl_GL_useProgram(stack, $this, id){
	$this.obj.useProgram(id);
}
function js_webgl_GL_bufferData(stack, $this, id, array, len){
	$this.obj.bufferData(id, array, len);
}
function js_webgl_GL_createBuffer(stack, $this){
	return $this.obj.createBuffer();
}
function js_webgl_GL_createVertexArray(stack, $this){
	return $this.obj.createVertexArray();
}
function js_webgl_GL_createShader(stack, $this, type){
	return $this.obj.createShader(type);
}
function js_webgl_GL_createTexture(stack, $this){
	return $this.obj.createTexture();
}
function js_webgl_GL_texImage2D(stack, $this,  target, level, internalformat, format, type, image){
	return $this.obj.texImage2D(target, level, internalformat, format, type, image.obj);
}
function js_webgl_GL_generateMipmap(stack, $this,  target){
	return $this.obj.generateMipmap(target);
}
function js_webgl_GL_createProgram(stack, $this){
	return $this.obj.createProgram();
}
function js_webgl_GL_linkProgram(stack, $this, id){
	$this.obj.linkProgram(id);
}
function js_webgl_GL_shaderSource(stack, $this, id, source){
	$this.obj.shaderSource(id, source.str);
}
function js_webgl_GL_compileShader(stack, $this, shader){
	const gl=$this.obj;
	gl.compileShader(shader);
	var compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	//console.log('Shader compiled successfully: ' + compiled);
	var compilationLog = gl.getShaderInfoLog(shader);
	if(!compiled)
		console.log('Shader compiler log: ' + compilationLog);

}
function js_webgl_GL_attachShader(stack, $this, pid, sid){
	$this.obj.attachShader(pid, sid);
}
function js_webgl_GL_bindFrameBuffer(stack, $this, i, fb){
	$this.obj.bindFrameBuffer(i, fb);
}
function js_webgl_GL_framebufferTexture2D(stack, target, attachment, textarget, texture, level){
	$this.obj.framebufferTexture2D(target, attachment, textarget, texture, level);
}
function js_webgl_GL_bindBuffer(stack, $this, i, id){
	$this.obj.bindBuffer(i, id);
}
function js_webgl_GL_bindVertexArray(stack, $this, id){
	$this.obj.bindVertexArray(id);
}
function js_webgl_GL_activeTexture(stack, $this, id){
	$this.obj.activeTexture(id);
}
function js_webgl_GL_bindTexture(stack, $this, type, id){
	$this.obj.bindTexture(type, id);
}
function js_webgl_GL_texParameteri(stack, $this, v1, v2, v3){
	$this.obj.texParameteri(v1, v2, v3);
}
function js_webgl_GL_clearColor(stack,$this, r, g, b, a){
	$this.obj.clearColor(r, g, b, a);
}
function js_webgl_GL_clearDepth(stack, $this, d){
	$this.obj.clearDepth(d);
}
function js_webgl_GL_getParameter(stack, $this, param){
	return $this.obj.getParameter(param);
}
function js_webgl_GL_enable(stack, $this, prop){
	$this.obj.enable(prop);
}
function js_webgl_GL_depthFunc(stack, $this, type){
	$this.obj.depthFunc(type);
}
function js_webgl_GL_colorMask(stack, $this, r, g, b, a){
	$this.obj.colorMask(r, g, b, a);
}
function js_webgl_GL_stencilMask(stack, $this, type){
	$this.obj.stencilMask(type);
}
function js_webgl_GL_depthMask(stack, $this, z){
	$this.obj.depthMask(z);
}
function js_webgl_GL_stencilMaskSeparate(stack, $this, v1, v2){
	$this.obj.stencilMaskSeparate(v1, v2);
}
function js_webgl_GL_stencilFunc(stack, $this, v1, v2, v3){
	$this.obj.stencilFunc(v1, v2, v3);
}
function js_webgl_GL_stencilFuncSeparate(stack, $this, v1, v2, v3){
	$this.obj.stencilFuncSeparate(v1, v2, v3);
}
function js_webgl_GL_stencilOp(stack, $this, v1, v2, v3){
	$this.obj.stencilOp(v1, v2, v3);
}
function js_webgl_GL_clear(stack, $this, mask){
	$this.obj.clear(mask);
}
function js_webgl_GL_getAttribLocation(stack, $this, pid, name){
	return ($this.obj.getAttribLocation(pid, name.str));
}
function js_webgl_GL_getUniformLocation(stack, $this, pid, name){
	return $this.obj.getUniformLocation(pid, name.str);
}
function js_webgl_GL_vertexAttribPointer(stack, $this, v1, v2, v3, v4, v5, v6){
	$this.obj.vertexAttribPointer(v1, v2, v3, v4, v5, v6);
}
function js_webgl_GL_enableVertexAttribArray(stack,$this, id){
	$this.obj.enableVertexAttribArray(id);
}
function js_webgl_GL_uniform1i(stack, $this, id, value){
	$this.obj.uniform1i(id, value);
}
function js_webgl_GL_uniform1f(stack, $this, id, value){
	$this.obj.uniform1f(id, value);
}
function js_webgl_GL_uniform3f(stack, $this, id, f1, f2, f3){
	$this.obj.uniform3f(id, f1, f2, f3);
}
function js_webgl_GL_uniformMatrix4fv(stack, $this, id, transposed, array){
	$this.obj.uniformMatrix4fv(id, transposed, array);
}
function js_webgl_GL_drawArrays(stack ,$this, v1, v2, v3){
	$this.obj.drawArrays(v1, v2, v3);
}
function js_webgl_GL_drawElements(stack, $this, v1, v2, v3, v4){
	$this.obj.drawElements(v1, v2, v3, v4);
}
