async function init_json(cl){
self.org_json_JSONObject = await cl.loadClass("org/json/JSONObject",true);
self.org_json_JSONArray = await cl.loadClass("org/json/JSONArray",true);
org_json_JSONArray.bindNatives();
org_json_JSONObject.bindNatives();
self.org_json_JSONException = await cl.loadClass("org/json/JSONException",true);
jvm.inits.push(function(cl){
cl.
dn(org_json_JSONObject, "toString()Ljava/lang/String;" , async function(stack, $this){
	try{
		return (new JString(JSON.stringify($this.obj)));
	}
	catch(e){
		throw await tNE(stack,"org/json/JSONException", e.message);
	}
});
cl.
dn(org_json_JSONObject, "getKeys()[Ljava/lang/String;" , function(stack, $this){
	const keys=Object.keys($this.obj);
	const len=keys.length;
	const sArr=new Array(len);
	for(let i=0;i<len;++i)
		sArr[i]=new JString(keys[i]);
	return sArr;
});
cl.
dn(org_json_JSONObject, "has(Ljava/lang/String;)Z", function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
		return 0;
	else
		return 1;
});
cl.
dn(org_json_JSONObject, "remove(Ljava/lang/String;)Ljava/lang/Object;" , function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined){
		return 0;
	}
	else
	{
		$this.obj[key.str]=undefined;
		switch(typeof value){
			case "string":
				return new JString(value);
			case "object":
				const child=new ClassInstance(org_json_JSONObject);
				child.obj=value;
				return child;
			case "boolean":
				return new JBoolean(value);
			case "number":
				if(Number.isInteger(value))
					return new JInteger(value);
				else
					return new JFloat(value);
			default:
				debugger;
				console.log(typeof value);
				throw "unknown type";
		}
	}
});
cl.
dn(org_json_JSONObject, "get(Ljava/lang/String;)Ljava/lang/Object;" , function(stack, $this, key){
	const value=$this.obj[key.str];
	switch(typeof value){
		case "string":
			return new JString(value);
		case "object":
			let child;
			if(Array.isArray(value))
				child=new ClassInstance(org_json_JSONArray);
			else
				child=new ClassInstance(org_json_JSONObject);
			child.obj=value;
			return child;
		case "boolean":
			return new JBoolean(value);
		case "number":
			if(Number.isInteger(value))
				return new JInteger(value);
			else
				return new JFloat(value);
		default:
			debugger;
			console.log(typeof value);
			throw "unknown type";
	}
});
cl.
dn(org_json_JSONObject, "getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined){
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not defined");
	}
	const child=new ClassInstance(org_json_JSONObject);
	child.obj=value;
	return child;
});
cl.
dn(org_json_JSONObject, "getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined){
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not defined");
	}
	const child=new ClassInstance(org_json_JSONArray);
	child.obj=value;
	return child;
});
cl.
dn(org_json_JSONObject, "getString(Ljava/lang/String;)Ljava/lang/String;" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
	{
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not defined");
	}
	if(typeof value === 'string')
		return new JString(value);
	throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] is not a string");
});
cl.
dn(org_json_JSONObject, "optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;" , async function(stack, $this, key, def){
	const value=$this.obj[key.str];
	if(value===undefined)
		return def;
	else
		return new JString(value);
});
cl.
dn(org_json_JSONObject, "getInt(Ljava/lang/String;)I" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
	{
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not found");
	}
	else{
		return value|0;
	}
});
cl.
dn(org_json_JSONObject, "getDouble(Ljava/lang/String;)D" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
	{
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not found");
	}
	else{
		return value;
	}
});
cl.
dn(org_json_JSONObject, "getLong(Ljava/lang/String;)J" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
	{
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not found");
	}
	else{
		return BigInt(value);
	}
});
cl.
dn(org_json_JSONObject, "getFloat(Ljava/lang/String;)F" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
	{
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not found");
	}
	else{
		if((typeof value)=="number")
			return value;
		else
			return 0.0;
	}
});
cl.
dn(org_json_JSONObject, "getBoolean(Ljava/lang/String;)Z" , async function(stack, $this, key){
	const value=$this.obj[key.str];
	if(value===undefined)
	{
		throw await tNE(stack,"org/json/JSONException", "JSONObject["+key+"] not found");
	}
	else{
		return value?1:0;
	}
});
cl.
dn(org_json_JSONObject, "put(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;" , function(stack, $this, key, value){
	$this.obj[key.str]=value.str;
	return $this;
}, true);

cl.
dn(org_json_JSONObject, "put(Ljava/lang/String;I)Lorg/json/JSONObject;" , function(stack, $this, key, value){
       $this.obj[key.str]=value;
       return $this;
}, true);
cl.
dn(org_json_JSONObject, "put(Ljava/lang/String;J)Lorg/json/JSONObject;" ,function(stack, $this, key, value){
       $this.obj[key.str]=Number(value);
       return $this;
}, true);

cl.
dn(org_json_JSONObject, "put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;" , function(stack, $this, key, obj){
	let value=obj;
	if(value instanceof ClassInstance){ 
		switch(value.clazz.name){
			case "java/lang/String":
				value=value.str;
				break;
			case "org/json/JSONObject":
			case "org/json/JSONArray":
				value=value.obj;
				break;
			case "java/lang/Integer":
			case "java/lang/Float":
			case "java/lang/Double":
			case "java/lang/Byte":
			case "java/lang/Character":
			case "java/lang/Short":
				value=value.value;
				break;
		}
	}
	$this.obj[key.str]=value;
	return $this;
},true);
});
}

function org_json_JSONObject_init(stack, $this){
		$this.obj={};
}

async function org_json_JSONObject_initFromString(stack, $this, str){
	const json=str.str;
	try{
		$this.obj=JSON.parse(json);
	}
	catch(e){
		throw await tNE(stack,"org/json/JSONException", e.message);
	}
}

function org_json_JSONArray_putObject(stack, $this, obj){
	let value=obj;
	if(value instanceof ClassInstance){ 
		switch(value.clazz.name){
			case "java/lang/String":
				value=value.str;
				break;
			case "org/json/JSONObject":
			case "org/json/JSONArray":
				value=value.obj;
				break;
			case "java/lang/Integer":
			case "java/lang/Float":
			case "java/lang/Double":
			case "java/lang/Byte":
			case "java/lang/Character":
			case "java/lang/Short":
				value=value.value;
				break;
			default: throw "unknown value type";
		}
	}
	$this.obj.push(value);
}

function org_json_JSONArray_init(stack, $this, str){
	$this.obj=[]
}

async function org_json_JSONArray_initFromString(stack, $this, str){
	let arr;
	try{
		arr=JSON.parse(str.str);
	}
	catch(e){
		throw await tNE(stack,"org/json/JSONException", e.message);
	}
	if(!Array.isArray(arr)){
		throw await tNE(stack,"org/json/JSONException", "not a json array");
	}
	$this.obj=arr;
}

async function org_json_JSONArray_toString(stack, $this){
	try{
		return new JString(JSON.stringify($this.obj));
	}
	catch(e){
		throw await tNE(stack,"org/json/JSONException", e.message);
	}
}
function org_json_JSONArray_length(stack, $this){
	return $this.obj.length;
}

function org_json_JSONArray_isEmpty(stack, $this){
	return $this.obj.length===0?1:0;
}

function org_json_JSONArray_getString(stack, $this, index){
	const value=$this.obj[index];
	return new JString(value);
}

function org_json_JSONArray_get(stack, $this, index){
	const value=$this.obj[index];
	switch(typeof value){
		case "string":
			return new JString(value);
		case "object":
			const child=new ClassInstance(org_json_JSONObject);
			child.obj=value;
			return child;
		case "boolean":
			return new JBoolean(value);
		case "number":
			if(Number.isInteger(value))
				return new JInteger(value);
			else
				return new JFloat(value);
		default:
			debugger;
			console.log(typeof value);
			throw "unknown type";
	}
}

function org_json_JSONArray_getJSONObject(stack, $this, index){
	const value=$this.obj[index];
	const child=new ClassInstance(org_json_JSONObject);
	child.obj=value;
	return child;
}
