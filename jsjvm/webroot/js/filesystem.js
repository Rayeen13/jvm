"use strict";
class LocalFile{
	constructor(file){
		this.file=file;
		this.reader = new FileReader();
		this.size=file.size;
		this.offset=0;
	}
	async open(){}
	async close(){}
	async read(offset){
		if(this.offset>=this.size){
			return -1;
		}
		const $this=this;
		return new Promise(function(resolve, reject){
			$this.reader.onload = function() {
				var arrayBuffer = this.result;
				let array = new Int8Array(arrayBuffer);
				//binaryString = String.fromCharCode.apply(null, array);

				//console.log(binaryString);
				++$this.offset;
				resolve(array[0]);

			}
			$this.reader.readAsArrayBuffer($this.file.slice(offset, offset + 1));
		});
	}
	async readarr(array, arroffset, len){
		let offset=this.offset;
		if(offset>=this.size){
			return -1;
		}
		const $this=this;
		return new Promise(function(resolve, reject){
			$this.reader.onload = function() {
				var arrayBuffer = this.result;
				let buf = new Int8Array(arrayBuffer);
				const readen=buf.byteLength;
				for(let i=0;i<readen;++i){
					array[arroffset+i]=buf[i];
				}
				$this.offset+=readen;
				resolve(readen);

			}
			$this.reader.readAsArrayBuffer($this.file.slice(offset, offset + len));
		});
	}
	async write(offset, b){
	}
}
class RemoteFile{
		constructor(path){
				this.path=path;
				this.closed=false;
		}
		async open(){
				var loc = window.location, new_uri;
				if (loc.protocol === "https:") {
						new_uri = "wss:";
				} else {
						new_uri = "ws:";
				}
				new_uri += "//"+loc.host+"/fs";
				const $this=this;
				return new Promise(async function(resolve, reject){
						try{
								$this.socket=new WebSocket(new_uri);
						}
						catch(error){
								reject(error);
						}
						$this.socket.onopen=function(ev){
								$this.socket.send($this.path);
						}
						$this.socket.onclose=function(ev){
								$this.closed=true;
								if($this.onClose) $this.onClose();
						}
						$this.socket.onmessage=async function(ev){
								const arr=new Int8Array(await ev.data.arrayBuffer());
								if(arr[0]==0){
										resolve();
										return;
								}
								switch(arr[0]){
										case -1:
												reject();
												break;
										case -2:
												reject();
												break;
										default:
												reject();
												break;
								}
						}
						$this.socket.onerror=function(error){
								$this.closed=true;
								reject(error);
						}
				});
		}
		async close(){
				debugger;
				if(this.closed) throw "file was already closed";
				const $this=this;
				return new Promise(function(resolve, reject){
						$this.onClose=function(){
								resolve();
						}
						$this.socket.close();
				});
		}
		async read(offset){
				const file=this.array;
				if(offset>file.byteLength)
						return -1;
				else
						callback(file[offset]);
		}
		async readarr(array, offset, len){
				const $this=this;
				return new Promise(function(resolve, reject){
						$this.onClose=function(){
								reject();
						}
						$this.socket.onmessage=async function(ev){
								const arr=new Int8Array(await ev.data.arrayBuffer());
								if(arr[0]!=0){
										reject();
										return;
								}
								const len=arr.byteLength;
								for(let i=1;i<len;++i){
									array[i-1]=arr[i];
								}
								$this.onClose=false;
								resolve();
						}
						let buf = new ArrayBuffer(5);
						let view = new DataView(buf);
						view.setInt8(0, 3);
						view.setInt32(1, len, false);
						let arr=new Int8Array(buf);
						$this.socket.send(arr);

				});
				/*
				const file=this.array;
				if(offset>file.byteLength)
						return -1;
				else{
						readen=file.slice(offset,offset+len);
						len=Math.min(len, file.byteLength);
						for(let i=0;i<len;++i){
								array[offset+i]=file[i];
						}
						return len;
				}
				*/
		}
		async write(offset, b, callback){
		}
		async writearr(array, arroffset, len){
				if(this.closed) throw "file closed";
				const arr=new Int8Array(len+1);
				try{
						arr.set(Int8Array.from(array.slice(arroffset, arroffset+len)),1);
				}
				catch(error){
						debugger;
				}
				arr[0]=2;
				const $this=this;
				return new Promise(function(resolve, reject){
						$this.socket.send(arr);
						resolve();
				});
		}
}
class FileSystem{
	constructor(){
		this.opened_files={};
		this.fd=1;
	}
	async open(jsfile){
		let fd=this.fd++;
		const file=new LocalFile(jsfile);
		this.opened_files[fd]=file;
		await file.open();
		return fd;
	}
	async openRemote(path, create_if_not_exists){
		let fd=this.fd++;
		const file=new RemoteFile(path);
		this.opened_files[fd]=file;
		await file.open();
		return fd;
	}
	async close(fd){
		debugger;
		let file=this.opened_files[fd];
		if(!file){
			throw "was not oppened";
		}
		await file.close();
		delete this.opened_files[fd];
	}
	async read(fd, offset){
		const file=this.opened_files[fd];	
		return await file.read(offset);
	}
	async readarr(fd, array, offset, len){
		const file=this.opened_files[fd];	
		return await file.readarr(array, offset, len);
	}
	async write(fd, offset, b){
	}
	async writearr(fd, array, offset, len){
		const file=this.opened_files[fd];	
		return await file.writearr(array, offset, len);
	}
}
const fs=new FileSystem();

self.
init_filesystem=async function (jvm){
let java_io_File = await jvm.getClass("java/io/File",true);
let java_io_InputStream = await jvm.getClass("java/io/InputStream",true);
let java_io_FileInputStream = await jvm.getClass("java/io/FileInputStream",true);
let java_io_OutputStream = await jvm.getClass("java/io/OutputStream",true);
let java_io_FileOutputStream = await jvm.getClass("java/io/FileOutputStream",true);
jvm.inits.push(async function(jvm){
	/*
jvm.
dn(js_io_JSFileInputStream, "open(Ljs/JSObject;)V" , [_L], async function(stack,lv,pF){
	debugger;
	try{
		lv[0].reader = new FileReader();
		lv[0].file=lv[1].obj;
		lv[0].size=lv[1].obj.size;
		lv[0].offset=0;
	}
	catch(err){
	
		throw await tNE(stack,"java/io/IOException", err+"");
	}
});
jvm.
dn(js_io_JSFileInputStream, "close()V" , [], async function(stack,lv,pF){
});
jvm.
dn(js_io_JSFileInputStream, "read()I" , [], async function(stack,lv,pF){
	const $this=lv[0];
	let offset=$this.offset;
	if($this.offset>=$this.size){
		stack.peek().push(-1);
		return;
	}
	return new Promise(function(resolve, reject){
		$this.reader.onload = function() {
			var arrayBuffer = this.result;
			let array = new Int8Array(arrayBuffer);
			//binaryString = String.fromCharCode.apply(null, array);

			//console.log(binaryString);
			++$this.offset;
			stack.peek().push(array[0]);
			resolve();

		}
		$this.reader.readAsArrayBuffer($this.file.slice(offset, offset + 1));
	});
});
jvm.
dn(js_io_JSFileInputStream, "read([BII)I" , [_ARR, _I, _I], async function(stack,lv,pF){
	const $this=lv[0];
	let offset=$this.offset;
	if($this.offset>=$this.size){
		stack.peek().push(-1);
		return;
	}
	return new Promise(function(resolve, reject){
		$this.reader.onload = function() {
			var arrayBuffer = this.result;
			let array = new Int8Array(arrayBuffer);
			const readen=array.byteLength;
			for(let i=0;i<readen;++i){
				lv[1].set(lv[2]+i, array[i]);
			}
			$this.offset+=readen;
			stack.peek().push(readen);
			resolve();

		}
		$this.reader.readAsArrayBuffer($this.file.slice(offset, offset + lv[3]));
	});
});
*/
jvm.
dn(java_io_FileInputStream, "open(Ljava/io/File;)I" , [_L], async function(stack, $this, file){
	const fci=file;
	const path=fci.fields.path.str;
	try{
		let fd;
		if(file.clazz.name==="js/io/JSFile")
			fd=await fs.open(file.fields.jsfile.obj);
		else
			try{
				fd=await fs.openRemote(path);
			}catch(error){
				throw await tNE(stack, "java/io/IOException", error+"");
			}
		$this.fields.fd=fd;
		return fd;
	}
	catch(err){
	
		throw await tNE(stack,"java/io/IOException", err+"");
	}
});
jvm.
dn(java_io_FileInputStream, "close()V" , [], async function(stack, $this){
	const fd=$this.fields.fd;
	await fs.close(fd);
});
jvm.
dn(java_io_FileInputStream, "read()I" , [], async function(stack, $this){
	const fis=$this;
	const fd=fis.fields.fd;
	let b=await fs.read(fd, offset);
	return (b);
});
jvm.
dn(java_io_FileInputStream, "read([BII)I" , [_ARR, _I, _I], async function(stack, $this, arr, off, len){
	const fis=$this;
	const fd=fis.fields.fd;
	return await fs.readarr(fd, arr, off, len);
});
jvm.
dn(java_io_FileOutputStream, "open(Ljava/io/File;)V" , [_L], async function(stack, $this, file){
	const path=file.fields.path.str;
	try{
		let fd;
		if(file.clazz.name==="js/io/JSFile")
			fd=await fs.open(file.fields.jsfile.obj);
		else
			try{
				fd=await fs.openRemote(path);
			}catch(error){
				throw await tNE(stack, "java/io/IOException", error.message);
			}
		$this.fields.fd=fd;
		return (fd);
	}
	catch(err){
	
		throw await tNE(stack,"java/io/IOException", err+"");
	}
});
jvm.
dn(java_io_FileOutputStream, "close()V" , [], async function(stack, $this){
	debugger;
	const fis=$this;
	const fd=fis.fields.fd;
	await fs.close(fd);
});
jvm.
dn(java_io_FileOutputStream, "write(I)V" , [_I], async function(stack, $this, value){
	const fd=$this.fields.fd;
	await fs.write(fd, value);
});
jvm.
dn(java_io_FileOutputStream, "write([BII)V" , [_ARR, _I, _I], async function(stack, $this, arr, off, len){
	const fos=$this;
	const fd=fos.fields.fd;
	await fs.writearr(fd, arr.arr, off, len);
});
jvm.
dn(java_io_File, "length()J" , [], async function(stack, file){
	const path=file.fields.path;
		return new Promise(function(resolve, reject){
				var xhr = $.ajax({
						type: "HEAD",
						url: path.str,
						success: function(msg){
								const flength=xhr.getResponseHeader('Content-Length');
								resolve(parseInt(flength));
						},
						error: function(){
								resolve(0);
						}
				});
		});
});
jvm.
dn(java_io_File, "getAbsolutePath()Ljava/lang/String;" , [], function(stack, $this){
	return $this.fields["path"];
});
jvm.
dn(java_io_File, "isDirectory()Z" , [], function(stack, $this){
	return 0;
});
jvm.
dn(java_io_File, "exists()Z" , [], function(stack, $this){
	const file=$this;
	const path=file.fields.path;
		return new Promise(function(resolve, reject){
				var xhr = $.ajax({
						type: "HEAD",
						url: path.str,
						success: function(msg){
								resolve(1);
						},
						error: function(){
								resolve(0);
						}
				});
		});
});
jvm.
dn(java_io_File, "canRead()Z" , [], function(stack, $this){
	const file=$this;
	const path=file.fields.path;
		return new Promise(function(resolve, reject){
				var xhr = $.ajax({
						type: "HEAD",
						url: path.str,
						success: function(msg){
								resolve(1);
						},
						error: function(){
								resolve(0);
						}
				});
		});
});
});
}
function java_io_FileInputStream_registerNatives(stack){}
function java_io_FileOutputStream_registerNatives(stack){}
function java_io_File_registerNatives(stack){}
