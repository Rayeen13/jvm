async function init_websocket(cl){
let js_net_websocket_WebSocket = await cl.loadClass("js/net/websocket/WebSocket",true);
jvm.inits.push(function(cl){
cl.
dn(js_net_websocket_WebSocket, "connect()V", async function(stack, $this){
	let url=$this.fields.url.str;
	if(url.charAt(0)=='/'){
		var loc = window.location, new_uri;
		if (loc.protocol === "https:") {
			new_uri = "wss:";
		} else {
			new_uri = "ws:";
		}
		new_uri += "//" + loc.host;
		new_uri += url;
		url=new_uri;
	}
	const onOpen=$this.fields.onOpen;
	const onMessage=$this.fields.onMessage;
	const onClose=$this.fields.onClose;
	try{
		const websocket=new WebSocket(url);
		$this.websocket=websocket;
		if(onOpen)
			websocket.onopen=function(){
				stack.dispatchEvent(function(){return onOpen});
			};
		if(onMessage)
			websocket.onmessage=function(message){
				stack.dispatchEvent(function(){
					onMessage.fields.message=new JString(message.data);
					return onMessage;
				});
			};
		if(onClose)
			websocket.onclose=function(){
				if(stack.dispatchEvent)
					stack.dispatchEvent(function(){return onClose});
			};
	}
	catch(e){
		debugger;
		throw _NE(stack, java_io_IOException, e.message);
	}
});

cl.
dn(js_net_websocket_WebSocket, "send(Ljava/lang/String;)V", function(stack, $this, url){
	try{
		$this.websocket.send(url.str);
	}
	catch(e){
		throw _NE(stack, java_io_IOException, e.message);
	}
});

cl.
dn(js_net_websocket_WebSocket, "close()V",function(stack, $this){
       if($this.websocket)
               $this.websocket.close();

});

});
}
