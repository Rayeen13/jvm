"use strict";
self.
init_zip=async function(jvm){
	await jvm.jvm_interface.loadLib("./zip/zip");
	await jvm.jvm_interface.loadLib("./zip/inflate");
jvm.inits.push(async function(jvm){
	const java_util_zip_ZipFile=await jvm.getClass("java/util/zip/ZipFile",false);
	const java_util_zip_ZipEntry=await jvm.getClass("java/util/zip/ZipEntry",false);
	const java_io_ByteArrayInputStream=await jvm.getClass("java/io/ByteArrayInputStream",false);
jvm.
dn(java_util_zip_ZipFile,"open(Ljava/lang/String;)J", [_L], async function(stack,lv){
	zip.useWebWorkers=false;
	var blob=await GETBIN(lv[0].str);
	// use a BlobReader to read the zip from a Blob object
	return new Promise(function(resolve, reject){
			zip.createReader(new zip.BlobReader(new Blob([blob])), function(reader) {

					// get all entries from the zip
					reader.getEntries(function(entries) {
							const _es={};
							lv[0].entries=_es;
							for(let i=0;i<entries.length;++i){
									const entry=entries[i];
									_es[entry.filename]=entry;
							}
							stack.peek().pushl(0);
							resolve();
					});
			}, function(error) {
					// onerror callback
					console.error(error);
					reject();
			});
	});
});
jvm.
dn(java_util_zip_ZipFile,"getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;", [_L], async function(stack,lv){
	const entry=new ClassInstance(java_util_zip_ZipEntry);
	entry.entry=lv[0].entries[lv[1].str];
	if(entry.entry===undefined)
		stack.peek().push(null);
	else
		stack.peek().push(entry);
	return false;
});
jvm.
dn(java_util_zip_ZipFile,"getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;", [_L], async function(stack,lv){
	const is=new ClassInstance(java_io_ByteArrayInputStream);
	const entry=lv[1].entry;
	const len=entry.uncompressedSize;
	const arr=new ArrayInstance(len,_B);
	is.fields.buf=arr;
	is.fields.pos=0;
	is.fields.count=len;
	is.fields.mark=0;
	return new Promise(function(resolve, reject){
			entry.getData(new zip.BlobWriter(), async function(blob) {
					// text contains the entry data as a String

					const buf=await blob.arrayBuffer();
					var int8View = new Int8Array(buf);

					arr.arr=int8View;
					// close the zip reader
					/*
					reader.close(function() {
					// onclose callback
					});*/
					stack.peek().push(is);
					resolve();

			}, function(current, total) {
					// onprogress callback
			});
	});
});
jvm.
dn(java_util_zip_ZipFile,"registerNatives()V", [], function(stack,lv){
	return false;
});
jvm.
dn(java_util_zip_ZipEntry,"getSize()J", [], async function(stack,lv){
	stack.peek().pushl(lv[0].entry.uncompressedSize);
	return false;
});

});
}
