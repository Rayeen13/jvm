"use strict";
self.
init_svg=async function(cl){
const js_svg_SVG= 		await cl.loadClass("js/svg/SVG",true);
js_svg_SVG.bindNatives();
const js_svg_SVGElement= 	await cl.loadClass("js/svg/SVGElement",true);
js_svg_SVGElement.bindNatives();
self.js_svg_Path= 		await cl.loadClass("js/svg/Path",true);
js_svg_Path.bindNatives();
self.js_svg_Group= 		await cl.loadClass("js/svg/Group",true);
self.js_svg_Line= 		await cl.loadClass("js/svg/Line",true);
self.js_svg_Rect= 		await cl.loadClass("js/svg/Rect",true);
self.js_svg_Image= 		await cl.loadClass("js/svg/Image",true);
self.js_svg_Circle= 		await cl.loadClass("js/svg/Circle",true);
self.js_svg_Text= 		await cl.loadClass("js/svg/Text",true);
const js_svg_PolyLine= 		await cl.loadClass("js/svg/PolyLine",true);
js_svg_PolyLine.bindNatives();
const js_svg_SVGPointList= 		await cl.loadClass("js/svg/SVGPointList",true);
js_svg_SVGPointList.bindNatives();
jvm.inits.push(function(cl){
});
}
const svgRoot=document.createElementNS('http://www.w3.org/2000/svg', 'svg');
function js_svg_SVG_createPath(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "path");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Path,4);
}
function js_svg_SVG_createPolyLine(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "polyline");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Path,4);
}
function js_svg_SVG_createGroup(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "g");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return (new JSObject(obj, js_svg_Group,4));
}
function js_svg_SVG_createLine(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "line");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Line,4);
}

function js_svg_SVG_createText(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "text");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Text,4);
}

function js_svg_SVG_createCircle(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "circle");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Circle,4);
}
function js_svg_SVG_createRect(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "rect");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Rect,4);
}
function js_svg_SVG_createImage(stack){
	const obj=document.createElementNS("http://www.w3.org/2000/svg", "image");
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	obj.transform.baseVal.appendItem(svgRoot.createSVGTransform());
	return new JSObject(obj,js_svg_Image,4);
}
function js_svg_SVGElement_translate(stack, $this, x, y){
	//$this.obj.setAttribute("transform","translate("+x+","+y+")");
	$this.obj.transform.baseVal.getItem(0).setTranslate(x,y);
}
function js_svg_SVGElement_rotate(stack, $this, angle, x, y){
	//$this.obj.setAttribute("transform","translate("+x+","+y+")");
	$this.obj.transform.baseVal.getItem(1).setRotate(angle, x,y);
}
function js_svg_SVGElement_scale(stack, $this, x, y){
	//$this.obj.setAttribute("transform","translate("+x+","+y+")");
	$this.obj.transform.baseVal.getItem(2).setScale(x,y);
}
function js_svg_SVGElement_getWidth(stack, $this){
	return $this.obj.getBBox().width;
}
function js_svg_SVGElement_getHeight(stack, $this){
	return $this.obj.getBBox().height;
}
function js_svg_PolyLine_getPoints(stack, $this){
	const pl=new JSObject($this.obj.points,js_svg_SVGPointList);
	pl.polyline=$this.obj;
	return pl;
}
function js_svg_SVGPointList_addPoint(stack, $this, x, y){
	var point = svg.createSVGPoint();
	point.x = x;
	point.y = y;
	$this.obj.appendItem(point);
}
function js_svg_Path_getSegmentList(stack, $this){
	return new JSObject($this.obj.getPathData());
}
