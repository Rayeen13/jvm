package com.cupvm.classbuilder;

public  class Type{
	public final String desc;
	public final boolean wide;
	public final boolean ref;
	public Type(String desc){
		char t=desc.charAt(0);	
		switch(t){
			case 'L':
				this.desc=desc;
				this.wide=false;
				this.ref=true;
				break;
			case '[':
				this.desc=desc;
				this.wide=false;
				this.ref=true;
				break;
			case 'I':
			case 'F':
			case 'S':
			case 'C':
			case 'B':
			case 'Z':
				this.desc=desc;
				this.wide=false;
				this.ref=false;
				break;
			case 'J':
			case 'D':
				this.desc=desc;
				this.wide=true;
				this.ref=false;
				break;
			default:
				throw new RuntimeException("unknown type desc: "+desc);
		}
	}
}
