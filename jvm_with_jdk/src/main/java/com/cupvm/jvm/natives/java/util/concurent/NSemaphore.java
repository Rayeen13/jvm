package com.cupvm.jvm.natives.java.util.concurent;
import java.util.concurrent.Semaphore;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm.instance.*;
import java.util.LinkedList;

public class NSemaphore{
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/util/concurrent/Semaphore.init(I)V",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance c=Heap.get(lv[0]);
				c.helper=new Semaphore(lv[1].intValue());
				return false;
			}
		});
		methods.add(new JNIMethod("java/util/concurrent/Semaphore.acquire()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance c=Heap.get(lv[0]);
				try{
					((Semaphore)c.helper).acquire();
				}catch(InterruptedException e){
					throw new RuntimeException(e);
				}
				return false;
			}
		});
		methods.add(new JNIMethod("java/util/concurrent/Semaphore.release()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance c=Heap.get(lv[0]);
				((Semaphore)c.helper).release();
				return false;
			}
		});
		return methods;
	}
}
