package com.cupvm.jvm.natives.java.lang.invoke;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;

import java.util.LinkedList;

public class NMethodHandle {
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/invoke/MethodHandle.invokeExact(Ljava/lang/String;I)Ljava/lang/String;",2) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(0L);
				return false;
			}
		});
		return methods;
	}
}
