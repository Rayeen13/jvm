package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.NativeFrame;
import com.cupvm.jni.ThreadInterface;

public class NClassLoader {
	public static boolean invoke(String methodname, NativeFrame cF, Object[] lv, ThreadInterface t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "findBuiltinLib(Ljava/lang/String;)Ljava/lang/String;":
				findBuiltinLib(cF, lv, t);
				return false;
		}
		throw new RuntimeException("method " + methodname + " was not found");
	}

	private static void findBuiltinLib(NativeFrame cF, Object[] lv, ThreadInterface t) {
		// TODO: Implement this method
	}

	private static void registerNatives(NativeFrame cF, Object[] lv, ThreadInterface t) {

	}

}
