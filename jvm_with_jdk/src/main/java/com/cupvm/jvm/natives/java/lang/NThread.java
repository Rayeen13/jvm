package com.cupvm.jvm.natives.java.lang;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.instance.*;
import com.cupvm.jvm.instance.natives.*;

import java.util.LinkedList;

import static com.cupvm.jvm.Globals.aconst_null;

public class NThread {
	private static final Logger logger= ConsoleLogger.get(JVM.logColor);
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/Thread.init()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				ClassLoader classLoader = t.classLoader;
				Instance i=Heap.get(lv[0]);
				Class c=(Class)classLoader.loadClass("java/lang/Thread");
				MyThread thread = new MyThread(c,classLoader, "main",null);
				i.helper=thread;
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Thread.currentThread()Ljava/lang/Thread;",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(aconst_null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Thread.start()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				MyThread thread = (MyThread)i.helper;
				thread.setStaticMethodToRun("start(Ljava/lang/Thread;)V", lv[0]);
				thread.start();
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Thread.setName(Ljava/lang/String;)V",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				MyThread thread=(MyThread)i.helper;
				JString name=(JString)Heap.get(lv[1]);
				thread.setName(name.s);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Thread.interrupt()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				MyThread thread = (MyThread)i.helper;
				thread.interrupt();
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Thread.join()V",0){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				MyThread thread=(MyThread)i.helper;
				try{
					if(JVM.verbose)
						logger.info("joining thread "+thread.getName());
					thread.join();
				}
				catch(InterruptedException e){
					throw new RuntimeException(e);
				}
				return false;
			}
		});
		return methods;
	}
}
