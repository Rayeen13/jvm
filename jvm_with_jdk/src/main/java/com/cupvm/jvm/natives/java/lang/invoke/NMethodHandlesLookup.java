package com.cupvm.jvm.natives.java.lang.invoke;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm._class.*;
import com.cupvm.jvm.instance.*;
import com.cupvm.jvm.instance.arrays.ObjectArrayInstance;
import com.cupvm.jvm.instance.natives.JClass;
import com.cupvm.jvm.instance.natives.JString;
import com.cupvm.jvm.method.Method;

import java.util.LinkedList;

public class NMethodHandlesLookup {
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/invoke/MethodHandles$Lookup.findVirtual(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;",3) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JClass clazz = (JClass) Heap.get(lv[1]);
				JString name= (JString) Heap.get(lv[2]);
				Instance mt = Heap.get(lv[3]);
				JClass rtype = (JClass) Heap.get(mt.getRef("rtype"));
				ObjectArrayInstance ptypes = (ObjectArrayInstance) Heap.get(mt.getRef("ptypes"));
				StringBuilder mtype=new StringBuilder();
				mtype.append("(");
				for(Instance i:ptypes){
					JClass tc= (JClass) i;
					mtype.append(tc.getJNISignature());
				}
				mtype.append(")");
				mtype.append(rtype.getJNISignature());
				final MethodHandleClass mhc = new MethodHandleClass(t.classLoader);
				Method m = (Method) clazz.the_clazz.getMethod(name.s + mtype.toString());
				mhc.setMethod(m);
				Instance i=new Instance(mhc);
				cF.push(Heap.add(i));
				return false;
			}
		});
		return methods;
	}
}
