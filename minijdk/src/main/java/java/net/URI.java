package java.net;

public class URI {
	private final String spec;
	public URI(String spec){
		this.spec=spec;
		if(spec==null)
			System.out.println("spec was null in URI CONSTRUCTOR::::::::::::::::::::!");
	}
	public String toString(){
		return spec;
	}
	public URL toURL()throws MalformedURLException{
		return new URL(spec);
	}
}
