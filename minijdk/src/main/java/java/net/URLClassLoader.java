package java.net;

public class URLClassLoader extends ClassLoader{
	static{
		try{
			System.loadLibrary("classloader");
		}
		catch (UnsatisfiedLinkError e){
			throw new RuntimeException(e);
		}
	}
	public URLClassLoader(URL[] urls){
		init(urls);	
	}
	private native void init(URL[] urls);
	public native Class<?> loadClass(String name) throws ClassNotFoundException;
}
