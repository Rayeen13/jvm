package java.net;
import java.io.*;

public class Socket {
	public native InputStream getInputStream();
	public native OutputStream getOutputStream();
}
