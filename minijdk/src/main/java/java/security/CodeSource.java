package java.security;
import java.net.*;

public class CodeSource{
	private final URL location;
	public CodeSource(URL location){
		this.location=location;
	}
	public URL getLocation(){
		return location;
	}
}
