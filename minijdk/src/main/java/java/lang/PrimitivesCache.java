package java.lang;

class PrimitivesCache{
	static final int low = -128;
	static final int high;
	static final Integer[] intcache;
	static final Long[] longcache;
	static final Short[] shortcache;

	static {
		int h = 127;
		high = h;

		int length=(high - low) + 1;
		intcache = new Integer[length];
		longcache = new Long[length];
		shortcache = new Short[length];
		int j = low;
		for(int k = 0; k < length; ++k){
			intcache[k] = new Integer(j);
			longcache[k] = new Long(j);
			shortcache[k] = new Short((short)j);
			++j;
		}

		// range [-128, 127] must be interned (JLS7 5.1.7)
		assert high >= 127;
	}

	private PrimitivesCache() {}

}
