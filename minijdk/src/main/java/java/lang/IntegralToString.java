package java.lang;

class IntegralToString{
	static final char[] digits = {
       '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
       'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
       'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
       'u', 'v', 'w', 'x', 'y', 'z',
     };
	static String intToString(int num, int radix){
		if (radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
        radix = 10;
 
      // For negative numbers, print out the absolute value w/ a leading '-'.
      // Use an array large enough for a binary number.
      char[] buffer = new char[33];
      int i = 33;
      boolean isNeg = false;
      if (num < 0)
        {
          isNeg = true;
          num = -num;
 
          // When the value is MIN_VALUE, it overflows when made positive
          if (num < 0)
        {
          buffer[--i] = digits[(int) (-(num + radix) % radix)];
          num = -(num / radix);
        }
        }
 
      do
        {
          buffer[--i] = digits[num % radix];
          num /= radix;
        }
      while (num > 0);
 
      if (isNeg)
        buffer[--i] = '-';
 
      return new String(buffer, i, 33 - i);
 
    }
	public static String longToString(long num, int radix)
	{
		// Use the Integer toString for efficiency if possible.
		if ((int) num == num)
			return intToString((int) num, radix);

		if (radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
			radix = 10;

		// For negative numbers, print out the absolute value w/ a leading '-'.
		// Use an array large enough for a binary number.
		char[] buffer = new char[65];
		int i = 65;
		boolean isNeg = false;
		if (num < 0)
		{
			isNeg = true;
			num = -num;

			// When the value is MIN_VALUE, it overflows when made positive
			if (num < 0)
			{
				buffer[--i] = digits[(int) (-(num + radix) % radix)];
				num = -(num / radix);
			}
		}

		do
		{
			buffer[--i] = digits[(int) (num % radix)];
			num /= radix;
		}
		while (num > 0);

		if (isNeg)
			buffer[--i] = '-';

		// Package constructor avoids an array copy.
		return new String(buffer, i, 65 - i);
	}
}
