package java.lang.reflect;

public final class Constructor<T>
   //extends AccessibleObject
//  implements GenericDeclaration, Member
 {
   private Class<T> clazz;
   private int slot;
   
   private static final int CONSTRUCTOR_MODIFIERS
     = Modifier.PRIVATE | Modifier.PROTECTED | Modifier.PUBLIC;
 
   /**
    * This class is uninstantiable except from native code.
    */
   private Constructor(Class declaringClass,int slot)
   {
     this.clazz = declaringClass;
     this.slot = slot;
   }
 
   private Constructor()
   {
   }
   public native T newInstance(Object... args)
     throws InstantiationException, IllegalAccessException,
            InvocationTargetException;
 }
