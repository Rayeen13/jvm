package java.lang.invoke;

public abstract class MethodHandle{
	@interface PolymorphicSignature{}

    public final native @PolymorphicSignature Object invokeExact(Object... args) throws Throwable;
}
