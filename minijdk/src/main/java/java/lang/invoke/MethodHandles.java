package java.lang.invoke;

public class MethodHandles{
	public static final class Lookup {
		static{
			System.loadLibrary("invoke");
		}
		public native MethodHandle findVirtual(Class<?> clazz, String methodName, MethodType type) throws IllegalAccessException, NoSuchMethodException;
		public native MethodHandle findStatic(Class<?> refc,
                               String methodname,
                               MethodType type)
                        throws NoSuchMethodException,
                               IllegalAccessException;
	}
	public static MethodHandles.Lookup lookup() {
		return new Lookup();
	}
}

