package java.lang;
import org.jetbrains.annotations.*;

public class StringBuilder implements  Appendable, CharSequence{
	int count;
	char[] value;
	public StringBuilder(int capacity) {
		value=new char[16];
	}
	public StringBuilder(){
		value=new char[16];
	}
	public StringBuilder(String str){
		count = str.value.length;
		value = new char[count + 16];
		str.getChars(0, count, value, 0);
	}
	//public native void init(String s);
	//public native void init(int capacity);
	@NotNull
	public StringBuilder append(String str){
		if (str == null)
			str = "null";
		int len = str.value.length;
		char[] value=ensureCapacity(count + len);
		str.getChars(0, len, this.value, count);
		count += len;
		return this;
	}

	public StringBuilder append(CharSequence s){
		return append(s.toString());
	}
	public StringBuilder append(CharSequence s, int offset, int len){
		throw new RuntimeException("implement me");
	}

	public StringBuilder append(char[] data){
		return append(data, 0, data.length);
	
	}
	public StringBuilder append(char[] data, int offset, int count){
		//if (offset < 0 || count < 0 || offset > data.length - count)
		//throw new StringIndexOutOfBoundsException();
		ensureCapacity(this.count + count);
		System.arraycopy(data, offset, value, this.count, count);
		this.count += count;
		return this;
	}

	public native CharSequence subSequence(int offset, int len);
	public StringBuilder append(int i){
		return append(Integer.toString(i));
	}

	public StringBuilder append(float f){
		return append(Float.valueOf(f));
	}
	public StringBuilder append(char ch){
		ensureCapacity(count + 1);
		value[count++] = ch;
		return this;
	}

	public StringBuilder append(long j){
		return append(Long.toString(j));
	}
	public StringBuilder append(double d){
		return append(Double.valueOf(d));
	}
	public StringBuilder append(byte i){
		return append(Byte.valueOf(i));
	}
	public native int length();
	public native char charAt(int index);
	public StringBuilder append(boolean b){
		return append(b?"true":"false");
	}

	public StringBuilder append(Object o){
		return o==null?append("null"):append(o.toString());
	}
	public String toString(){
		return new String(value, 0, count);
	}
	public char[] ensureCapacity(int minimumCapacity){
		if (minimumCapacity > value.length){
			int max = (value.length * 3)/2 + 1;
			minimumCapacity = (minimumCapacity < max ? max : minimumCapacity);
			char[] nb = new char[minimumCapacity];
			 System.arraycopy(value, 0, nb, 0, count);

			value = nb;
			return nb;
		}
		return value;
	}
 
}
