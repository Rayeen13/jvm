package java.lang;
public final class Byte extends Number{
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("B");
	public static final short MIN_VALUE = -32768;
  public static final short MAX_VALUE = 32767;

	public static Byte valueOf(byte b){
		return new Byte(b);
	}
	public native static String toString(short s);

	private final byte value;
	Byte(byte value){
		this.value=value;
	}

	@Override
	public final int intValue(){
		return value;
	}
	@Override
	public final long longValue(){
		return value;
	}
	@Override
	public final float floatValue(){
		return (float)value;
	}
	@Override
	public final double doubleValue(){
		return (double)value;
	}
	@Override
	public final short shortValue(){
		return value;
	}
	@Override
	public final byte byteValue(){
		return value;
	}

	@Override
	public final boolean equals(Object o){
		if(o==this)return true;
		if(o instanceof Byte)
			return value==((Byte)o).value;
		return false;
	}
	@Override
	public final int hashCode(){
		return value;
	}
}
