package java.lang;

public final class Float extends Number {
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("F");
	public static final float POSITIVE_INFINITY = 1.0F / 0.0F;
	public static final float NEGATIVE_INFINITY = -1.0F / 0.0F;
	public static final float NaN = 0.0F / 0.0F;
	public static final float MAX_VALUE = 3.4028235E38F;
	public static final float MIN_NORMAL = 1.17549435E-38F;
	public static final float MIN_VALUE = 1.4E-45F;
	public static final int MAX_EXPONENT = 127;
	public static final int MIN_EXPONENT = -126;
	public native static Float valueOf(float f);
	public native static String toString(float f);
	public static Float valueOf(String s){
		return valueOf(parseFloat(s));	
	}

	public native static float parseFloat(String s) throws NumberFormatException;

	@Override
	public String toString(){
		return toString(floatValue());
	}

	public native static int floatToIntBits(float value);

	public native final float floatValue();

	@Override
	public native final double doubleValue();
	public native final int intValue();
	public native final long longValue();

	public native static boolean isNaN(float f);

	@Override
	public final native boolean equals(Object o);
	@Override
	public final native int hashCode();
}
