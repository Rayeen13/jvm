package java.lang;

public class ThreadLocal<T> {
    public ThreadLocal() {
    }

    public T get(){
    	T v=get0();
	if(v==null){
		v=initialValue();
		set(v);
	}
	return v;
    }
    public native T get0();
    public native void set(T value); 
    public native void remove();
    protected T initialValue() {
	    return null;
    }
}
