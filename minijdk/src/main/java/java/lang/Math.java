package java.lang;

public final class Math{
	static{
		System.loadLibrary("math");
	}
	public static final double E = 2.7182818284590452354;
	public static final double PI = 3.14159265358979323846;

	public native static double pow(double v, double exponent);
	public native static double exp(double v);
	public native static double log(double v);
	public native static double sin(double v);
	public native static double cos(double v);
	public native static double tan(double v);
	public native static double sqrt(double v);
	public native static double ceil(double v);
	public native static int round(float a);
	private native static long roundDouble(double a);
	public static long round(double a){
		return roundDouble(a);
	}

	public static int floorMod(int a, int b){
		return ((a%b)+b)%b;
	}

	public static int floorDiv(int x, int y) {
        int r = x / y;
        if ((x ^ y) < 0 && (r * y != x)) {
            r--;
        }
        return r;
    }

	public static double min(double f1, double f2){
		return f1<f2?f1:f2;
	}
	public static double max(double f1,double f2){
		return f1>f2?f1:f2;
	}
	public static long min(long f1, long f2){
		return f1<f2?f1:f2;
	}
	public static long max(long f1, long f2){
		return f1>f2?f1:f2;
	}
	public static float min(float f1,float f2){
		return f1<f2?f1:f2;
	}
	public static float max(float f1,float f2){
		return f1>f2?f1:f2;
	}
	public static int min(int a,int b){
		return a<b?a:b;
	}
	public static int max(int i1,int i2){
		return i1>i2?i1:i2;
	}
	public native static double random();
	public static double abs(double f) {
		return f < 0 ? 0 - f : f;
	}
	public static float abs(float f) {
		return f < 0 ? 0 - f : f;
	}
	public static long abs(long l) {
		return (l < 0) ? -l : l;
	}
}
