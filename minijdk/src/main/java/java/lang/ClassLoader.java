package java.lang;
import java.io.*;

public class ClassLoader{
	public native InputStream getResourceAsStream(String name);
	private static final ClassLoader classLoader=new ClassLoader();
	public static ClassLoader getSystemClassLoader(){
		return classLoader;
	}
	public native Class<?> loadClass(String name) throws ClassNotFoundException;
}
