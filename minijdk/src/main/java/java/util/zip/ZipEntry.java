package java.util.zip;

public class ZipEntry {
	private final String name;
	private final long id;
	ZipEntry(long id, String name){
		this.name=name;
		this.id=id;
	}

	public String getName(){
		return name;
	}

	public long getSize(){
		return ZipFile.getSize(id, name);
	}
}
