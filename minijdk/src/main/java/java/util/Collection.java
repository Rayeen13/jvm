package java.util;
import java.util.stream.Stream;
import java.util.function.*;
import java.util.stream.StreamSupport;

public interface Collection<E> extends Iterable<E> {
	Iterator<E> iterator();
	boolean add(E e);
	boolean addAll(Collection<? extends E> c);
	    boolean removeAll(Collection<?> c);

	boolean contains(Object o);
	    boolean containsAll(Collection<?> c);

	boolean remove(Object o);
	boolean isEmpty();
	    void clear();

	int size();
	    boolean retainAll(Collection<?> c);
		    Object[] toArray();
			    <T> T[] toArray(T[] a);



	default boolean removeIf(Predicate<? super E> filter) {
        Objects.requireNonNull(filter);
        boolean removed = false;
        final Iterator<E> each = iterator();
        while (each.hasNext()) {
            if (filter.test(each.next())) {
                each.remove();
                removed = true;
            }
        }
        return removed;
    }
	default Spliterator<E> spliterator() {
        return Spliterators.spliterator(this, 0);
    }

    default Stream<E> stream() {
        return StreamSupport.stream(this.spliterator(), false);
    }
	default Stream<E> parallelStream() {
        return StreamSupport.stream(spliterator(), true);
    }
}
