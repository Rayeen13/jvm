package java.util;
@FunctionalInterface
public interface Comparator<T> {
	int compare(T o1, T o2);
	boolean equals(Object obj);
	default Comparator<T> reversed() {
        return Collections.reverseOrder(this);
    }
	@SuppressWarnings("unchecked")
    public static <T extends Comparable<? super T>> Comparator<T> naturalOrder() {
			return null;
        //return (Comparator<T>) Comparators.NaturalOrderComparator.INSTANCE;
    }
}
