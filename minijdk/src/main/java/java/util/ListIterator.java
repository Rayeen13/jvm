package java.util;

public interface ListIterator<E> extends Iterator<E>{

    boolean hasPrevious();

    E previous();

    int nextIndex();

    int previousIndex();

    void remove();

    void set(E var1);

    void add(E var1);
}
