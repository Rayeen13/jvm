package java.util;

public class HashSet<E> extends AbstractSet<E>
		implements Set<E>{
	private final HashMap<E,Object> elements;
	private static final Object DUMMY=new Object();
	public HashSet(){
		elements=new HashMap<>();
	}
	public HashSet(int initialSize){
		elements=new HashMap<>(initialSize);
	}
	public int size(){
		return elements.size();
	}
	public HashSet(Collection<E> c){
			/*
			if(c instanceof HashSet){
				this.elements=((HashSet)c).elements;
				return;
			}*/
		elements=new HashMap<>();
		addAll(c);
	}
	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private final Iterator<Map.Entry<E, Object>> it=elements.entrySet().iterator();
			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public E next() {
				return it.next().getKey();
			}
			public void remove(){}
        };
	}

	@Override
	public boolean add(E e) {
		if(elements.containsKey(e))
			return false;
		elements.put(e,DUMMY);
		return true;
	}


	@Override
	public boolean contains(Object o) {
		return elements.containsKey(o);
	}

	@Override
	public boolean remove(Object o) {
		return elements.remove(o)!=null;
	}
}
