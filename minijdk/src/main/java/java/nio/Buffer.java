package java.nio;

public abstract class Buffer{
	Buffer(int capacity, int limit, int position, int mark){
		if(capacity<0) throw new IllegalArgumentException();
		this.capacity=capacity;
	}
	private int position;
	private int limit;
	private int capacity;
	private int mark=-1;
	public Buffer limit(int newLimit){
		if ((newLimit < 0) || (newLimit > capacity))
			throw new IllegalArgumentException ();

		if (newLimit < mark)
			mark = -1;

		if (position > newLimit)
			position = newLimit;

		limit = newLimit;
		return this;
	}
	public int position(){
		return position;
	}
	public int limit(){
		return limit;
	}
	public int capacity(){
		return capacity;
	}
	public Buffer clear(){
		limit=capacity;	
		position=0;
		mark=-1;
		return this;
	}
	public Buffer flip(){
		limit=position;	
		position=0;
		mark=-1;
		return this;
	}
	public Buffer mark(){
		mark=position;
		return this;
	}
	public Buffer position(int newPosition){
		if(newPosition<0 || newPosition>limit)
			throw new IllegalArgumentException();
		if (newPosition <= mark)
			mark = -1;
		position=newPosition;
		return this;
	}
	public boolean hasRemaining(){
		return remaining()>0;
	}
	public int remaining(){
		return limit - position;
	}
	public Buffer reset(){
		if(mark==-1)
			throw new RuntimeException("Invalid Mark");
		position=mark;
		return this;
	}
	public Buffer rewind(){
		position=0;
		mark=-1;
		return this;
	}
}
