package com.cupvm.jvm;

public interface FrameIntf {
    int getID();

    int getClassID();

    int getMethodID();

    int getPC();

    Object[] getLocalVariables();
}
