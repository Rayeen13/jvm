package com.cupvm.jvm;

import java.util.List;

public interface MethodIntf {
    String getName();
    String getSignature();
    int getAccessFlags();
    int getID();
    List<? extends LineNumberEntry> getLineNumbers();
    int getCodeLength();
    int getNextLineNumber(int pc);
}
