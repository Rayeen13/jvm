package com.cupvm.jvm;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.instance.Instance;
import java.util.*;

import static com.cupvm.jvm.Globals.aconst_null;

public class Heap{
	private static Logger logger= ConsoleLogger.get();
	public static final Object lock=new Object();
	public static boolean printgcstats;
	
	private static int capacity=2*2*2*2*2*2;
	private static Instance[] _heap=new Instance[capacity];

	//private static HashMap<Number, Instance> heap = new HashMap<>();
	private static final LinkedList<Number> eden=new LinkedList<>();
	private static final ArrayList<MyThread> threads=new ArrayList<>(16);
	private static final HashSet<Number> permanent=new HashSet<>();
	//private static final HashSet<Number> newObjects=new HashSet<>();
	public static volatile int static_refs_size=0;
	public static volatile Number[] static_refs=new Number[128];
	public static int addStaticRef(Number value){
		if(static_refs.length<=static_refs_size){
			int new_static_refs_size=(static_refs_size*3)/2+1;
			if(JVM.verbose)
				logger.info("increasing static refences array length to ",new_static_refs_size);
			Number[] new_static_refs=new Number[new_static_refs_size];
			System.arraycopy(static_refs,0,new_static_refs,0,static_refs_size);
			static_refs=new_static_refs;
		}
		static_refs[static_refs_size]=value;
		return static_refs_size++;
	}
	public static volatile int static_fields_size=0;
	public static volatile Number[] static_fields=new Number[128];
	public static int addStaticField(Number value){
		if(static_fields.length<=static_fields_size){
			int new_static_fields_size=(static_fields_size*3)/2+1;
			if(JVM.verbose)
				logger.info("increasing static fields array length to ",new_static_fields_size);
			Number[] new_static_fields=new Number[new_static_fields_size];
			System.arraycopy(static_fields,0,new_static_fields,0,static_fields_size);
			static_fields=new_static_fields;
		}
		static_fields[static_fields_size]=value;
		return static_fields_size++;
	}

	public static void addThread(MyThread t){
		synchronized(threads){
			threads.add(t);
		}
	}

	public static void removeThread(MyThread t) {
		synchronized(threads){
			if(!threads.remove(t))
				throw new RuntimeException();
		}
	}

	public static String tostring() {
		StringBuilder sb=new StringBuilder();
		synchronized(lock){
			sb.append("objects created: ")
				.append(addr)
				.append("\n")
				//.append("garbage objects collected: ")
				//.append(addr-heap.size())	
				.append("heap capacity: ")
				.append(capacity)
				.append("\n")
				.append("static_refs_size:")
				.append(static_refs_size)
				.append("\n")
				.append("static_fields_size:")
				.append(static_fields_size)
				.append("\n");
			if(printgcstats){
				int holes=0;
				for(Instance i:_heap){
					if(i==null)
						holes++;
				}
				sb.append("heap holes: "+holes)
					.append("\n");
				sb.append("heap fill rate: "+(1f-((float)holes/(float)_heap.length)))
				.append("\n");
			}
		}
		return sb.toString();
	}

	private static long addr = 1;
	//alternate addr
	private static int aaddr = 1;
	public static void reset() {
		synchronized(lock){
		//heap.clear();
		//eden.clear();
		permanent.clear();
		//newObjects.clear();
		}
		synchronized(threads){
		threads.clear();
		}
	}
	/*
	public static void removeFromHeap(int addr) {
		synchronized(lock){
		heap.remove(addr);
		}
	}*/

	public static Number add(Instance c) {
		synchronized(lock){
			if(aaddr>=capacity)
				aaddr=1;
			if(_heap[aaddr]==null){
				c.addr=aaddr;
				_heap[aaddr]=c;
				++addr;
				return Long.valueOf(aaddr++);
			}
			++aaddr;
			int pos=((int)addr)&(capacity-1);
			if(_heap[pos]!=null){
				++addr;
				int aaddr=Heap.aaddr;
				Instance[] _heap=Heap._heap;
				///*
				int halfaddr=capacity;
				while(aaddr<halfaddr){
					if(_heap[aaddr]==null){
						_heap[aaddr]=c;
						c.addr=aaddr;
						++addr;
						Heap.aaddr=aaddr;
						return Long.valueOf(aaddr);
					}
					++aaddr;
				}
				aaddr=1;
				//*/
				grow();
				return add(c);
			}
			c.addr=addr;
			_heap[pos]=c;

			//heap.put(addr, c);
			return addr++;
		}
	}

	public static void grow(){
		int newCapacity=capacity*2;	
		Instance[] old=_heap;
		_heap=new Instance[newCapacity];
		int cap=newCapacity-1;
		for(Instance i:old){
			if(i==null)continue;
			int pos=((int)i.addr)&cap;
			if(_heap[pos]!=null)
				throw new RuntimeException("double collision");
			_heap[pos]=i;
		}
		capacity=newCapacity;
	}

	public static Number gcadd(Instance c) {
		synchronized(lock){
			if((addr&65535)==0)
				gc();
			if(aaddr>=capacity)
				aaddr=1;
			if(_heap[aaddr]==null){
				c.addr=aaddr;
				_heap[aaddr]=c;
				++addr;
				return Long.valueOf(aaddr++);
			}
			++aaddr;
			int pos=((int)addr)&(capacity-1);
			if(_heap[pos]!=null){
				Instance[] _heap=Heap._heap;
				++addr;
				//trying alternate addrs
				int aaddr=Heap.aaddr;
				int halfaddr=capacity;
				//search for holes
				while(aaddr<halfaddr){
					if(_heap[aaddr]==null){
						//found a hole
						_heap[aaddr]=c;
						c.addr=aaddr;
						Heap.aaddr=aaddr;
						++addr;
						return Long.valueOf(aaddr);
					}
					++aaddr;
				}
				if(printgcstats){
					logger.info("no hole found aaddr is now ",aaddr);
				}
				aaddr=1;
				//gc();
				grow();
				return add(c);
			}

			_heap[pos]=c;
			c.addr=addr;

			//heap.put(addr, c);
			return addr++;
		}
	}

	/*
	public static Number gcadd(Instance c) {
		//synchronized(lock){
			if((addr&65535)==0)
				//if((addr&1048575)==0)
				gc();
			return add(c);
			//heap.put(addr, c);
			/*
			eden.add(addr);
			if(eden.size()>256)
				eden.removeFirst();
				*/
			//run garbage collector sometimes
			//if((addr&32767)==0)
			//return addr++;
		//}
	//}*/

	private static  int gccounter=0;
	/*simple garbage collector (may be buggy)

	*/

	/**
	 *  this garbage collector implementation seems to work,
	 *  even though the current JVM implementation still does
	 *  not calculate the types of local variables and operand
	 *  stack values (for each program counter value).
	 */
	public static void gc(){
		synchronized(lock){
			gccounter++;

			HashSet<Number> roots = new HashSet<>(permanent);

			int length=static_refs_size;
			for(int i=0;i<length;++i){
					roots.add(static_refs[i]);
			}
			//checkStatics(roots);
			checkThreads(roots);

			//HashMap<Number,Instance> newheap=new HashMap<>(heap.size()/4+1);
			//HashSet<Number> refs=new HashSet<>(heap.size()/4+1);
				Instance[] newheap;
				if((addr&2097151)==0)
					newheap=new Instance[capacity/2];
				else
					newheap=new Instance[capacity];
loop:
			while(true){
				//roots.addAll(eden);
				//roots.addAll(newObjects);
				for(Number ref:roots){
					if(!checkRef(ref,newheap)){
						newheap=new Instance[capacity];		
						if(printgcstats){
							logger.info("same size heap after gc:",capacity);
						}
						continue loop;
					}
				}
				break;
			}
			capacity=newheap.length;

			/*
			for(Number p:refs)

				newheap.put(p,heap.get(p));
			 */
			if(printgcstats) {
				logger.info("threads: ",threads.size());
				//JVM.log("gc: " + (heap.size() - newheap.size()) + " garbage objects collected");
				//JVM.log("new heap size: " +  newheap.size()+" objects");
				logger.info("gc: heap capacity: ",newheap.length);
				logger.info("addr: ",addr);
				logger.info("aaddr: "+aaddr);
				int holes=0;
				for(Instance i:_heap){
					if(i==null)
						holes++;
				}
				logger.info("holes: ",holes);
				logger.info("fill rate: ",(1f-((float)holes/(float)_heap.length)));
			}
			_heap=newheap;
		}
		System.gc();
		//heap.clear();
		//heap.putAll(newheap);
	}

	private static boolean checkRef(Number ref, Instance[] newheap){
		if(ref == null)return true;
		if(ref == aconst_null)return true;
		//if(newheap.containsKey(ref))return;
		long addr=ref.longValue();
		int pos=((int)addr)&(newheap.length-1);
		Instance instance=newheap[pos];
		if(instance!=null){
			return instance.addr == addr;
		}


		int cpos=((int)addr)&(capacity-1);
		instance=_heap[cpos];

			newheap[pos]=instance;


		if(instance.refs==null)return true;
		if(instance.refs.length==0)return true;
		for(Number r:instance.refs){
			if(!checkRef(r,newheap))
				return false;
		}
		/*
		if(instance instanceof ObjectArrayInstance){
			ObjectArrayInstance ai= (ObjectArrayInstance) instance;
			Number[] array= ai.refs;
			for(Number a:array)
				checkRef(a,refs);
			return;
		}
		if(instance.refs==null)
			return;
		if(instance.refs.length==0)
			return;
		//AClass c = instance.clazz;
		//ArrayList<Integer> ref_names= c.getReferenceNames();
		//HashMap<String, Number> fields = ci.fields;
		//if(ref_names==null)
				//return;
		for(Number r:instance.refs){
			checkRef(r,refs);
		}*/
		return true;
	}

	private static void checkThreads(HashSet<Number> refs) {
		synchronized(threads){
			for(MyThread t:threads){
				t.checkReferences(refs);
			}
		}
	}

	private static void checkStatics(HashSet<Number> refs) {
		/*
		for(AClass c:sclasses){
			c.getStaticReferecnes(refs);
		}*/
	}

	public static Instance get(Number a) {
		long addr=a.longValue();
		synchronized(lock){
			return _heap[((int)addr)&(capacity-1)];
			/*
			if(i==null)
				return null;
			if(i.addr==addr)
				return i;
			return null;
			*/
			//return heap.get(a);
		}
	}

	public static Number addPermanent(Instance ci) {
		Number a = add(ci);
		makePermanent(a);
		return a;
	}

	public static void makePermanent(Number addr) {
		synchronized(lock){
			permanent.add(addr);
		}
	}

	/*
	public static void newObject(Number oref){
		newObjects.add(oref);
	}


	public static void objectCreated(Number addr) {
		newObjects.remove(addr);
	}*/

	public static void set(Number addr, Instance i){
		int pos=((int)addr.longValue())&(capacity-1);
		_heap[pos]=i;
		synchronized(lock){
			//heap.put(addr,i);
		}
	}

	public static ArrayList<MyThread> getThreads(){
		return threads;
	}
}
