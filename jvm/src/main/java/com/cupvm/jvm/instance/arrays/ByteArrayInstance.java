package com.cupvm.jvm.instance.arrays;


public class ByteArrayInstance extends ArrayInstance{
	public final byte[] arr;
	public ByteArrayInstance(final int size){
		super(size);
		arr=new byte[size];
		helper=arr;
	}
	public ByteArrayInstance(byte[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new ByteArrayInstance(arr.clone());
	}

}
