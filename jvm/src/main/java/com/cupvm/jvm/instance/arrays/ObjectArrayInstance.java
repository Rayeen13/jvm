package com.cupvm.jvm.instance.arrays;

import com.cupvm.jvm.Heap;
import com.cupvm.jvm.instance.Instance;

import java.util.Iterator;

public class ObjectArrayInstance extends ArrayInstance implements Iterable<Instance>{

	public ObjectArrayInstance(int size){
		super(size);
		refs=new Number[size];
		helper=refs;
	}
	public ObjectArrayInstance(Number[] arr){
		super(arr.length);
		this.refs=arr;
		helper=refs;
	}
	public ArrayInstance cloneme(){
		return new ObjectArrayInstance(refs.clone());
	}

	@Override
	public Iterator<Instance> iterator(){
		return new Iterator<Instance>(){
			int pos=0;
			@Override
			public boolean hasNext(){
				return pos<refs.length;
			}

			@Override
			public Instance next(){
				return Heap.get(refs[pos++]);
			}
		};
	}
}
