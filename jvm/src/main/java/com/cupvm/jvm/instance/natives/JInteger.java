package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;

public class JInteger extends Instance {
    public final int value;

    public JInteger(AClass clazz, int value) {
        super(clazz);
        this.value=value;
    }

    @Override
    public final boolean equals(Object o) {
        JInteger jInteger = (JInteger) o;
        return value == jInteger.value;
    }

    @Override
    public final int hashCode() {
        return value;
    }
}
