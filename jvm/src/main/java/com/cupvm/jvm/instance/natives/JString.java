package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm.JVM;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm._abstract.AClass;

public class JString extends Instance {
    public final String s;
    private final int hash;


    public JString(String s){
        super(JVM.stringClass);
        this.s=s;
        this.hash=s.hashCode();
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public String toString() {
        return s;
    }

}
