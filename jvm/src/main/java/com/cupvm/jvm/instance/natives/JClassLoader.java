package com.cupvm.jvm.instance.natives;

import com.cupvm.jni.JNI;
import com.cupvm.jvm.MyThread;
import com.cupvm.jvm.Heap;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.instance.Instance;

public class JClassLoader extends Instance {
	private static Number oref;
	private ClassLoader classLoader;

	public static Number get(ClassLoader classLoader, JNI jni){
		if(oref==null){
			oref=Heap.addPermanent(new JClassLoader(classLoader));
			jni.registerNatives(classLoader.loadClass("java/lang/ClassLoader"));
			MyThread.addNativeMethods(NInputStream.gen());
			jni.registerNatives(
			classLoader.loadClass("neo/NInputStream"));
		}
		return oref;	
	}

	private JClassLoader(ClassLoader classLoader) {
		super(classLoader.loadClass("java/lang/ClassLoader"));
		this.classLoader=classLoader;
	}
}
