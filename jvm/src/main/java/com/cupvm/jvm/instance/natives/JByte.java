package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;

public class JByte extends Instance {
    public final byte value;

    public JByte(AClass clazz, byte value) {
        super(clazz);
        this.value=value;
    }
}
