package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm.MyThread;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;

public class JThread extends Instance{
	private final MyThread thread;

	public JThread(AClass clazz, MyThread thread){
		super(clazz);
		this.thread=thread;
	}
}
