package com.cupvm.jvm.instance.arrays;

public class LongArrayInstance extends ArrayInstance{
	public final long[] arr;
	public LongArrayInstance(int size){
		super(size);
		arr=new long[size];
		helper=arr;
	}
	public LongArrayInstance(long[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new LongArrayInstance(arr.clone());
	}
}
