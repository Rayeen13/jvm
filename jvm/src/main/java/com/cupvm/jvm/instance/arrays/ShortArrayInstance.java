package com.cupvm.jvm.instance.arrays;

public class ShortArrayInstance extends ArrayInstance{
	public final short[] arr;

	public ShortArrayInstance(int size){
		super(size);
		arr=new short[size];
		helper=arr;
	}
	public ShortArrayInstance(short[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new ShortArrayInstance(arr.clone());
	}
}
