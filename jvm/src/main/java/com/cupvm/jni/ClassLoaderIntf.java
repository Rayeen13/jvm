package com.cupvm.jvm;

import java.util.List;

public interface ClassLoaderIntf{
	ClassIntf getLoadedClass(String className);
	List<? extends ClassIntf> getLoadedClasses();
	ClassIntf getClassByID(int id);
}
