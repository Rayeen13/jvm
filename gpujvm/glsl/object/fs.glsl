#version 150 core

in vec3 vertexColor;

out vec4 fragColor;

void main() {
vec3 c=vertexColor;
c.r=0.0;
    fragColor = vec4(c, 1.0);
}
