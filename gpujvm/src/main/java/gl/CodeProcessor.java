package gl;


import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm._class.Class;

import java.util.HashMap;

public class CodeProcessor {
    /*
    private final Map<Integer,Integer> p_old_to_new=new HashMap<>();
    private final Map<Integer,Integer> p_new_to_old=new HashMap<>();

    //optimized code
    private final List<Short> ocode=new ArrayList<>();
    //precalculated params
    private final ArrayList<Integer> params=new ArrayList<>();
    private short[] getCode(){
        short[] code=new short[ocode.size()];
        for(int i=0;i<ocode.size();++i){
            code[i]=ocode.get(i);
        }
        return code;
    }
    private int[] getParams(){
        int[] _params=new int[params.size()];
        for(int i=0;i<params.size();++i){
            _params[i]=params.get(i);
        }
        return _params;
    }

    private void addIndex(){
        int i=params.size()-1;
        ocode.add((short)i);
    }
    private final ArrayList<Integer> jumps=new ArrayList<>();

    private static int counter=0;
    private void addJump(int pos){
        jumps.add(pos);
    }

    private void calculateJump(int newpc,int i){
        int pindex=ocode.get(i);
        int pc=p_new_to_old.get(newpc);
        int oldjump=params.get(pindex);
        pc+=oldjump;
        int newpos=p_old_to_new.get(pc);
        params.set(pindex,newpos-newpc);
    }

    public CodeProcessor(CodeAttribute codeattr) {
        try {
            optimize((byte[])codeattr.getCode());
        }
        catch (RuntimeException e){
            success=false;
        }
        if(success){
            recalcLineNumbers(codeattr);
            recalcExceptionHandlers(codeattr);
        }
    }

    private void recalcExceptionHandlers(CodeAttribute codeattr) {
        MException[] etable = codeattr.getExceptionTable();
        for(MException e:etable){
            e.handler_pc=p_old_to_new.get(e.handler_pc);
            e.start_pc=p_old_to_new.get(e.start_pc);
            e.end_pc=p_old_to_new.get(e.end_pc);
        }
    }

    private void recalcLineNumbers(CodeAttribute codeattr) {
        HashMap<Integer,Integer> newtable=new HashMap<>();
        LineNumberTableAttribute lta=codeattr.getLineNumberTableAttribute();
        Map<Integer,Integer> oldtable=lta.getTable();
        Set<Integer> pca=oldtable.keySet();
        for(Integer oldpc:pca){
            int newpc=p_old_to_new.get(oldpc);
            newtable.put(newpc,oldtable.get(oldpc));
        }
        lta.setTable(newtable);
        codeattr.setCode(getCode());
        codeattr.setParams(getParams());

    }

    private boolean success=false;
    public boolean success(){
        return success;
    }

*/
    public static int[] optimize(short[] code, Class cC, HashMap<String,Integer> mtable){
        int icode[]=new int[code.length];
        //local variables are better for performance
        //program counter
        int pc = 0;
        int i1, i2, i3, i4, aref, oref, jump;
        long l1, l2;
        float f1, f2;
        double d1, d2;
        Object obj;
        Constant fc;
        Constant fname;
        Constant ftype;

        while(pc<code.length){
			/*
			   StringBuilder sb = new StringBuilder();
			   try {
				   int aa = CodeAttribute.parseOp(pc, code, sb);
			   }
			   catch (Exception e){}
			   System.out.println(sb);
			//*/

            switch (code[pc]) {
                case 0:
                    ++pc;
                    continue;
                case (byte) 0x10:
                    //bipush
                    icode[pc]=code[pc];
                    icode[pc+1]=(byte)code[pc+1];
                    pc+=2;
                    continue;
                case (short) 0x12:
                    //ldc
                case (short) 0x15:
                    //iload
                case (short) 0x16:
                    //lload
                case (short) 0x17:
                    //fload
                case (short) 0x18:
                    //dload
                case (short) 0x19:
                    //aload
                case (short) 0x36:
                    //istore
                case (short) 0x37:
                    //lstore
                case (short) 0x38:
                    //fstore
                case (short) 0x39:
                    //dstore
                case (short) 0x3A:
                    //astore
                case (short) 0xA9:
                    //ret
                case (short) 0xBC:
                    //newarray
                    //ocode.add((short)code[pc]);
                    //p_old_to_new.put(pc,ocode.size()-1);
                    //p_new_to_old.put(ocode.size()-1,pc);
                    //ocode.add((short)code[pc+1]);
                    icode[pc]=code[pc];
                    icode[pc+1]=code[pc+1];
                    pc+=2;
                    continue;
                case (short) 0x99:
                    //ifeq
                case (short) 0x9A:
                    //ifne
                case (short) 0x9B:
                    //iflt
                case (short) 0x9C:
                    //ifge
                case (short) 0x9D:
                    //ifgt
                case (short) 0x9E:
                    //ifle
                case (short) 0x9F:
                    //if_icmpeq
                case (short) 0xA0:
                    //if_icmpne
                case (short) 0xA1:
                    //if_icmplt
                case (short) 0xA2:
                    //if_icmpge
                case (short) 0xA3:
                    //if_icmpgt
                case (short) 0xA4:
                    //if_icmple
                case (short) 0xA5:
                    //if_acmpeq
                case (short) 0xA6:
                    //if_acmpeq
                case (short) 0xA7:
                    //goto
                case (short) 0xA8:
                    //jsr
                case (short) 0xC6:
                    //ifnull
                case (short) 0xC7:
                    //ifnonnull
                    //ocode.add((short)code[pc]);
                    icode[pc]=code[pc];
                    //p_old_to_new.put(pc,ocode.size()-1);
                    //p_new_to_old.put(ocode.size()-1,pc);
                    //addJump(ocode.size()-1);
                    //int p1=i(code[pc + 1], code[pc + 2]);
                    //int p2=i(code[pc + 1], code[pc + 2]);
                    icode[pc+1]=i(code[pc + 1], code[pc + 2]);
                    pc += 3;
                    continue;
                case (short) 0x84:
                    //iinc
                    icode[pc]=code[pc];
                    //ocode.add((short)code[pc]);
                    //p_old_to_new.put(pc,ocode.size()-1);
                    //p_new_to_old.put(ocode.size()-1,pc);
                    //ocode.add((short)code[pc+1]);
                    //ocode.add((short)code[pc+2]);
                    icode[pc+1]=code[pc+1];
                    icode[pc+2]=code[pc+2];
                    pc += 3;
                    continue;
                case (short) 0x11:
                    //sipush
                case (short) 0x13:
                    //ldc_w
                case (short) 0x14:
                    //ldc2_w
                case (short) 0xB2:
                    //getstatic
                case (short) 0xB3:
                    //putstatic
                case (short) 0xB4:
                    //getfield
                case (short) 0xB5:
                    //putfield
                case (short) 0xBB:
                    //new
                case (short) 0xC0:
                    //checkcast
                case (short) 0xC1:
                    //instanceof
                    icode[pc]=code[pc];
                    icode[pc+1]=i(code[pc + 1], code[pc + 2]);
                    pc += 3;
                    continue;
                case (short) 0xB6:
                    //invokevirtual
                case (short) 0xB7:
                    //invokespecial
                case (short) 0xB8:
                    //invokestatic
                    icode[pc]=code[pc];
                    AMethod method = cC.getMethod(i(code[pc + 1], code[pc + 2]));
                    icode[pc+1]=mtable.get(method.signature);
                    pc += 3;
                    continue;
                case (short) 0xAA:
                    icode[pc]=code[pc];
                    pc = pc + (4 - pc % 4);
                    /*
                    int default_offset = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                    params.add(default_offset);
                    addIndex();
                    pc += 4;
                    int low = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                    params.add(low);
                    pc += 4;
                    int high = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                    params.add(high);
                    pc += 4;
                    int pc2=pc;

                    for(i1=low;i1<=high;++i1){
                        pc=pc2 + (i1 - low) * 4;
                        jump= i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                        params.add(jump);
                    }
                    int n = high - low + 1;
                     */
                    pc+=4;

                    throw new RuntimeException();
                    //continue;
                case (short) 0xAB:
                    //lookupswitch
                    /*
                    pc = pc + (4 - pc % 4);
                    pc += 4;
                    i2 = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                    params.add(i2);
                    pc += 4;
                    for (i3 = 0; i3 < i2; ++i3) {
                        int key = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                        params.add(key);

                        pc += 4;
                        int value = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
                        params.add(value);
                        pc += 4;
                    }
                     */
                    throw new RuntimeException();
                    //continue;
                case (short) 0xB9:
                    //invokeinterface
                    /*
                    ocode.add((short)code[pc]);
                    p_old_to_new.put(pc,ocode.size()-1);
                    p_new_to_old.put(ocode.size()-1,pc);
                    //index to constantpool
                    i1=i(code[pc + 1], code[pc + 2]);
                    params.add(i1);
                    addIndex();

                    byte count = code[pc + 3];
                    ocode.add((short)count);
                    byte zero = code[pc + 4];
                    ocode.add((short)zero);
                    pc += 5;
                     */
                    throw new RuntimeException();
                    //continue;
                case (short) 0xBA:
                    //invokedynamic
                    throw new RuntimeException("invokedynamic not implemented");
                case (short) 0xBD:
                    //anewarray
                    icode[pc]=code[pc];
                    /*
                    ocode.add((short)code[pc]);
                    p_old_to_new.put(pc,ocode.size()-1);
                    p_new_to_old.put(ocode.size()-1,pc);
                     */
                    pc += 3;
                    continue;
                case (short) 0xC4:
                    //wide
                    /*
                    ocode.add((short)code[pc]);
                    p_old_to_new.put(pc,ocode.size()-1);
                    p_new_to_old.put(ocode.size()-1,pc);
                    pc += 1;
                    switch (code[pc]) {
                        case (short) 0x15:
                            //iload
                        case (short) 0x16:
                            //lload
                        case (short) 0x17:
                            //fload
                        case (short) 0x18:
                            //dload
                        case (short) 0x19:
                            //aload
                        case (short) 0x36:
                            //istore
                        case (short) 0x37:
                            //lstore
                        case (short) 0x38:
                            //fstore
                        case (short) 0x39:
                            //dstore
                        case (short) 0x3A:
                            //astore
                            continue;
                        case (short) 0x84:
                            //iinc
                            //i1 = (int) lv[ii(code[pc + 1], code[pc + 2])];
                            //lv[(int) code[pc + 1]] = i1 + ii(code[pc + 3], code[pc + 4]);
                            pc += 5;
                            continue;

                    }
                     */
                    throw new RuntimeException("wide not implememted");
                    //continue;
                case (short) 0xC5:
                    //multianewarray
                    /*
                    ocode.add((short)code[pc]);
                    p_old_to_new.put(pc,ocode.size()-1);
                    p_new_to_old.put(ocode.size()-1,pc);
                    i1 = i(code[pc + 1], code[pc + 2]);
                    params.add(i1);
                    addIndex();
                    //dimensions
                    i2 = code[pc + 3];
                    ocode.add((short)i2);
                     */
                    pc += 4;
                    throw new RuntimeException("wide not implememted");
                case (short) 0xC8:
                    //goto
                    icode[pc]=code[pc];
                    icode[pc+1]=i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
                    continue;
                case (short) 0xC9:
                    //jsr_w
                    icode[pc]=code[pc];
                    icode[pc+1]= i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
                    continue;
                default:
                    icode[pc]=code[pc];
                    pc += 1;
            }
        }
        return icode;
    }

    private int l2i1(long l) {
        return (int) (l >> 32);
    }

    private int l2i2(long l) {
        return (int) l;
    }


    private static short i(short b1, short b2) {
        return (short) (((b1 & 0xff) << 8) | (b2 & 0xff));
    }

    private static long _2i2l(int i1, int i2) {
        return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
    }

	/*private static int ii(byte b1, byte b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}*/

    private static int i(short b1, short b2, short b3, short b4) {
        return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
                ((0xFF & b3) << 8) | (0xFF & b4);
    }


}
