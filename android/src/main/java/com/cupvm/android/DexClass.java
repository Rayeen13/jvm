package com.cupvm.android;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AField;
import com.cupvm.jvm._abstract.AMethod;

import java.util.*;
import java.io.*;
import java.nio.*;

public class DexClass extends AClass{
	private static int string_ids_off;
	private static int type_ids_off;
	private static Map<String, AClass> classes;
	public static void load(InputStream raw, int size)throws IOException{
	
		ByteArrayOutputStream buffer = new ByteArrayOutputStream(size);

		int nRead;
		byte[] bytes = new byte[size];

		while ((nRead = raw.read(bytes, 0, bytes.length)) != -1) {
			buffer.write(bytes, 0, nRead);
		}

		bytes = buffer.toByteArray();

		ByteBuffer buf = ByteBuffer.wrap(bytes);
		DexParser dex_parser=new DexParser(buf);
		classes=dex_parser.parse();
		/*
		long magic=buf.getLong();
		System.out.print("magic number: ");
		System.out.println(Long.toHexString(magic));
		int checksum=buf.getInt();
		byte[] signature=new byte[20];
		buf.get(signature);
		int file_size=buf.getInt();

		System.out.print("file size: ");
		System.out.println(little2big(file_size));

		int header_size=buf.getInt();

		System.out.print("header size: ");
		System.out.println(little2big(header_size));


		int endian_tag=buf.getInt();

		System.out.print("endian_tag: ");
		System.out.println(little2big(endian_tag));


		int link_size=buf.getInt();

		System.out.print("link_size: ");
		System.out.println(little2big(link_size));

		int link_off=buf.getInt();

		System.out.print("link_off: ");
		System.out.println(little2big(link_off));

		int map_off=buf.getInt();

		System.out.print("map_off: ");
		System.out.println(little2big(map_off));

		int string_ids_size=buf.getInt();

		System.out.print("string_ids_size: ");
		System.out.println(little2big(string_ids_size));

		string_ids_off=little2big(buf.getInt());

		System.out.print("string_ids_off: ");
		System.out.println(little2big(string_ids_off));


		int type_ids_size=buf.getInt();

		System.out.print("type_ids_size: ");
		System.out.println(little2big(type_ids_size));

		type_ids_off=little2big(buf.getInt());

		System.out.print("type_ids_off: ");
		System.out.println(type_ids_off);


		int proto_ids_size=buf.getInt();

		System.out.print("proto_ids_size: ");
		System.out.println(little2big(proto_ids_size));

		int proto_ids_off=buf.getInt();

		System.out.print("proto_ids_off: ");
		System.out.println(little2big(proto_ids_off));


		int field_ids_size=buf.getInt();

		System.out.print("field_ids_size: ");
		System.out.println(little2big(field_ids_size));

		int field_ids_off=buf.getInt();

		System.out.print("field_ids_off: ");
		System.out.println(little2big(field_ids_off));


		int method_ids_size=buf.getInt();

		System.out.print("method_ids_size: ");
		System.out.println(little2big(method_ids_size));

		int method_ids_off=buf.getInt();

		System.out.print("method_ids_off: ");
		System.out.println(little2big(method_ids_off));



		int class_defs_size=little2big(buf.getInt());

		System.out.print("class_defs_size: ");
		System.out.println(class_defs_size);

		int class_defs_off=buf.getInt();

		System.out.print("class_defs_off: ");
		System.out.println(little2big(class_defs_off));



		int data_size=buf.getInt();

		System.out.print("data_size: ");
		System.out.println(little2big(data_size));

		int data_off=buf.getInt();

		System.out.print("data_off: ");
		System.out.println(little2big(data_off));

		int soff=buf.getInt();
		System.out.print("soff: ");
		System.out.println(little2big(soff));

		//CLASSES
		buf.position(little2big(class_defs_off));
		for(int i=0;i<class_defs_size;i++){
				DexClass cl=new DexClass(buf);
				classes.put(cl.getName(),cl);
		}
		*/
	}

	public DexClass(DexParser dex_parser) throws IOException{
		super(null, null);
		int class_idx=dex_parser.getInt();
				dex_parser.pushpos();
				String name=dex_parser.getTypeDesc(class_idx);
				this.name=name.substring(1,name.length()-1);
				System.out.println();
				System.out.println("CLASSNAME: "+this.name);
				dex_parser.poppos();
				this.access_flags=dex_parser.getInt();
				int superclass_idx=dex_parser.getInt();
				int interfaces_off=dex_parser.getInt();
				int source_file_idx=dex_parser.getInt();
				dex_parser.pushpos();
				source_file_name=dex_parser.readString(source_file_idx);

				dex_parser.poppos();
				int annotations_off=dex_parser.getInt();
				int class_data_off=dex_parser.getInt();
				int static_values_off=dex_parser.getInt();

				dex_parser.pushpos();

				dex_parser.setPosition(class_data_off);
				int static_fields_size=(int)dex_parser.getUleb128();
				int instance_fields_size=(int)dex_parser.getUleb128();
				int direct_methods_size=(int)dex_parser.getUleb128();
				int virtual_methods_size=(int)dex_parser.getUleb128();
				System.out.print("static_fields_size:");
				System.out.println(static_fields_size);
				for(int i=0;i<static_fields_size;i++){
					DexField f=dex_parser.parseField();	
				}

				System.out.print("instance_fields_size:");
				System.out.println(instance_fields_size);
				for(int i=0;i<instance_fields_size;i++){
					DexField f=dex_parser.parseField();	
				}

				System.out.println("direct_methods_size: "+direct_methods_size);
				System.out.println("virtual_methods_size: "+virtual_methods_size);
				for(int i=0;i<direct_methods_size;i++){
					DexMethod f=dex_parser.parseMethod(this);	
				}

				for(int i=0;i<virtual_methods_size;i++){
					DexMethod f=dex_parser.parseMethod(this);	
				}

				dex_parser.poppos();
	}

	private String source_file_name;
	private String name;

	@Override
	public boolean isInstance(AClass cl) {
		return false;
	}



	@Override
	public AMethod getMethod(String nameAndDesc) {
		return null;
	}




	@Override
	public AMethod getDeclaredMethod(String nameAndDesc) {
		return null;
	}


	@Override
	public AClass[] getInterfaces() {
		return new AClass[0];
	}


	@Override
	public AMethod getMethodHead(String s) {
		return null;
	}

	@Override
	public HashMap<String, AField> getInstanceFields(){
		return null;
	}

	public int getID(){
		return 0;
	}
}
